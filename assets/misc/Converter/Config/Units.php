<?php
/**
 * - Adapted notation to the one used in ECMWF Weather data.
 *      - Original Units had km^2 as km2 - exponents are now marked up with '**' making it km**2
 * - Added Area Density functions
 */
return array(
    ///////Units Of Length///////
    "m" => array("base" => "m", "conversion" => 1, "inverted" => "ft"), //meter - base unit for distance
    "km" => array("base" => "m", "conversion" => 1000, "inverted" => "mi"), //kilometer
    "dm" => array("base" => "m", "conversion" => 0.1, "inverted" => "ft"), //decimeter
    "cm" => array("base" => "m", "conversion" => 0.01, "inverted" => "in"), //centimeter
    "mm" => array("base" => "m", "conversion" => 0.001, "inverted" => "in"), //milimeter
    "µm" => array("base" => "m", "conversion" => 0.000001, "inverted" => "in"), //micrometer
    "nm" => array("base" => "m", "conversion" => 0.000000001, "inverted" => "in"), //nanometer
    "pm" => array("base" => "m", "conversion" => 0.000000000001, "inverted" => "in"), //picometer
    "in" => array("base" => "m", "conversion" => 0.0254, "inverted" => "cm"), //inch
    "inches" => array("base" => "m", "conversion" => 0.0254, "inverted" => "cm"), //inch
    "\"" => array("base" => "m", "conversion" => 0.0254, "inverted" => "cm"), //inch
    "ft" => array("base" => "m", "conversion" => 0.3048, "inverted" => "m"), //foot
    "fts" => array("base" => "m", "conversion" => 0.3048, "inverted" => "m"), //foot
    "feet" => array("base" => "m", "conversion" => 0.3048, "inverted" => "m"), //foot
    "feets" => array("base" => "m", "conversion" => 0.3048, "inverted" => "m"), //foot
    "'" => array("base" => "m", "conversion" => 0.3048, "inverted" => "m"), //foot
    "yd" => array("base" => "m", "conversion" => 0.9144, "inverted" => "m"), //yard
    "yds" => array("base" => "m", "conversion" => 0.9144, "inverted" => "m"), //yard
    "yard" => array("base" => "m", "conversion" => 0.9144, "inverted" => "m"), //yard
    "yards" => array("base" => "m", "conversion" => 0.9144, "inverted" => "m"), //yard
    "mi" => array("base" => "m", "conversion" => 1609.344, "inverted" => "km"), //mile
    "mile" => array("base" => "m", "conversion" => 1609.344, "inverted" => "km"), //mile
    "miles" => array("base" => "m", "conversion" => 1609.344, "inverted" => "km"), //mile


    ///////Units Of Area///////
    "m**2" => array("base" => "m**2", "conversion" => 1, "inverted" => "ft**2"), //meter square - base unit for area
    "km**2" => array("base" => "m**2", "conversion" => 1000000, "inverted" => "mi**2"), //kilometer square
    "cm**2" => array("base" => "m**2", "conversion" => 0.0001, "inverted" => "ft**2"), //centimeter square
    "mm**2" => array("base" => "m**2", "conversion" => 0.000001, "inverted" => "ft**2"), //milimeter square
    "ft**2" => array("base" => "m**2", "conversion" => 0.092903, "inverted" => "m**2"), //foot square
    "mi**2" => array("base" => "m**2", "conversion" => 2589988.11, "inverted" => "km**2"), //mile square
    "ac" => array("base" => "m**2", "conversion" => 4046.86, "inverted" => "km**2"), //acre
    "ha" => array("base" => "m**2", "conversion" => 10000, "inverted" => "km**2"), //hectare

    ///////Units Of Volume///////
    "dm3" => array("base" => "l", "conversion" => 1, "inverted" => "gal"), //cubic decimeter - litre
    "l" => array("base" => "l", "conversion" => 1, "inverted" => "gal"), //litre - base unit for volume
    "ml" => array("base" => "l", "conversion" => 0.001, "inverted" => "pt"), //mililitre
    "cm3" => array("base" => "l", "conversion" => 0.001, "inverted" => "pt"), //cubic centimeter - mililitre
    "hl" => array("base" => "l", "conversion" => 100, "inverted" => "gal"), //hectolitre
    "kl" => array("base" => "l", "conversion" => 1000, "inverted" => "gal"), //kilolitre
    "m3" => array("base" => "l", "conversion" => 1000, "inverted" => "gal"), //meters cubed - kilolitre
    "pt" => array("base" => "l", "conversion" => 0.56826125, "inverted" => "ml"), //pint
    "gal" => array("base" => "l", "conversion" => 4.405, "inverted" => "l"), //gallon
    "qt" => array("base" => "l", "conversion" => 1.1365225, "inverted" => "l"), //quart
    "ft3" => array("base" => "l", "conversion" => 28.316846592, "inverted" => "l"), //cubic feet
    "in3" => array("base" => "l", "conversion" => 0.016387064, "inverted" => "ml"), //cubic inches

    ///////Units Of Weight///////
    "kg" => array("base" => "kg", "conversion" => 1, "inverted" => "lb"), //kilogram - base unit for weight
    "g" => array("base" => "kg", "conversion" => 0.001, "inverted" => "oz"), //gram
    "mg" => array("base" => "kg", "conversion" => 0.000001, "inverted" => "oz"), //miligram
    "N" => array("base" => "kg", "conversion" => 1 / 9.80665002863885), //Newton (based on earth gravity)
    "st" => array("base" => "kg", "conversion" => 6.35029, "inverted" => "kg"), //stone
    "lb" => array("base" => "kg", "conversion" => 0.453592, "inverted" => "kg"), //pound
    "lbs" => array("base" => "kg", "conversion" => 0.453592, "inverted" => "kg"), //pound
    "oz" => array("base" => "kg", "conversion" => 0.0283495, "inverted" => "g"), //ounce

    //////Units Of Speed///////
    "m s**-1" => array("base" => "m s**-1", "conversion" => 1, "inverted" => "mi h**-1"), //meter per seond - base unit for speed
    "km h**-1" => array("base" => "m s**-1", "conversion" => 1/3.6, "inverted" => "mi h**-1"), //kilometer per hour
    "mi h**-1" => array("base" => "m s**-1", "conversion" => 1.60934*1/3.6, "inverted" => "km h**-1"), //mi => km then convert like km/h

    ///////Units Of Rotation///////
    "deg" => array("base" => "deg", "conversion" => 1, "inverted" => "rad"), //degrees - base unit for rotation
    "rad" => array("base" => "deg", "conversion" => 57.2958, "inverted" => "deg"), //radian

    ///////Units Of Temperature///////
    "k" => array("base" => "k", "conversion" => 1, "inverted" => "c"), //kelvin - base unit for distance
    "c" => array("base" => "k", "conversion" => function ($val, $tofrom) {
        return $tofrom ? $val - 273.15 : $val + 273.15;
    }, "inverted" => "f"), //celsius
    "f" => array("base" => "k", "conversion" => function ($val, $tofrom) {
        return $tofrom ? ($val * 9 / 5 - 459.67) : (($val + 459.67) * 5 / 9);
    }, "inverted" => "c"), //Fahrenheit

    ///////Units Of Pressure///////
    "pa" => array("base" => "pa", "conversion" => 1, "inverted" => "psi"), //Pascal - base unit for Pressure
    "hpa" => array("base" => "pa", "conversion" => 100, "inverted" => "psi"), //hpa
    "kpa" => array("base" => "pa", "conversion" => 1000, "inverted" => "psi"), //kilopascal
    "mpa" => array("base" => "pa", "conversion" => 1000000, "inverted" => "psi"), //megapascal
    "bar" => array("base" => "pa", "conversion" => 100000, "inverted" => "psi"), //bar
    "mbar" => array("base" => "pa", "conversion" => 100, "inverted" => "psi"), //milibar
    "psi" => array("base" => "pa", "conversion" => 6894.76, "inverted" => "bar"), //pound-force per square inch

    ///////Units Of Time///////
    "s" => array("base" => "s", "conversion" => 1), //second - base unit for time
    "year" => array("base" => "s", "conversion" => 31536000), //year - standard year
    "month" => array("base" => "s", "conversion" => 18748800), //month - 31 days
    "week" => array("base" => "s", "conversion" => 604800), //week
    "day" => array("base" => "s", "conversion" => 86400), //day
    "hr" => array("base" => "s", "conversion" => 3600), //hour
    "min" => array("base" => "s", "conversion" => 60), //minute
    "ms" => array("base" => "s", "conversion" => 0.001), //milisecond
    "μs" => array("base" => "s", "conversion" => 0.000001), //microsecond
    "ns" => array("base" => "s", "conversion" => 0.000000001), //nanosecond

    ///////Units Of Power///////
    "j" => array("base" => "j", "conversion" => 1), //joule - base unit for energy
    "kj" => array("base" => "j", "conversion" => 1000), //kilojoule
    "mj" => array("base" => "j", "conversion" => 1000000), //megajoule
    "cal" => array("base" => "j", "conversion" => 4184), //calorie
    "Nm" => array("base" => "j", "conversion" => 1), //newton meter
    "ftlb" => array("base" => "j", "conversion" => 1.35582), //foot pound
    "whr" => array("base" => "j", "conversion" => 3600), //watt hour
    "kwhr" => array("base" => "j", "conversion" => 3600000), //kilowatt hour
    "mwhr" => array("base" => "j", "conversion" => 3600000000), //megawatt hour
    "mev" => array("base" => "j", "conversion" => 0.00000000000000016), //mega electron volt

    ///////Area density///////
    "kg m**-2" => array("base" => "kg m**-2", "conversion" => 1),
    //vary area
    "kg km**-2" => array("base" => "kg m**-2", "conversion" => 0.000001),
    "kg cm**-2" => array("base" => "kg m**-2", "conversion" => 1e4),
    "kg mm**-2" => array("base" => "kg m**-2", "conversion" => 1e6),
    //vary weight
    "g m**-2" => array("base" => "kg m**-2", "conversion" => 0.001), //gram
    "mg m**-2" => array("base" => "kg m**-2", "conversion" => 0.000001), //miligram
    "st m**-2" => array("base" => "kg m**-2", "conversion" => 6.35029), //stone
    "lb m**-2" => array("base" => "kg m**-2", "conversion" => 0.453592), //pound
    "oz m**-2" => array("base" => "kg m**-2", "conversion" => 0.0283495), //ounce
    //todo: add your density conversions here if you need them.
);