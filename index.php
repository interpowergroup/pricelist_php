<script type="text/javascript">
  /**
   * This is a simple javascript script to redirect the website using
   * the same language set in the browser of the client
   */
  var language = "";

  if (navigator.browserLanguage) {
    language = navigator.browserLanguage;

  } else {
    language = navigator.language;
  }

  language = language.split("-"); //Using simple localization, ex: fr-FR & fr-CA are both "fr"

  document.location.href = 'login?lang='+language[0];
</script>