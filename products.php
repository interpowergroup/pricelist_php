<?php
	include('shared/config.php');
	
	if(isset($_SESSION["USER"])) {
		$breadcrumb = 0;
		$user = $_SESSION["USER"];

		if($user["branchCount"] == 0) {
			header('Location: logout?err=NoBranches');
		}

		$reqbranches = $bdd->query("SELECT * FROM branches");
    $branches = $reqbranches->fetchAll();

    if(isset($_GET["branch"]) && !empty($_GET["branch"]) && is_numeric($_GET["branch"])) {
			if(!$rbac->check('branch_'.htmlspecialchars($_GET["branch"]), $user["id"])) {
				header('Location: branchselection');
			}
      $reqbranch = $bdd->prepare("SELECT * FROM branches WHERE id = ?");
      $reqbranch->execute(array(htmlspecialchars($_GET["branch"])));
      if($reqbranch->rowCount() == 1) {
        $selectedBranch = $reqbranch->fetch();

        $reqcategories = $bdd->query("SELECT * FROM products_categories");
				$categories = $reqcategories->fetchAll();
				$reqtypes = $bdd->query("SELECT * FROM products_types");
				$types = $reqtypes->fetchAll();
				$reqseries = $bdd->query("SELECT * FROM products_series");
				$series = $reqseries->fetchAll();
				$reqspecset = $bdd->query("SELECT * FROM products_specset");
      	$specset = $reqspecset->fetchAll();

        if(isset($_GET["cat"]) && !empty($_GET["cat"])) {
					$currCategory = $_GET["cat"];
					if($currCategory != 'all' && $currCategory != 'prices') {
						foreach ($categories as $key => $value) {
							if($value["id"] == $_GET["cat"]) {
								$currCategoryName = json_decode($value["name"], true)[$language];
								continue;
							}
						}
					} else {
						if($currCategory == 'all') {
							$currCategoryName = $lang['Navbar']["Categories"]["AllProducts"];
						} else {
							$currCategoryName = $lang['Navbar']["Categories"]["AllPrices"];
						}
					}
					$breadcrumb++;
        } else {
          $currCategory = null;
				}
				if(isset($_GET["serie"]) && !empty($_GET["serie"])) {
					$currSerie = $_GET["serie"];
					$breadcrumb++;
					$currSerieName = '';
					foreach ($series as $key => $value) {
						if($value["id"] == $_GET["serie"]) {
							$currSerieName = json_decode($value["name"], true)[$language];
							continue;
						}
					}
        } else {
					$currSerie = null;
					
        }

				if($currSerie != null) {
					$reqproducts = $bdd->prepare("SELECT prods.*, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, 
																			series.name AS `serie`, subseries.name AS `subserie`, details.details, opts.options, docs.docs, inv.*,
																			series.discounts AS `discounts`, series.multipliers AS `multipliers`, series.transport AS `transport`,
																			series.img AS `serie_img`, cats.img AS `category_img`, types.img AS `type_img`, makers.img AS `maker_img` 
																			FROM products AS prods 
																			RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																			RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																			RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																			RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																			RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																			RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																			RIGHT JOIN products_options AS opts ON prods.id = opts.product_id 
																			RIGHT JOIN products_docs AS docs ON prods.id = docs.product_id 
																			RIGHT JOIN products_inventory AS inv ON prods.id = inv.product_id 
																			WHERE prods.serie_id = ? ORDER BY prods.sequence");
					$reqproducts->execute(array($currSerie));
				} elseif($currCategory == 'all' || $currCategory == 'prices') {
					$reqproducts = $bdd->query("SELECT prods.*, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, 
																			series.name AS `serie`, subseries.name AS `subserie`, details.details, inv.*,
																			series.discounts AS `discounts`, series.multipliers AS `multipliers`, series.transport AS `transport`
																			FROM products AS prods 
																			RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																			RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																			RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																			RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																			RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																			RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																			RIGHT JOIN products_inventory AS inv ON prods.id = inv.product_id 
																			ORDER BY prods.sequence");
				} elseif($currCategory != null) {
					$reqproducts = $bdd->prepare("SELECT * FROM products_categories WHERE id = ?");
					$reqproducts->execute(array($currCategory));
				} else {
					$reqproducts = $bdd->query("SELECT * FROM products_types");
				}
				$allproducts = $reqproducts->fetchAll();
				
				function getProductImage($product) {
					if($product["img"] != "") {
						return $product["img"];
					}
					if($product["serie_img"] != "") {
						return $product["serie_img"];
					}
					if($product["type_img"] != "") {
						return $product["type_img"];
					}
					if($product["category_img"] != "") {
						return $product["category_img"];
          }
          if($product["maker_img"] != "") {
						return $product["maker_img"];
					}
					return ROOT_PATH.'assets/img/no-image.png';
				}

				if($currSerie != null || $currCategory == 'all' || $currCategory == 'prices') {
					for ($i = 0; $i < count($series); $i++) { //Fill only the good serie with the product
						$series[$i]["products"] = [];
						if($currCategory != 'all' && $currCategory != 'prices') {
							if($series[$i]["id"] == $currSerie) {
								foreach ($allproducts as $key => $value) { //Set the image
									$allproducts[$key]["img"] = getProductImage($value);
									$allproducts[$key]["serie_img"] = null;
									$allproducts[$key]["type_img"] = null;
									$allproducts[$key]["category_img"] = null;
									$allproducts[$key]["maker_img"] = null;
									$specs = json_decode($value["details"], true);
									foreach ($specs as $key3 => $optionset) {
										foreach ($specset as $index => $alloptionset) {
											if($alloptionset["id"] == $optionset["id"]) {
												$specs[$key3]["name"] = json_decode($alloptionset["name"], true);
												$specs[$key3]["specs"] = json_decode($alloptionset["specs"], true);
												foreach ($specs[$key3]["specs"] as $key2 => $value) {
													if(array_key_exists($key2, $optionset["args"])) {
														$specs[$key3]["specs"][$key2]["arg"] = $optionset["args"][$key2];
													} else {
														$specs[$key3]["specs"][$key2]["arg"] = ["can" => "", "us" => ""];
													}
												}
											}
										}
									}
									$allproducts[$key]["details"] = json_encode($specs);
								}
								$series[$i]["products"] = $allproducts;
							}
						} else {
							for ($i = 0; $i < count($series); $i++) { $series[$i]["products"] = []; }
							foreach ($series as $key => $serie) {
								foreach ($allproducts as $key2 => $value) { //Set the image
									// $allproducts[$key2]["img"] = getProductImage($value);
									if($value["serie_id"] == $serie["id"]) {
										$specs = json_decode($value["details"], true);
										foreach ($specs as $key3 => $optionset) {
											foreach ($specset as $index => $alloptionset) {
												if($alloptionset["id"] == $optionset["id"]) {
													$specs[$key3]["name"] = json_decode($alloptionset["name"], true);
													$specs[$key3]["specs"] = json_decode($alloptionset["specs"], true);
													foreach ($specs[$key3]["specs"] as $key2 => $value3) {
														if(array_key_exists($key2, $optionset["args"])) {
															$specs[$key3]["specs"][$key2]["arg"] = $optionset["args"][$key2];
														} else {
															$specs[$key3]["specs"][$key2]["arg"] = ["can" => "", "us" => ""];
														}
													}
												}
											}
										}
										$value["details"] = json_encode($specs);
										if($currCategory == 'all') {
											if(json_decode($value["in_stock"], true)[$selectedBranch["id"]] > 0 || json_decode($value["on_order"], true)[$selectedBranch["id"]] > 0) {
												array_push($series[$key]["products"], $value);
											}
										} elseif ($currCategory == 'prices') {
											array_push($series[$key]["products"], $value);
										}
									}
								}
							}
						}
					}
				} else {
					for ($i = 0; $i < count($types); $i++) { $types[$i]["series"] = []; }

					foreach ($types as $index => $type) {
						if(json_decode($type["availCategories"], true)[0] == $currCategory) { //Test if the type is in the current category
							foreach ($series as $key => $serie) {
								if(json_decode($serie["availCategories"], true)[0] == $type["id"]) { //Test if the serie is in this type
									array_push($types[$index]["series"], $serie);
								}
							}
						}
					}
				}

				//Set the proper exchange rate
				if($selectedBranch["spectype"] == "can") {
					//Apply canadian exchange rate
					$exchangeRate = $userconfig["exchangerate"];
				} else {
					//Do not apply any rate
					$exchangeRate = 1;
				}

				function prettifyCashAmount($baseAmount, $exchangeRate) {
					return '$' . number_format(floatval($baseAmount) * floatval($exchangeRate), 2);
				}
			
				function applyDiscounts($amount, $discounts, $multipliers, $transport){
					$discounts = explode(',', $discounts);
					$multipliers = explode(',', $multipliers);
					$transport = explode(',', $transport);
			
					foreach ($discounts as $key => $value) { //Calculate discounts
						$value = trim($value);
						if(!empty($value) && $value != "-" && $value > 0) {
							$amount = floatval($amount) * (1 - floatval($value)/100);
						}
					}
					foreach ($multipliers as $key => $value) { //Calculate multipliers
						$value = trim($value);
						if(!empty($value) && $value != "-" && $value > 0) {
							$amount = floatval($amount) / (1 - floatval($value)/100);
						}
					}
					foreach ($transport as $key => $value) { //Calculate transport
						$value = trim($value);
						if(!empty($value) && $value != "-" && $value > 0) {
							$amount = floatval($amount) / (1 - floatval($value)/100);
						}
					}
					return $amount;
				}

				function calculatedPrice($amount, $discounts, $multipliers, $transport, $exchangeRate) {
					$amount = calculatedDiscount($amount, $discounts);
					$amount = calculatedMultipliers($amount, $multipliers);
					$amount = calculatedTransport($amount, $transport);

					$amount = floatval($amount) * floatval($exchangeRate);

					return formatPrice($amount);
				}
				function calculatedDiscount($amount, $discounts) {
					$discounts = explode(',', $discounts);
					foreach ($discounts as $discount) {
						$discount = trim($discount);
						if($discount != "" && $discount != "-" && $discount > 0) {
							$amount = floatval($amount) * (1 - floatval($discount)/100);
						}
					}

					return $amount;
				}
				function calculatedMultipliers($amount, $multipliers) {
					$multipliers = explode(',', $multipliers);
					foreach ($multipliers as $multiplier) {
						$multiplier = trim($multiplier);
						if($multiplier != "" && $multiplier != "-" && $multiplier > 0) {
							$amount = floatval($amount) / (1 - floatval($multiplier)/100);
						}
					}

					return $amount;
				}
				function calculatedTransport($amount, $transports) {
					$transports = explode(',', $transports);
					foreach ($transports as $transport) {
						$transport = trim($transport);
						if($transport != "" && $transport != "-" && $transport > 0) {
							$amount = floatval($amount) / (1 - floatval($transport)/100);
						}
					}

					return $amount;
				}
				function formatPrice($amount) {
					return '$' . number_format(floatval($amount), 2);
				}
				function getSpec($product, $spec) {
					$allSpecs = json_decode($product["details"], true);

					$finalAnswer = '-';
					foreach ($allSpecs as $element) {
						foreach ($element["specs"] as $key => $currspec) {
							foreach ($currspec["title"] as $key2 => $title) {
								if(strpos(strtolower($title), $spec)) {
									return $finalAnswer = $currspec["arg"][$selectedBranch["spectype"]];
								}
							}
						}
					}

					if($finalAnswer == "") {
						$finalAnswer = "-";
					}

					return $finalAnswer;
				}

      } else {
        header('Location: branchselection');
      }
    } else {
      header('Location: branchselection');
    }
	} else {
		header('Location: logout?err=NoSession');
	}

	$arr_browsers = ["Firefox", "Chrome", "Safari", "Opera", 
					"MSIE", "Trident", "Edge"];

	$agent = $_SERVER['HTTP_USER_AGENT'];

	$user_browser = '';
	foreach ($arr_browsers as $browser) {
		if (strpos($agent, $browser) !== false) {
				$user_browser = $browser;
				break;
		}	
	}

	switch ($user_browser) {
		case 'MSIE':
			$user_browser = 'ie';
			break;

		case 'Trident':
			$user_browser = 'ie';
			break;

		case 'Edge':
			$user_browser = 'ie';
			break;
	}
?>
<!DOCTYPE html>
<html lang="en" class="has-navbar-fixed-top">
<head>
  <title>IPG - <?= $lang["BranchSelection"]["PageTitle"] ?></title>
  <?php include_once('shared/head.php') ?>
	<link rel="stylesheet" type="text/css" href="assets/css/hero.css">
	<link rel="stylesheet" type="text/css" href="assets/css/products-ie.css">
	<!--[if lte IE 9]>
	<link rel="stylesheet" type="text/css" href="assets/css/products-ie9.css">
	<![endif]-->

	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="assets/css/products-ie8.css">
	<![endif]-->
</head>
<body>
  <div id="app">
  <?php include_once("shared/navbar.php") ?>
		<div class="is-multiline is-centered is-loading">
			<?php if($currCategory != null) { ?>
				<?php if($user_browser == 'ie') { 
						include_once('productsie.php');
					} else {
				?>
				<template v-if="filteredCategories.length > 0">
					<div class="column is-narrow" v-for="category in filteredCategories">
						<article class="message is-link">
							<div class="message-header">
								<p>{{ category.name }}</p>
							</div>
							<div class="message-body" style="font-size: .9rem; padding: .50em .50em;padding-bottom: 1.7em;">
								
								<?php if($currSerie != null || $currCategory == 'all') { ?>
									<div class="table_wrapper">
										<table class="table is-hoverable is-striped is-fullwidth">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<?php foreach ($branches as $branch) {
														if($branch["spectype"] == $selectedBranch["spectype"]) {
													?>
														<th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="2"><?= $branch["name"] ?></th>
													<?php } } ?>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
												<tr>
													<th><?= $lang["Products"]["Name"] ?></th>
													<th><?= $lang["Products"]["Price"] ?></th>
													<?php foreach ($branches as $branch) {
														if($branch["spectype"] == $selectedBranch["spectype"]) {
													?>
														<th style="text-align: center;"><?= $lang["Products"]["Stock"] ?></th>
														<th style="text-align: center;"><?= $lang["Products"]["Order"] ?></th>
													<?php } } ?>
													<th><?= $lang["Products"]["Description"] ?></th>
													<th><?= $lang["Products"]["SCFM"] ?></th>
													<th><?= $lang["Products"]["PSI"] ?></th>
													<th><?= $lang["Products"]["HP"] ?></th>
													<th><?= $lang["Products"]["Voltage"] ?></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="product in category.products">
													<td style="text-align: left;" scope="row"><a :href="'viewproduct?product=' + product.id + '&branch=<?= htmlspecialchars($_GET["branch"]) ?>'" class="has-text-link">{{ JSON.parse(product.maker).<?= $language ?>.trim() }} - {{ product.product_name.trim() }}</a></td></td>
													<td style="text-align: left;">{{ calculatedPrice(product.base_price, JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>], JSON.parse(product.multipliers)[<?= $selectedBranch["id"] ?>], product.transport) }}</td> 
													<?php foreach ($branches as $branch) {
														if($branch["spectype"] == $selectedBranch["spectype"]) {
													?>
														<td :class="{ 'is-success': (JSON.parse(product.in_stock)[<?= $branch["id"] ?>] >= 1) }">{{ JSON.parse(product.in_stock)[<?= $branch["id"] ?>] || 'N/A' }}</td>
														<td :class="{ 'is-warning': (JSON.parse(product.on_order)[<?= $branch["id"] ?>] >= 1) }">{{ JSON.parse(product.on_order)[<?= $branch["id"] ?>] || 'N/A' }}</td>
													<?php } } ?>
													<td style="text-align: left;">{{ JSON.parse(product.desc).<?= $language ?>.trim() }}</td>
													<td style="text-align: left;">{{ getSpec(product, 'Capacity') }}</td>
													<td style="text-align: left;">{{ getSpec(product, 'Pressure') }}</td>
													<td style="text-align: left;">{{ getSpec(product, 'Horsepower') }}</td>
													<td style="text-align: left;">{{ getSpec(product, 'Voltage') }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								<?php } elseif($currCategory == 'prices') { ?>
									<div class="table_wrapper">
										<table class="table is-hoverable is-striped is-fullwidth">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="4">List Price and Discount</th>
													<th></th>
													<th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="4">Exchange Rate and Profit</th>
												</tr>
												<tr>
													<th><?= $lang["Products"]["Name"] ?></th>
													<th><?= $lang["Products"]["Description"] ?></th>
													<th><?= $lang["Products"]["ListPrice"] ?></th>
													<th><?= $lang["Products"]["Discount1"] ?></th>
													<th><?= $lang["Products"]["Discount2"] ?></th>
													<th><?= $lang["Products"]["BeforeExchange"] ?></th>
													<th><?= $lang["Products"]["Transport"] ?></th>
													<th><?= $lang["Products"]["ExchangeRate"] ?></th>
													<th><?= $lang["Products"]["AfterExchange"] ?></th>
													<th><?= $lang["Products"]["ProfitMultiplier"] ?></th>
													<th><?= $lang["Products"]["SellingPrice"] ?></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="product in category.products">
													<td style="text-align: left;" scope="row"><a :href="'viewproduct?product=' + product.id + '&branch=<?= htmlspecialchars($_GET["branch"]) ?>'" class="has-text-link">{{ JSON.parse(product.maker).<?= $language ?>.trim() }} - {{ product.product_name.trim() }}</a></td></td>
													<td style="text-align: left;">{{ JSON.parse(product.desc).<?= $language ?>.trim() }}</td> 
													<td style="text-align: left;">{{ formatPrice(product.base_price) }}</td>
													<td style="text-align: left;">{{ JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>][0] }}%</td>
													<td style="text-align: left;">{{ JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>][1] }}%</td>
													<td style="text-align: left;">{{ formatPrice(calculatedDiscount(product.base_price, JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>])) }}</td>
													<td style="text-align: left;">{{ product.transport }}%</td>
													<td style="text-align: left;"><?= $exchangeRate ?></td>
													<td style="text-align: left;font-weight: bold;">{{ formatPrice((parseFloat(calculatedTransport(calculatedDiscount(product.base_price, JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>]), product.transport)) * parseFloat(<?= $exchangeRate ?>))) }}</td>
													<td style="text-align: left;">{{ JSON.parse(product.multipliers)[<?= $selectedBranch["id"] ?>] }}%</td>
													<td style="text-align: left;font-weight: bold;">{{ calculatedPrice(product.base_price, JSON.parse(product.discounts)[<?= $selectedBranch["id"] ?>], JSON.parse(product.multipliers)[<?= $selectedBranch["id"] ?>], product.transport) }}</td> 
												</tr>
											</tbody>
										</table>
									</div>
								<?php } else { ?>
									<div class="board-item">
										<!-- Loop through the `items` array from the current season to show each item -->
										<div class="board-item-content">
											<div class="row columns is-multiline">

												<div class="column is-2" v-for="product in category.products">
													<div class="card is-shady has-text-centered card-equal-height">
														<a style="text-decoration: none;" :href="'products?branch=<?= htmlspecialchars($_GET["branch"]) ?>&cat=<?= $currCategory ?>&serie='+ product.id">
															<div class="card-image card-equal-height">
																<figure class="image">
																	<img :src="product.img" style="width: 50%;margin: auto;margin-bottom: 0;padding-bottom: 0;" alt="Image">
																</figure>
															</div>
															<div class="card-content" style="padding-top: 0;">
																<div class="content" style="margin: 0;">
																	<h4>{{ JSON.parse(product.name).<?= $language ?> }}</h4>

																	<!-- <a :href="'products?branch=<?= htmlspecialchars($_GET["branch"]) ?>&cat='+ JSON.parse(product.availCategories)[0] +'&serie='+ product.id" class="button is-link is-outlined see_stock_btn"><?= $lang["BranchSelection"]["Select"] ?></a> -->
																</div>
															</div>
														</a>
													</div>
												</div>

											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</article>
					</div>
				</template>
				<template v-else>
					<div class="has-text-centered">
						<h4 class="has-text-danger"><?= $lang['Navbar']["Categories"]["NothingToShow"] ?></h4>
					</div>		
				</template>
				<?php } ?>
			</div>
			<?php } elseif($currCategory == null) { ?>
				<div class="columns is-multiline is-centered">
				<?php foreach ($categories as $key => $cat) { ?>
					
					<div class="column" style="flex: none;">
						<div class="card is-shady has-text-centered card-equal-height">
						<a href="products?branch=<?= $selectedBranch['id'] ?>&cat=<?= $cat["id"] ?>">
							<div class="card-image card-equal-height">
								<figure class="image">
									<img style="width: 200px;" src="<?= $cat["img"] ?>" alt="Image">
								</figure>
							</div>
							<div class="card-content">
								<div class="content">
									<h4><?= json_decode($cat["name"], true)[$language] ?></h4>
									<!-- <a href="products?branch=<?= $selectedBranch['id'] ?>&cat=<?= $cat["id"] ?>" class="button is-link is-outlined see_stock_btn"><?= $lang["BranchSelection"]["SeeStock"] ?></a> -->
								</div>
							</div>
						</a>
						</div>
					</div>
				<?php } ?>
				</div>
		<?php } ?>
  </div>
  <?php include_once('shared/scripts.php'); ?>
  <script type="text/javascript">
		function doesInclude(obj, value) {
			let isTrue = false;
			$.each(obj, function (index, element) { 
				if(element.toLowerCase().trim().includes(value)) {
					return isTrue = true;
				}
			});
			return isTrue;
		}
    var App = new Vue({
      el: "#app",
      data() {
        return {
          search: "",
						<?php if($currSerie != null || $currCategory == 'all' || $currCategory == 'prices') { ?>
							items: [ <?php foreach ($series as $type) { echo '{ name: "'.json_decode($type["name"], true)[$language].'", products: '.json_encode($type["products"]).' },'; } ?> ],
						<?php } else { ?>
							items: [ <?php foreach ($types as $cat) { echo '{ name: "'.json_decode($cat["name"], true)[$language].'", products: '.json_encode($cat["series"]).' },'; } ?> ],
						<?php } ?>
						exchangeRate: <?= $exchangeRate ?>,
				}
      }, 
      computed: {
        filteredCategories() {
          var _this = this
          //if(this.search.length === 0) return this.categories //Show everything (even unavailable)
					let search = unescape(_this.search.toLowerCase().trim()).split(/[ ]/gmi);
					for (let i = 0; i < search.length; i++) {
						search[i] = search[i].trim();
						if(search[i].length < 1) search.splice(i, 1);
					}
          let allcats = this.items.map(function(cat) {
            let mycat = {
              name: cat.name,
              products: cat.products.filter(function (product) {
								<?php if($currCategory != null && $currSerie != null) { ?>
									let string = (JSON.parse(product.maker).<?= $language ?> + " - " + product.product_name).toLowerCase();

									let trueCount = 0;
									$.each(search, function (i, value) { 
										//If maker
										if(doesInclude(JSON.parse(product.maker), value.trim())) {
											trueCount++;
										} else {
											if(doesInclude(JSON.parse(product.serie), value.trim())) {
												trueCount++;
											} else {
												if(doesInclude(JSON.parse(product.subserie), value.trim())) {
													trueCount++;
												} else {
													if(((JSON.parse(product.serie).<?= $language ?>).toLowerCase() + '-' + (JSON.parse(product.subserie).<?= $language ?>).toLowerCase()).includes(value.trim())) {
														trueCount++;
													} else {
														if(product.product_name.toLowerCase().includes(value.trim())) {
															trueCount++;
														} else {
															if(doesInclude(JSON.parse(product.type), value.trim())) {
																trueCount++;
															} else {

															}
														}
													}
												}
											}
										}
									});
								<?php } else if($currCategory != null && $currSerie == null) { ?>
									let trueCount = 0;
									$.each(search, function (i, value) { 
										//If maker
										<?php if($currCategory != 'prices') { ?>
											if(doesInclude(JSON.parse(product.name), value.trim())) {
												trueCount++;
											} else {

											}
										<?php } else { ?>
											if(product.product_name.toLowerCase().includes(value.trim())) {
												trueCount++;
											} else {
												if(product.maker.toLowerCase().includes(value.trim())) {
													trueCount++;
												} else {

												}
											}
										<?php } ?>
									});
								<?php } else { ?>
										let trueCount = 0;
										$.each(search, function (i, value) { 
										
											trueCount++;
									
									});
								<?php } ?>

								if(trueCount == search.length) {
									return true;
								}
              })
            }
						if(mycat.products.length > 0) {
							return mycat;
						}
          })
					
					allcats = allcats.filter(function( element ) {
						return element !== undefined;
					});
					return allcats;
        }
      },
      methods: {
        clearSearchField () {
          this.search = ''
        },
				calculatedPrice(amount, discounts, multipliers, transport) {
					amount = this.calculatedDiscount(amount, discounts);
					amount = this.calculatedMultipliers(amount, multipliers);
					amount = this.calculatedTransport(amount, transport);

					amount = parseFloat(amount) * parseFloat(this.exchangeRate);

					return this.formatPrice(amount);
				},
				calculatedDiscount(amount, discounts) {
					discounts = discounts.split(',');
					discounts.forEach(element => {
						element = element.trim();
						if(element != "" && element != "-" && element > 0) {
							amount = parseFloat(amount) * (1 - parseFloat(element)/100);
						}
					});

					return amount;
				},
				calculatedMultipliers(amount, multipliers) {
					multipliers = multipliers.split(',');
					multipliers.forEach(element => {
						element = element.trim();
						if(element != "" && element != "-" && element > 0) {
							amount = parseFloat(amount) / (1 - parseFloat(element)/100);
						}
					});

					return amount;
				},
				calculatedTransport(amount, transport) {
					transport = transport.split(',');
					transport.forEach(element => {
						element = element.trim();
						if(element != "" && element != "-" && element > 0) {
							amount = parseFloat(amount) / (1 - parseFloat(element)/100);
						}
					});

					return amount;
				},
				formatPrice(amount) {
					return '$ ' + parseFloat(amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
				},
				getSpec(product, spec) {
					allSpecs = JSON.parse(product.details);

					finalAnswer = '-';
					allSpecs.forEach(element => {
						$.each(element.specs, function(index2, currspec) {
							$.each(currspec.title, function( index, title ) {
								if(title.toLowerCase().includes(spec.toLowerCase())) {
									return finalAnswer = currspec.arg["<?= $selectedBranch["spectype"] ?>"];
								}
							});
						});
					});
					if(finalAnswer == "") {
						finalAnswer = "-";
					}

					return finalAnswer;
				}
      }
    });
  </script>
</body>
</html>