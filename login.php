<?php
  include('shared/config.php'); 

  if(isset($_GET["err"])) {
    $err = htmlspecialchars($_GET["err"]);
    foreach ($lang["Errors"] as $key => $value) {
      if($err == $key) {
        $error = $value;
        break;
      } else {
        $error = $lang["Errors"]["SQL"];
      }
    }
  }

  $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));

  if(isset($_SESSION["USER"]) && !empty($_SESSION["USER"])) {
    header("Location: branchselection");
  } else if(isset($_COOKIE["Username"]) && !empty($_COOKIE['Username']) && isset($_COOKIE["Password"]) && !empty($_COOKIE['Password'])) {
    loginUser($_COOKIE["Username"], $_COOKIE["Password"], $bdd, $lang, $rbac);
  }

  if(isset($_POST['username']))
  {
    $error = loginUser($_POST['username'], $_POST['password'], $bdd, $lang, $rbac);
    $capchat = checkForFailedAttempts($bdd, $userconfig);
  }

  function loginUser($username, $password, $bdd, $lang, $rbac){
    $username = htmlspecialchars($username);
    $password = htmlspecialchars($password);
    if(!empty($username) && !empty($password))
    {
      $requser = $bdd->prepare("SELECT * FROM users WHERE username = ?");
      $requser->execute(array(strtolower($username)));
      $userCount = $requser->rowCount();
      if($userCount == 1)
      {
        $user = $requser->fetch();
        if (password_verify($password, $user['password'])) { 
          $userRoles = $rbac->Users->allRoles($user['id']);

          $branchCount = 0;
          $lastBranchID = 0;
          foreach ($userRoles as $role) {
            $pos = strrpos($role["Title"], "branch_");
            if($pos === 0){
              //If it is a branch_<ID> permission and we are allowed
              //Increment the branchCount
              $lastBranchID = explode("branch_", $role["Title"]);
              $branchCount++;
            } else if($role["Title"] == "root") {
              $lastBranchID = 0;
              $branchCount += 2;
            }
          }

          $user["branchCount"] = $branchCount;
          //Set the onlyBranchID to the ID of the only branch that exist
          if($branchCount == 1) {
            $user["onlyBranchID"] = $lastBranchID[1];
          }

          if(isset($_POST['g-recaptcha-response'])) {
            $secretKey = '6LcXHGUUAAAAAJktQh-aAC8-apT-d_7zKJWJPW_c';
            $response = $_POST['g-recaptcha-response'];
            $remoteIp = $_SERVER['REMOTE_ADDR'];
        
        
            $reCaptchaValidationUrl = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$response&remoteip=$remoteIp");
            $result = json_decode($reCaptchaValidationUrl, TRUE);
        
            if($result['success'] == 1) {
              $_SESSION["USER"] = $user;

              //Insert the success event
              $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
              
              $details = [
                "username" => $username,
                "ip" => $_SERVER["REMOTE_ADDR"].' ('.$geo["geoplugin_city"].')'
              ];
              $insertEvent->execute(array("loginSuccess", json_encode($details), date("Y-m-d H:i:s")));

              header("Location: branchselection");
              die();
            } else {
              return $lang['Errors']['CapchatFailed'];
            }
          } else {
            $_SESSION["USER"] = $user;

            //Insert the success event
            $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
            $details = [
              "username" => $username,
              "ip" => $_SERVER["REMOTE_ADDR"].' ('.$geo["geoplugin_city"].')'
            ];
            $insertEvent->execute(array("loginSuccess", json_encode($details), date("Y-m-d H:i:s")));

            header("Location: branchselection");
            die();
          }
        }
        else
        {
          $insertLoginAttempt = $bdd->prepare("INSERT INTO failed_logins (ip_address, attempted) VALUES (?, ?)");
          $insertLoginAttempt->execute(array($_SERVER["REMOTE_ADDR"], date("Y-m-d H:i:s")));
          $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
          $details = [
            "username" => $username,
            "ip" => $_SERVER["REMOTE_ADDR"].' ('.$geo["geoplugin_city"].')'
          ];
          $insertEvent->execute(array("loginFailure", json_encode($details), date("Y-m-d H:i:s")));

          return $lang['Errors']['WrongPassword'];
        }
      }
      else
      {
        return $lang['Errors']['WrongUsername'];
      }
    }
    else
    {
      return $lang['Errors']['SomethingIsMissing'];
    }
  }

  function checkForFailedAttempts($bdd, $userconfig) {
    // array of throttling
    $throttle = array(($userconfig["attemptBefore_recapchat"] - 1) => 'recaptcha');

    // retrieve the latest failed login attempts
    $reqFailedAttempt = $bdd->prepare("SELECT MAX(attempted) AS attempted FROM failed_logins WHERE ip_address = ?");
    $reqFailedAttempt->execute(array($_SERVER["REMOTE_ADDR"]));
    if ($reqFailedAttempt->rowCount() > 0) {
        $row = $reqFailedAttempt->fetchAll();
        $latest_attempt = (int) date('U', strtotime($row[0]['attempted']));

        // get the number of failed attempts
        $reqAttemped = $bdd->prepare("SELECT COUNT(1) AS failed FROM failed_logins WHERE attempted > DATE_SUB(NOW(), INTERVAL 15 minute) AND ip_address = ?");
        $reqAttemped->execute(array($_SERVER["REMOTE_ADDR"]));
        if ($reqAttemped->rowCount() > 0) {
            // get the returned row
            $row = $reqAttemped->fetchAll();
            $failed_attempts = (int) $row[0]['failed'];
            // assume the number of failed attempts was stored in $failed_attempts
            krsort($throttle);
            foreach ($throttle as $attempts => $delay) {
                if ($failed_attempts > $attempts) {
                    // we need to throttle based on delay
                    if (is_numeric($delay)) {
                        $remaining_delay = $delay - (time() - $latest_attempt);
                        // output remaining delay
                        return $remaining_delay;
                    } else {
                        // code to display recaptcha on login form goes here
                        return 'recapchat';
                    }
                    break;
                }
            }        
        }
    }
  }

  function toHumanReadableTime($seconds){
    $zero    = new DateTime("@0");
    $offset  = new DateTime("@$seconds");
    $diff    = $zero->diff($offset);
    return sprintf("%02d ". $lang["Hours"] ."%02d ". $lang["Minutes"] ."%02d ".$lang["Seconds"], $diff->days * 24 + $diff->h, $diff->i, $diff->s);
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>IPG - <?= $lang["Login"]["PageTitle"] ?></title>
  <?php include_once('shared/head.php') ?>
  <link rel="stylesheet" type="text/css" href="assets/css/login.css">
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script>
    function onSubmit(token) {
      document.getElementById("loginform").submit();
    }
  </script>
</head>
<body>
<section class="hero is-success is-fullheight">
    <div class="hero-body">
      <div class="container has-text-centered">
        <div class="column is-4 is-offset-4">
          <h3 class="title has-text-grey"><?= $lang["Login"]["PageTitle"] ?></h3>
          <p class="subtitle has-text-danger "><?php if(isset($error)) { echo $error; } ?></p>
          <div class="box">
            <form method="POST" name="loginform" id="loginform" class="loginform">
              <div class="field">
                <div class="control">
                  <input class="input is-large" type="text" name="username" placeholder="<?= $lang['Login']['Username'] ?>" autofocus="">
                </div>
              </div>

              <div class="field">
                <div class="control">
                  <input class="input is-large" type="password" name="password" placeholder="<?= $lang['Login']['Password'] ?>">
                </div>
              </div>
              <button class="button is-block is-info is-large is-fullwidth <?php if(isset($capchat) && $capchat == 'recapchat') { echo 'g-recaptcha'; } ?>" name="loginform" <?php if(isset($capchat) && $capchat == 'recapchat') { echo 'data-sitekey="6LcXHGUUAAAAAERja8Ff67A3Td9tOeXkLl_584hC" data-callback="onSubmit"'; } ?>><?= $lang['Login']['PageTitle'] ?></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include_once("shared/scripts.php") ?>
</body>
</html>