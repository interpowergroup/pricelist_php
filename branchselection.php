<?php
	include('shared/config.php');
	
	if(isset($_SESSION["USER"])) {
		$user = $_SESSION["USER"];

		if($user["branchCount"] == 0) {
			header('Location: logout?err=NoBranches');
		} else if($user["branchCount"] == 1) {
			header('Location: products?branch='.$user["onlyBranchID"]);
		}

		$reqbranches = $bdd->query("SELECT * FROM branches");
		$branches = $reqbranches->fetchAll();
	} else {
		header('Location: logout?err=NoSession');
	}
?>
<!DOCTYPE html>
<html lang="en" class="has-navbar-fixed-top">
<head>
  <title>IPG - <?= $lang["BranchSelection"]["PageTitle"] ?></title>
	<?php include_once('shared/head.php') ?>
	<script src="assets/css/flexibility.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/hero.css">
	<link rel="stylesheet" type="text/css" href="assets/css/branchselection-ie.css">
	<!--[if lte IE 9]>
	<link rel="stylesheet" type="text/css" href="assets/css/branchselection-ie9.css">
	<![endif]-->
	
	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="assets/css/branchselection-ie8.css">
	<![endif]-->
</head>
<body>
  <?php include_once("shared/navbar.php") ?>
  <section class="container">
		<div class="intro column is-8 is-offset-2">
			<h2 class="title"><?= $lang["BranchSelection"]["PleaseSelect"] ?></h2>
			<p class="subtitle has-text-danger"><?php if(isset($error)) { echo $error; } ?></p>
		</div>
		<div class="columns is-multiline is-centered">
      <?php
        foreach ($branches as $value) {
          if($rbac->check('branch_'.$value["id"], $user["id"])) {
      ?>
        <div class="column is-3">
					<div class="card is-shady has-text-centered card-equal-height">
						<a href="products?branch=<?= $value['id'] ?>">
							<div class="card-image card-equal-height">
								<figure class="image">
									<img src="<?= $value["logo"] ?>" alt="Image">
								</figure>
							</div>
							<div class="card-content card-equal-height">
								<div class="content">
									<h4><?= $value["name"] ?></h4>
									<p><?= $value["location"] ?></p>
									<!-- <a href="products?branch=<?= $value['id'] ?>" class="button is-link is-outlined see_stock_btn"><?= $lang["BranchSelection"]["SeeStock"] ?></a> -->
								</div>
							</div>
						</a>
					</div>
				</div>
      <?php
          }
        }
      ?>
		</div>
	</section>
	<?php include_once('shared/scripts.php'); ?>
</body>
</html>