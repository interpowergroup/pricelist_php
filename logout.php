<?php

  include('shared/config.php');


  //Remove cookies
  if(isset($_COOKIE["Username"])) {
    unset($_COOKIE['Username']);
    setcookie('Username', null, -1, '/');
  }
  if(isset($_COOKIE["Password"])) {
    unset($_COOKIE['Password']);
    setcookie('Password', null, -1, '/');
  }
  session_destroy();

  //Let the query parameters follow throught the login page
  //Availlable queries are: 'err' and 'param'
  $queryString = "?";
  if(isset($_GET["err"])) {
    $queryString .= "err=".$_GET["err"];
  }
  if($queryString != "?") { $queryString .= "&"; }
  if(isset($_GET["param"])) {
    $queryString .= "param=".$_GET["param"];
  } else {
    $queryString = substr($queryString, 0, strlen($queryString) - 1);
  }
  if($queryString == "?") { $queryString = ""; }
  
  header('Location: '. ROOT_PATH .'login'.$queryString);
?>