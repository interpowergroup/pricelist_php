<?php
	include('shared/config.php');
	include('assets/misc/Converter/Convertor.php');

	//Create the converter and convertion array
	use Olifolkerd\Convertor\Convertor;
	$convertor = new Convertor();
	$allConvType = $convertor->getUnits("m"); //Distance
	$allConvType = array_merge($convertor->getUnits("kg"), $allConvType); //Weight
	$allConvType = array_merge($convertor->getUnits("deg"), $allConvType); //Rotation
	$allConvType = array_merge($convertor->getUnits("pa"), $allConvType); //Pressure
	$allConvType = array_merge($convertor->getUnits("l"), $allConvType); //Volume
	

	if(isset($_SESSION["USER"])) {
		$user = $_SESSION["USER"];
		$breadcrumb = 3;

		if($user["branchCount"] == 0) {
			header('Location: logout?err=NoBranches');
		}
    
    $reqbranches = $bdd->query("SELECT * FROM branches");
    $branches = $reqbranches->fetchAll();
    $reqcategories = $bdd->query("SELECT * FROM products_categories");
		$categories = $reqcategories->fetchAll();
		$reqspecset = $bdd->query("SELECT * FROM products_specset");
    $specset = $reqspecset->fetchAll();

    if(isset($_GET["product"]) && !empty($_GET["product"]) && is_numeric($_GET["product"])) {
      if(isset($_GET["branch"]) && !empty($_GET["branch"]) && is_numeric($_GET["branch"])) {
				if(!$rbac->check('branch_'.htmlspecialchars($_GET["branch"]), $user["id"])) {
					header('Location: branchselection');
				}
        $reqbranch = $bdd->prepare("SELECT * FROM branches WHERE id = ?");
        $reqbranch->execute(array(htmlspecialchars($_GET["branch"])));
        if($reqbranch->rowCount() == 1) {
          $selectedBranch = $reqbranch->fetch();
        }

        $reqproducts = $bdd->prepare("SELECT prods.*, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, 
                                      series.name AS `serie`, subseries.name AS `subserie`, details.details, opts.options, docs.docs, inv.*,
																			series.discounts AS `discounts`, series.multipliers AS `multipliers`, series.transport AS `transport`,
                                      series.img AS `serie_img`, cats.img AS `category_img`, types.img AS `type_img`, makers.img AS `maker_img` 
                                      FROM products AS prods 
																			RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																			RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																			RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																			RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																			RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																			RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																			RIGHT JOIN products_options AS opts ON prods.id = opts.product_id 
																			RIGHT JOIN products_docs AS docs ON prods.id = docs.product_id 
																			RIGHT JOIN products_inventory AS inv ON prods.id = inv.product_id 
																			WHERE prods.id = ?");
				$reqproducts->execute(array(htmlspecialchars($_GET["product"])));
				if($reqproducts->rowCount() == 0) {
					header('Location: branchselection');
				}
				$product = $reqproducts->fetch();

				$activeBranchHasRightToProduct = false;
				foreach (json_decode($product["branches"], true) as $key => $branch) {
					if($key == htmlspecialchars($_GET["branch"]) && $branch == 'true') {
						$activeBranchHasRightToProduct = true;
						break;
					}
				}
				if(!$activeBranchHasRightToProduct) {
					header('Location: branchselection');
				}
				
				$reqoptions = $bdd->query("SELECT * FROM products_optionset");
				$alloptions = $reqoptions->fetchAll();

        function getProductImage($product) {
					if($product["img"] != "") {
						return $product["img"];
					}
					if($product["serie_img"] != "") {
						return $product["serie_img"];
					}
					if($product["type_img"] != "") {
						return $product["type_img"];
					}
					if($product["category_img"] != "") {
						return $product["category_img"];
          }
          if($product["maker_img"] != "") {
						return $product["maker_img"];
					}
					return ROOT_PATH.'assets/img/no-image.png';
        }
        
        $product["img"] = getProductImage($product);

				$options = json_decode($product["options"], true);
				$details = json_decode($product["details"], true);
				$docs = json_decode($product["docs"], true);

				$currCategory = $product["category_id"];
				$currSerie = $product["serie_id"];
				$currCategoryName = json_decode($product["cat"], true)[$language];
				$currSerieName = json_decode($product["serie"], true)[$language];
				$discounts = json_decode($product["discounts"], true)[$selectedBranch["id"]];
				$multipliers = json_decode($product["multipliers"], true)[$selectedBranch["id"]];
				$transport = $product["transport"];

				foreach ($details as $key3 => $optionset) {
					foreach ($specset as $index => $alloptionset) {
						if($alloptionset["id"] == $optionset["id"]) {
							$details[$key3]["name"] = json_decode($alloptionset["name"], true);
							$details[$key3]["specs"] = json_decode($alloptionset["specs"], true);
							foreach ($details[$key3]["specs"] as $key2 => $value) {
								if(array_key_exists($key2, $optionset["args"])) {
									$details[$key3]["specs"][$key2]["arg"] = $optionset["args"][$key2];
								} else {
									$details[$key3]["specs"][$key2]["arg"] = ["can" => "", "us" => ""];
								}
							}
						}
					}
				}

				$product["base_price"] = applyDiscounts($product["base_price"], $discounts, $multipliers, $transport);

				//Get all enforced options
				$enforceCount = 0;
				$optPrice = 0;
				foreach ($options as $key => $optionset) {
					foreach ($alloptions as $index => $alloptionset) {
						if($alloptionset["id"] == $optionset["id"]) {
							$options[$key]["name"] = json_decode($alloptionset["name"], true);
							$options[$key]["options"] = json_decode($alloptionset["options"], true);
							$options[$key]["relation"] = $alloptionset["relation"];
							foreach ($options[$key]["options"] as $key2 => $value) {
								if(array_key_exists($key2, $optionset["args"]) && $optionset["args"][$key2] != "") {
									$options[$key]["options"][$key2]["arg"] = applyDiscounts($optionset["args"][$key2], $discounts, $multipliers, $transport);
									if(isset($options[$key]["options"][$key2]["isDefault"]) && $options[$key]["options"][$key2]["isDefault"] == 'true') {
										$optPrice += $options[$key]["options"][$key2]["arg"];
									}
								} else {
									$options[$key]["options"][$key2]["arg"] = "notset";
								}
							}
						}
					}
				}

      } else {
        header('Location: branchselection');
      }
    } else {
      header('Location: branchselection');
    }
	} else {
		header('Location: logout?err=NoSession');
	}

	if($selectedBranch["spectype"] == "can") {
		//Apply canadian exchange rate
		$exchangeRate = $userconfig["exchangerate"];
	} else {
		//Do not apply any rate
		$exchangeRate = 1;
	}

	function prettifyCashAmount($baseAmount, $exchangeRate) {
		return '$' . number_format(floatval($baseAmount) * floatval($exchangeRate), 2);
	}

	function applyDiscounts($amount, $discounts, $multipliers, $transport){
		$discounts = explode(',', $discounts);
		$multipliers = explode(',', $multipliers);
		$transport = explode(',', $transport);

		foreach ($discounts as $key => $value) { //Calculate discounts
			$value = trim($value);
			if(!empty($value) && $value != "-" && $value > 0) {
				$amount = floatval($amount) * (1 - floatval($value)/100);
			}
		}
		foreach ($multipliers as $key => $value) { //Calculate multipliers
			$value = trim($value);
			if(!empty($value) && $value != "-" && $value > 0) {
				$amount = floatval($amount) / (1 - floatval($value)/100);
			}
		}
		foreach ($transport as $key => $value) { //Calculate transport
			$value = trim($value);
			if(!empty($value) && $value != "-" && $value > 0) {
				$amount = floatval($amount) / (1 - floatval($value)/100);
			}
		}
		return $amount;
	}
?>
<!DOCTYPE html>
<html lang="en" class="has-navbar-fixed-top">
<head>
  <title>IPG - <?= $lang["BranchSelection"]["PageTitle"] ?></title>
  <?php include_once('shared/head.php') ?>
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/hero.css"> -->
  <link rel="stylesheet" type="text/css" href="assets/css/toggle.css">
  <style type="text/css">
    html,
    body {
      background: #EFF3F4;
      font-family: 'Open Sans', serif;
    }
    .hero-body img {
      padding: 5px;
      border: 1px solid #ccc;
		}
		.removeItem {
			text-decoration: none !important;
		}
		.removeItem:hover {
			background-color: #ff3860 !important;
		}
		.addItem {
			text-decoration: none !important;
		}
		.addItem:hover {
			background-color: #23d160 !important;
		}
		.tag {
			font-size: 0.9rem !important;
		}
  </style>
</head>
<body>
  <div id="app">
  <?php include_once("shared/navbar.php") ?>
    <section class="hero is-medium is-bold">
    
      <div class="hero-body" style="padding: 0;">
        <div class="container has-text-centered">
          <div class="columns is-vcentered">
            <div class="column is-3 is-offset-1">
              <figure class="image">
                <img src="<?= $product["img"] ?>" alt="Image">
              </figure>
            </div>
            <div class="column is-6 is-offset-1">
              <h1 class="title is-2">
                <?= json_decode($product["maker"], true)[$language] . ' ' . $product["product_name"] ?>
              </h1>
              <h2 class="subtitle">
                <?= json_decode($product["desc"], true)[$language] ?>
              </h2>
              <h2 class="subtitle">
								<?= $lang["ProductViewer"]["BasePrice"] ?>: <span class="base-price has-text-weight-bold"><?= prettifyCashAmount($product["base_price"], $exchangeRate) ?></span>
								<br>
								<span class="options-price">
									<span class="text"><?= $lang["ProductViewer"]["OptionsPrice"]["Opts"] ?>: </span>
									<span class="opts has-text-weight-bold"><?= prettifyCashAmount($optPrice, $exchangeRate) ?></span>
								</span>
								<br>
								<span class="options-price">
									<span class="text"><?= $lang["ProductViewer"]["OptionsPrice"]["Final"] ?>: </span>
									<span class="price has-text-weight-bold"><?= prettifyCashAmount(floatval($product["base_price"]) + floatval($optPrice), $exchangeRate) ?></span>
								</span>
							</h2>
							<h2 class="is-2">
								<div class="level">
								
								<div class="field is-grouped is-grouped-multiline" style="margin:auto;">
									<?php if($rbac->check('edit_stock', $user["id"])) { ?>
										<div class="tag is-dark"><?= $lang["ProductViewer"]["InStock"] ?></div>
										<div class="control" style="margin-left: 5px;">
											<div class="tags has-addons">
												<?php 
													$stockCount = json_decode($product["in_stock"], true)[$selectedBranch["id"]];
												?>
												<a class="tag fa fa-minus is-dark removeItem" data-productName="<?= $product["product_name"] ?>" data-productID="<?= $product["id"] ?>" data-type="stock"></a>
												<span class="tag is-info inStockItems"><?= $stockCount ?></span>
												<a class="tag fa fa-plus is-dark addItem" data-productName="<?= $product["product_name"] ?>" data-productID="<?= $product["id"] ?>" data-type="stock"></a>
											</div>
										</div>

										<div class="tag is-dark" style="margin-left: 20px;"><?= $lang["ProductViewer"]["OnOrder"] ?></div>
										<div class="control" style="margin-left: 5px;">
											<div class="tags has-addons">
												<a class="tag fa fa-minus is-dark removeItem" data-productName="<?= $product["product_name"] ?>" data-productID="<?= $product["id"] ?>" data-type="order"></a>
												<span class="tag is-info onOrderItems"><?= json_decode($product["on_order"], true)[$selectedBranch["id"]] ?></span>
												<a class="tag fa fa-plus is-dark addItem" data-productName="<?= $product["product_name"] ?>" data-productID="<?= $product["id"] ?>" data-type="order"></a>
											</div>
										</div>
									<?php } else { ?>
										<div class="control" style="margin-left: 5px;">
											<div class="tags has-addons">
												<?php 
													$stockCount = json_decode($product["in_stock"], true)[$selectedBranch["id"]];
												?>
												<div class="tag is-dark"><?= $lang["ProductViewer"]["InStock"] ?></div>
												<span class="tag is-info inStockItems"><?= $stockCount ?></span>
											</div>
										</div>

										<div class="control" style="margin-left: 5px;">
											<div class="tags has-addons">
												<div class="tag is-dark" style="margin-left: 20px;"><?= $lang["ProductViewer"]["OnOrder"] ?></div>
												<span class="tag is-info onOrderItems"><?= json_decode($product["on_order"], true)[$selectedBranch["id"]] ?></span>
											</div>
										</div>
									<?php } ?>
								</div>
							</h2>
              <!-- <p class="has-text-centered">
                <a class="button is-medium is-info is-outlined">Learn more</a>
              </p> -->
            </div>
          </div>
					<div class="is-multiline is-centered">
						<?php if(count($options) > 0) { ?>
							<div class="column is-narrow">
								<article class="message is-link">
									<div class="message-header">
										<p><?= $lang["ProductViewer"]["Options"] ?></p>
									</div>
									<div class="message-body">
										<div class="board-item">
											<div class="board-item-content">
												<div class="row is-multiline" style="justify-content: center;">
													<?php 
														foreach ($options as $optionset) { 
															if($optionset["relation"] == "or") {
													?>
														<div class="field is-horizontal" style="justify-content: center;">
                              <div class="field-label is-normal" style="flex-grow: unset;flex-basis: unset;padding-top: 0;">
                                <label class="label" style="font-size: 1rem !important;" for="option_<?= $optionset["id"] ?>"><?= $optionset["name"][$language] ?></label>
                              </div>
                              <div class="field-body" style="flex-grow: unset;flex-basis: unset;">
                                <div class="field">
    																<div class="select">
																		<?php $defaultLastVal = 0; foreach ($optionset["options"] as $option) { if($option["isDefault"] == 'true') { $defaultLastVal = $option["arg"]; } } ?>

																			<select class="select options option" data-lastval="<?= $defaultLastVal ?>" name="option_<?= $optionset["id"] ?>" id="option_<?= $optionset["id"] ?>">
																				<?php foreach ($optionset["options"] as $option) { ?>
																					<?php if($option["arg"] != "notset" || $option["arg"] === floatval(0)) { ?>
																						<option class="option" value="<?= $option["arg"]?>" <?php if($option["isDefault"] == 'true') { echo 'selected'; } ?>><?= $option["title"][$language] ?> (<?= prettifyCashAmount($option["arg"], $exchangeRate) ?>)</option>
																					<?php } else { ?>
																						<option class="option" value="0" disabled><?= $option["title"][$language] ?> (<?= $lang["Invalid"] ?>)</option>
																					<?php } ?>
																				<?php } ?>
																			</select>
                                 		</div>
                                </div>
                              </div>
                            </div>
													<?php } elseif ($optionset["relation"] == "and") { ?>
														<div class="field is-horizontal" style="justify-content: center;">
                              <div class="field-label is-normal" style="flex-grow: unset;flex-basis: unset;padding-top: 0;">
                                <label class="label" style="font-size: 1rem !important;" for="option_<?= $optionset["id"] ?>"><?= $optionset["name"][$language] ?></label>
                              </div>
                              <div class="field-body is-horizontal" style="flex-grow: unset;flex-basis: unset;">
                                <div class="field">
																	<?php foreach ($optionset["options"] as $option) { ?>
																		<div class="checkbox">
																			<?php if($option["arg"] != "notset" || $option["arg"] === floatval(0)) { ?>
																				<input type="checkbox" class="options checkbox" value="<?= $option["arg"] ?>" name="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>" id="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>" <?php if($option["isDefault"] == 'true') { echo 'checked '; } if($option["isEnforced"] == 'true') { echo ' disabled'; }  ?>><label for="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>"> <?= $option["title"][$language] ?> (<?= prettifyCashAmount($option["arg"], $exchangeRate) ?>)</label>
																			<?php } else { ?>
																				<input type="checkbox" class="options checkbox" value="0" name="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>" id="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>" disabled><label for="option_<?= $optionset["id"] ?>_<?= $option["title"][$language] ?>"> <?= $option["title"][$language] ?> ( <?= $lang["Invalid"] ?> )</label>
																			<?php } ?>
																		</div>
																	<?php } ?>
                                </div>
                              </div>
                            </div>
														<?php } ?>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</article>
							</div>
						<?php } ?>
						<div class="column is-narrow">
							<article class="message is-link">
								<div class="message-header">
									<p><?= $lang["ProductViewer"]["Specs"] ?></p>
								</div>
								<div class="message-body">
									<div class="board-item">
										<div class="board-item-content">
											<div class="row is-vertical-center" style="justify-content: center;display: flex;">
												<?php if(count($details) > 0) { ?>
													<table class="table is-striped is-hoverable" style="width: 100%;">
														<tbody>
															<?php foreach ($details as $bigdetail) { foreach ($bigdetail["specs"] as $detail) { ?>
																<tr>
																	<th scope="row"><?= $detail["title"][$language] ?></th>
																	<td style="text-align: right;"><?php
																			//Show either the spec as plain text or the spec and the converted value
																			$specOrig = $detail["arg"][$selectedBranch["spectype"]];
																			$spec = explode(' ', $specOrig);
																			if(count($spec) == 2 && is_numeric($spec[0]) && in_array($spec[1], $allConvType)) {
																				$inverted = $convertor->from($spec[0], $spec[1]);
																				echo $specOrig . ' ('.$convertor->to($inverted).' '.$inverted.')';
																			} else {
																				echo $specOrig;
																			}
																		?></td>
																</tr>
															<?php } } ?>
														</tbody>
													</table>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</article>
						</div>
						<div class="column is-narrow">
							<article class="message is-link">
								<div class="message-header">
									<p><?= $lang["ProductViewer"]["Documents"] ?></p>
								</div>
								<div class="message-body">
									<div class="board-item">
										<div class="board-item-content">
											<div class="row columns is-multiline" style="justify-content: center;">
												<?php if(count($docs) > 0) { ?>
													<?php foreach ($docs as $doc) { ?>
														<a style="margin-left: 30px;" class="button is-link is-outlined" target="_blank" href="<?= $doc["file"] ?>"><?= $doc["name"][$language] ?></a>
													<?php } ?>
												<?php } else { ?>
													<h3><?= $lang["ProductViewer"]["NoDocuments"] ?></h3>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</article>
						</div>
					</div>

        </div>
      </div>

    </section>
  </div>
  <?php include_once('shared/scripts.php'); ?>
  <script type="text/javascript">
		var currOptionsPrice = <?= $optPrice ?>;
		var productPrice = <?= $product["base_price"] ?>;
		var currStock = <?= $product["in_stock"] ?>;
		var currOrder = <?= $product["on_order"] ?>;
		var exchangeRate = <?= $exchangeRate ?>
		/**
		 * Number.prototype.format(n, x)
		 * 
		 * @param integer n: length of decimal
		 * @param integer x: length of sections
		 */
		function format(element, n, x) {
				var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
				return element.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
		}

		function prettifyCashAmount(amount) {
			return '$' + format(parseFloat(amount) * parseFloat(exchangeRate), 2);
		}
		

		$('.options').change(function() {
			if($(this).val() != "none") {
				$('#option_none').prop('checked', false);
				if($(this).hasClass('checkbox') && $(this).is(':checked')) {
					currOptionsPrice += parseFloat($(this).val());
				} else if($(this).hasClass("option")) {
					currOptionsPrice -= parseFloat($(this).attr('data-lastval'));
					$(this).attr('data-lastval', $(this).val());
					currOptionsPrice += parseFloat($(this).val());
				} else {
					currOptionsPrice -= parseFloat($(this).val());
				}
				$('.options-price .opts').text(prettifyCashAmount(currOptionsPrice));
				$('.options-price .price').text(prettifyCashAmount(parseFloat(productPrice) + parseFloat(currOptionsPrice)));
			} else {
				$('.options-price .opts').text('N/A');
				$('.options-price .price').text(prettifyCashAmount(productPrice));
				$('input[name="options"]').each(function (index, element) {
					$(this).prop('checked', false);
				});
				currOptionsPrice = <?= $optPrice ?>;
				$(this).prop('checked', true);
			}
		});

		function updateInventory(productID, stock, type) {
			let data = {
				'userID': "<?= $user["id"] ?>",
				"userPassword": "<?= $user["password"] ?>",
				'productID': productID,
				'data': {
					'type': 'product_inventory_update',
					'stock': stock,
					'inventory': type
				}	
			}
			$.ajax({
				type: "POST",
				url: "<?= ROOT_PATH ?>admin/ajax/changeproduct",
				data: data,
				dataType: "json",
				success: function (response) {
					if(response.type == 'success') {
						if(type == 'stock') {
							$('.inStockItems').text(stock[<?= $selectedBranch["id"] ?>]);
						} else if(type == 'order') {
							$('.onOrderItems').text(stock[<?= $selectedBranch["id"] ?>]);
						}
					}
				}
			}).fail(function( jqXHR, textStatus ) {
				alert(`Request failed: ${textStatus}`)
			});
		}

		$('.addItem').click(function() {
			let productName = $(this).attr('data-productName');
			let productID = $(this).attr('data-productID');
			let type = $(this).attr('data-type');
			let productAmount = 0;
			if(type == 'stock') {
				productAmount = $('.inStockItems').text();
			} else if(type == 'order') {
				productAmount = $('.onOrderItems').text();
			}

			swal({
				title: `<?= $lang["ProductViewer"]["Add"] ?> ${productName}`,
				text: `${productAmount} + 0 = ${productAmount}`,
				input: 'number',
				onOpen: () => {
					$('input').on('keyup', function(e) {
						if($(this).val() == "") {
							$('#swal2-content').text(`${productAmount} + 0 = ${productAmount}`);
							return;
						}
						if($(this).val() < 0) $(this).val(0);

						$('#swal2-content').text(`${productAmount} + ${parseInt($(this).val())} = ${parseInt(productAmount, 10) + parseInt($(this).val(), 10)}`);
					});
				},
			}).then((value) => {
				if(value && typeof value.value != 'undefined') {
					if(value.value > 0) {
						let amount = parseInt(value.value, 10);
						if(type == 'stock') {
							currStock[<?= $selectedBranch["id"] ?>] = parseInt(currStock[<?= $selectedBranch["id"] ?>], 10) + amount;
							updateInventory(productID, currStock, type);
						} else if(type == 'order') {
							currOrder[<?= $selectedBranch["id"] ?>] = parseInt(currOrder[<?= $selectedBranch["id"] ?>], 10) + amount;
							updateInventory(productID, currOrder, type);
						}
					}
					if(value.value > 1) {
						//More than one
						swal({
							title: `${value.value} ${productName} <?= $lang["ProductViewer"]["whereAdded"] ?> <?= $selectedBranch["name"] ?>`,
							type: 'success',
						});
					} else {
						if(value.value == 1) {
							//Only one
							swal({
								title: `${value.value} ${productName} <?= $lang["ProductViewer"]["wasAdded"] ?> <?= $selectedBranch["name"] ?>`,
								type: 'success',
							});
						} else {
							if(value.value != 0) {
								swal({
									title: `<?= $lang["ProductViewer"]["CannotEnterNegativeValue"] ?>`,
									type: 'error',
								});
							}
						}
					}
				}
			});
		});
		$('.removeItem').click(function() {
			let productName = $(this).attr('data-productName');
			let productID = $(this).attr('data-productID');
			let type = $(this).attr('data-type');
			let productAmount = 0;
			if(type == 'stock') {
				productAmount = $('.inStockItems').text();
			} else if(type == 'order') {
				productAmount = $('.onOrderItems').text();
			}

			swal({
				title: `<?= $lang["ProductViewer"]["Remove"] ?> ${productName}`,
				text: `${productAmount} - 0 = ${productAmount}`,
				input: 'number',
				onOpen: () => {
					$('input').on('keyup', function(e) {
						if($(this).val() == "") {
							$('#swal2-content').text(`${productAmount} + 0 = ${productAmount}`);
							return;
						}
						if($(this).val() < 0) $(this).val(0);
						if($(this).val() > parseInt(productAmount, 10)) $(this).val(parseInt(productAmount, 10));

						$('#swal2-content').text(`${productAmount} - ${parseInt($(this).val())} = ${parseInt(productAmount, 10) - parseInt($(this).val(), 10)}`);
					});
				},
			}).then((value) => {
				if(value && typeof value.value != 'undefined') {
					if(value.value > 0) {
						let amount = parseInt(value.value, 10);
						if(type == 'stock') {
							currStock[<?= $selectedBranch["id"] ?>] = parseInt(currStock[<?= $selectedBranch["id"] ?>], 10) - amount;
							updateInventory(productID, currStock, type);
						} else if(type == 'order') {
							currOrder[<?= $selectedBranch["id"] ?>] = parseInt(currOrder[<?= $selectedBranch["id"] ?>], 10) - amount;
							updateInventory(productID, currOrder, type);
						}
					}
					if(value.value > 1) {
						//More than one
						swal({
							title: `${value.value} ${productName} <?= $lang["ProductViewer"]["whereRemoved"] ?> <?= $selectedBranch["name"] ?>`,
							type: 'success',
						});
					} else {
						if(value.value == 1) {
							//Only one
							swal({
								title: `${value.value} ${productName} <?= $lang["ProductViewer"]["wasRemoved"] ?> <?= $selectedBranch["name"] ?>`,
								type: 'success',
							});
						} else {
							if(value.value != 0) {
								swal({
									title: `<?= $lang["ProductViewer"]["CannotEnterNegativeValue"] ?>`,
									type: 'error',
								});
							}
						}
					}
				}
			});
		});
  </script>
</body>
</html>