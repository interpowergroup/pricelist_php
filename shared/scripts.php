<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
<!-- jQuery -->
<script src="<?= ROOT_PATH ?>assets/js/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<!-- nav -->
<script src="<?= ROOT_PATH ?>assets/js/jquery.scrollTo.js"></script>
<script src="<?= ROOT_PATH ?>assets/js/jquery.nav.js"></script>
<!-- localScroll -->
<script src="<?= ROOT_PATH ?>assets/js/jquery.localScroll.js"></script>
<!-- bootstrap -->
<script src="<?= ROOT_PATH ?>assets/js/bootstrap.js"></script>
<!-- prettyPhoto -->
<script src="<?= ROOT_PATH ?>assets/js/jquery.prettyPhoto.js"></script>
<!-- Works scripts -->
<script src="<?= ROOT_PATH ?>assets/js/isotope.js"></script>
<!-- flexslider -->
<script src="<?= ROOT_PATH ?>assets/js/jquery.flexslider.js"></script>
<!-- inview -->
<script src="<?= ROOT_PATH ?>assets/js/inview.js"></script>
<!-- animation -->
<script src="<?= ROOT_PATH ?>assets/js/animate.js"></script>
<!-- custom functions -->
<script src="<?= ROOT_PATH ?>assets/js/custom.js"></script>
<!-- bulma -->
<script async type="text/javascript" src="<?= ROOT_PATH ?>assets/js/bulma.js"></script>
<!-- sweetalert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.25.6/dist/sweetalert2.all.min.js"></script>
<!-- Vu.js -->
<!-- <script src="<?= ROOT_PATH ?>assets/js/vue.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<!-- bootstrap and bootrap-multiselect -->
<script type="text/javascript" src="<?= ROOT_PATH ?>assets/js/bootstrap-multiselect.js"></script>