<!-- START NAV -->
<nav class="navbar is-fixed-top">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="<?= ROOT_PATH ?>">
        <img src="<?php if(isset($selectedBranch)) { echo $selectedBranch['logo']; } else { echo ROOT_PATH .'assets/img/compagny_logo/logo_ipg.png'; } ?>" alt="Logo">
      </a>
      <span class="navbar-burger burger" data-target="navbarMenu">
        <span></span>
        <span></span>
        <span></span>
      </span>
    </div>
    <div id="navbarMenu" class="navbar-menu">
      <div class="navbar-start">
        
      </div>
      <div class="navbar-end">
        <?php if(isset($selectedBranch)) { ?>
          <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link"><?= $lang["Navbar"]["Categories"]["Header"] ?></a>
            <div class="navbar-dropdown">
                <a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>" class="navbar-item <?php if($currCategory['id'] == null) { echo 'is-active'; } ?>">
                  <?= $lang["Navbar"]["Categories"]["All"] ?>
                </a>
              <?php foreach ($categories as $category) { ?>
                <a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>&cat=<?= $category["id"] ?>" class="navbar-item <?php if($currCategory['id'] == $category['id']) { echo 'is-active'; } ?>">
                  <?= json_decode($category["name"], true)[$language] ?>
                </a>
              <?php } ?>
              <hr class="navbar-divider">
                <a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>&cat=all" class="navbar-item <?php if($currCategory['id'] == $category['id']) { echo 'is-active'; } ?>">
                  <?= $lang["Navbar"]["Categories"]["AllProducts"] ?>
                </a>
                <a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>&cat=prices" class="navbar-item <?php if($currCategory['id'] == $category['id']) { echo 'is-active'; } ?>">
                  <?= $lang["Navbar"]["Categories"]["AllPrices"] ?>
                </a>
            </div>
          </div>
          <?php if($user["branchCount"] > 1) { ?>
            <div class="navbar-item has-dropdown is-hoverable">
              <a class="navbar-link"><?= $lang["Navbar"]["Branch"] ?></a>
              <div class="navbar-dropdown">
                <?php 
                  foreach ($branches as $key => $value) { 
                    if($rbac->check('branch_'.$value["id"], $user["id"])) {
                ?>
                  <a href="<?= ROOT_PATH ?>products?branch=<?= $value['id'] ?><?php if($currCategory != null) { echo '&cat='.$currCategory; } ?>" class="navbar-item <?php if($selectedBranch['id'] == $value['id']) { echo 'is-active'; } ?>">
                    <?= $value["name"] ?>
                  </a>
                <?php 
                    } 
                  }
                ?>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
        
				<?php 
					$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
					if (strpos($url,'admin') !== false) {
				?>
					<a href="<?= ROOT_PATH ?>branchselection" class="navbar-item">
						<?= $lang["BranchSelection"]["PageTitle"] ?>
					</a>
				<?php } ?>
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">
            <?= $lang["Navbar"]["Account"]["MyAccount"] ?>
          </a>
          <div class="navbar-dropdown">
            <?php if($rbac->check('view_admin_panel', $user["id"])) { ?>
              <a href="<?= ROOT_PATH ?>admin" class="navbar-item">
                <?= $lang["Navbar"]["Account"]["Admin"] ?>
              </a>
            <?php } ?>
						<a class="navbar-item changelang">
              <?= $lang["Navbar"]["Language"] ?>: <?= $lang[$language] ?>
            </a>
            <hr class="navbar-divider">
            <a href="<?= ROOT_PATH ?>logout" class="navbar-item">
              <?= $lang["Navbar"]["Account"]["Logout"] ?>
            </a>
          </div>
        </div>
        <?php if(isset($selectedBranch)) { ?>
          <div class="navbar-item">
            <input class="input" type="search" v-model="search" autocomplete="off" placeholder="<?= $lang["Navbar"]["Search"] ?>">
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</nav>
<?php if(isset($breadcrumb)) { ?>
  <nav class="breadcrumb is-centered" aria-label="breadcrumbs">
    <ul>
      <?php 
        //0 = Selection
        //1 = Category
        //2 = Serie
        //3 = Product
      ?>
      <?php if($breadcrumb >= 0) { ?>
        <li class="<?php if($breadcrumb == 0) echo 'is-active'; ?>"><a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>"><?= $selectedBranch["name"] ?></a></li>
      <?php } ?>
      <?php if($breadcrumb >= 1) { ?>
        <li class="<?php if($breadcrumb == 1) echo 'is-active'; ?>"><a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>&cat=<?= $currCategory ?>"><?= $currCategoryName ?></a></li>
      <?php } ?>
      <?php if($breadcrumb >= 2) { ?>
        <li class="<?php if($breadcrumb == 2) echo 'is-active'; ?>"><a href="<?= ROOT_PATH ?>products?branch=<?= $selectedBranch["id"] ?>&cat=<?= $currCategory ?>&serie=<?= $currSerie ?>"><?= $currSerieName ?></a></li>
      <?php } ?>
      <?php if($breadcrumb >= 3) { ?>
        <li class="<?php if($breadcrumb == 3) echo 'is-active'; ?>"><a href="<?= ROOT_PATH ?>viewproduct?product=<?= $product["id"] ?>&branch=<?= $selectedBranch["id"] ?>"><?= $product["product_name"] ?></a></li>
      <?php } ?>
    </ul>
  </nav>
<?php } ?>
<?php 
  //Update the user last_seen value
  if(isset($user)) {
    $updateLastSeen = $bdd->prepare("UPDATE users SET last_seen = NOW() WHERE id = ?");
    $updateLastSeen->execute(array($user["id"]));
?>
<script type="text/javascript">
	var inputOptions = {
		<?php foreach ($availableLanguages as $langs) {
			echo '"'.$langs.'":'.' "'.$lang[$langs].'",';
		} ?>
	}
	var langSwalTitle = "<?= $lang["Navbar"]["SelectLanguage"] ?>";

	setInterval(function() {
		$.get('<?= ROOT_PATH ?>admin/ajax/updatelastseen?id=<?= $user["id"] ?>');
	}, 60 * 1000); //Each minutes
</script>
<?php } ?>
<!-- END NAV -->