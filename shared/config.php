<?php
session_start();

//The path of the root folder
define('ROOT_PATH', '/pricelist_php/');
//The timezone of the server
date_default_timezone_set("Canada/Eastern");
//The format of the time
setlocale(LC_TIME, 'fr_FR.utf8','fra');

ini_set('memory_limit', '256M');

//The database connexion
$mysql_host       = 'localhost';
$mysql_dbname     = 'interpowergroup';
$mysql_user       = 'root';
$mysql_pass       = '';
//Directory of the languages files
$langDir          = $_SERVER['DOCUMENT_ROOT'] . '' . ROOT_PATH . 'languages';
//The language abbrevehiation to fallback
$fallbackLanguage = 'en';


$bdd = new PDO('mysql:host='.$mysql_host.';dbname='.$mysql_dbname, $mysql_user, $mysql_pass);
$bdd->exec("SET CHARACTER SET utf8");

$langFiles = scandir($langDir);
$availableLanguages = [];
foreach($langFiles as $langFile) {
  if($langFile != '.' && $langFile != '..') {
    $l = explode('.', $langFile);
    array_push($availableLanguages, $l[1]);
  }
}

if(isset($_GET['lang']))
{
  $lang = htmlspecialchars($_GET['lang']);
 
  // register the session and set the cookie
  $_SESSION['lang'] = $lang;
  setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
else if(isset($_SESSION['lang']))
{
  $lang = $_SESSION['lang'];
}
else if(isset($_COOKIE['lang']))
{
  $lang = htmlspecialchars($_COOKIE['lang']);
}
else
{
  $lang = $fallbackLanguage;
}

//If the langauge is a valid one
if(!in_array($lang, $availableLanguages)) {
  $lang = $fallbackLanguage;
}

//Get the user config (pass max attemp, bantime, password complexity, etc...)
$requserconfig = $bdd->query("SELECT * FROM user_parameters");
while($conf = $requserconfig->fetch()) {
  $userconfig[$conf["name"]] = $conf["value"];
}

require_once $_SERVER['DOCUMENT_ROOT'] . ROOT_PATH . 'assets/misc/PhpRbac/src/PhpRbac/Rbac.php';
use PhpRbac\Rbac;
$rbac = new Rbac();

include_once $langDir.'/lang.'.$lang.'.php';

?>