<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bulma.min.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="shortcut icon" href="<?= ROOT_PATH ?>assets/img/favicon.ico">
<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/modal-fx.min.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="assets/css/style-ie.css">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="assets/css/style-ie9.css">
<![endif]-->

<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="assets/css/style-ie8.css">
<script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="assets/js/main-ie.js"></script>
<![endif]-->