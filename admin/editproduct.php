<?php
  include('../shared/config.php');
 

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $activeURL = 'productslist';
      
      if(isset($_GET["id"]) && !empty($_GET["id"]) && is_numeric($_GET["id"])) {

        $reqmakers = $bdd->query("SELECT * FROM products_makers");
        $makers = $reqmakers->fetchAll();
        $reqcategories = $bdd->query("SELECT * FROM products_categories");
        $categories = $reqcategories->fetchAll();
        $reqtypes = $bdd->query("SELECT * FROM products_types");
        $types = $reqtypes->fetchAll();
        $reqseries = $bdd->query("SELECT * FROM products_series");
        $series = $reqseries->fetchAll();
        $reqsubseries = $bdd->query("SELECT * FROM products_subseries");
        $subseries = $reqsubseries->fetchAll();
        $reqbranches = $bdd->query("SELECT * FROM branches");
        $branches = $reqbranches->fetchAll();
        $reqoptionsset = $bdd->query("SELECT * FROM products_optionset");
        $optionsset = $reqoptionsset->fetchAll();
        $reqspecset = $bdd->query("SELECT * FROM products_specset");
        $specset = $reqspecset->fetchAll();

        $reqproduct = $bdd->prepare("SELECT prods.*, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, series.name AS `serie`, subseries.name AS `subserie`, details.details, opts.options, docs.docs FROM products AS prods 
																	RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																	RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																	RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																	RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																	RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																	RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																	RIGHT JOIN products_options AS opts ON prods.id = opts.product_id 
																	RIGHT JOIN products_docs AS docs ON prods.id = docs.product_id 
                                  WHERE prods.id = ? ORDER BY prods.id");
        $reqproduct->execute(array(htmlspecialchars($_GET["id"])));
				$product = $reqproduct->fetch();
				
				$options = json_decode($product["options"], true);
				$specs = json_decode($product["details"], true);

				foreach ($options as $key => $optionset) {
					foreach ($optionsset as $index => $alloptionset) {
						if($alloptionset["id"] == $optionset["id"]) {
							$options[$key]["name"] = json_decode($alloptionset["name"], true);
							$options[$key]["options"] = json_decode($alloptionset["options"], true);
							$options[$key]["relation"] = $alloptionset["relation"];
							foreach ($options[$key]["options"] as $key2 => $value) {
								if(array_key_exists($key2, $optionset["args"])) {
									$options[$key]["options"][$key2]["arg"] = $optionset["args"][$key2];
								} else {
									$options[$key]["options"][$key2]["arg"] = "";
								}
							}
						}
					}
        }
        
        foreach ($specs as $key => $optionset) {
					foreach ($specset as $index => $alloptionset) {
						if($alloptionset["id"] == $optionset["id"]) {
							$specs[$key]["name"] = json_decode($alloptionset["name"], true);
							$specs[$key]["specs"] = json_decode($alloptionset["specs"], true);
							foreach ($specs[$key]["specs"] as $key2 => $value) {
								if(array_key_exists($key2, $optionset["args"])) {
									$specs[$key]["specs"][$key2]["arg"] = $optionset["args"][$key2];
								} else {
									$specs[$key]["specs"][$key2]["arg"] = ["can" => "", "us" => ""];
								}
							}
						}
					}
        }

      } else {
        header('Location: '. ROOT_PATH .'admin/productslist');
      }
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["EditProduct"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap-multiselect.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["EditProduct"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
												
                    <div id="productProperties">
                          <form method="post" v-on:submit.prevent="onSubmit" enctype="multipart/form-data">
                          <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Makers"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="maker in makers" :key="'makers_' + maker.id" v-model="selected['makers']" :parameter="maker" param-type="makers" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Categories"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="category in filtered('categories', 'makers')" :key="'categories_' + category.id" v-model="selected['categories']" :parameter="category" param-type="categories" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Types"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="type in filtered('types', 'categories')" :key="'types_' + type.id" v-model="selected['types']" :parameter="type" param-type="types" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Series"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="serie in filtered('series', 'types', 'makers')" :key="'series_' + serie.id" v-model="selected['series']" :parameter="serie" param-type="series" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Subseries"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="subserie in filtered('subseries', 'series')" :key="'subseries_' + subserie.id" v-model="selected['subseries']" :parameter="subserie" param-type="subseries" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["SideMenu"]["Products"]["Branches"]["Header"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="branch in branches" :key="'branches_' + branch.id" v-model="selected['branches'][branch.id]" :parameter="branch" param-type="branches" input-type="checkbox"></parameters-list>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Name"] ?></label>
                              </div>
                              <div class="field-body is-horizontal">
                                <div class="field">
                                  <p class="control is-expanded has-icons-left">
                                    <input class="input" type="text" autocomplete="off" name="productname" v-model="productname" placeholder="<?= $lang["Admin"]["AddProduct"]["Name"] ?>">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-box"></i>
                                    </span>
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Description"] ?></label>
                              </div>
                              <div class="field-body is-horizontal">
                                <div class="field">
																	<?php foreach ($availableLanguages as $key => $value) { ?>
																		<div class="field-label is-small">
																			<label class="label"><?= $lang[$value] ?></label>
																		</div>
																		<p class="control is-expanded">
																			<textarea name="productdesc" id="productdesc" v-model="productdesc['<?= $value ?>']" class="textarea" placeholder="<?= $lang["Admin"]["AddProduct"]["Description"] ?>"></textarea>
																		</p>
																	<?php } ?>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["SequenceNumber"] ?></label>
                              </div>
                              <div class="field-body is-horizontal">
                                <div class="field">
                                  <p class="control is-expanded has-icons-left">
                                    <input class="input" type="number" autocomplete="off" name="sequence" v-model="sequence" placeholder="<?= $lang["Admin"]["AddProduct"]["SequenceNumber"] ?>">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-hashtag"></i>
                                    </span>
                                  </p>
                                </div>
                              </div>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Price"] ?></label>
                              </div>
															<div class="field-body is-horizontal">
																<div class="field">
																	<p class="control is-expanded has-icons-left">
																		<input class="input currentStock" autocomplete="off" type="number" step="any" v-model="productprice" placeholder="<?= $lang["Admin"]["AddProduct"]["Price"] ?>">
																		<span class="icon is-small is-left">
																			<i class="fas fa-dollar-sign"></i>
																		</span>
                                    <p class="help"><?= $lang["Admin"]["AddProduct"]["PriceDisclamer"] ?> (1 USD = <?= $userconfig["exchangerate"] ?> CAD)</p>
																	</p>
																</div>
															</div>
														</div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Specs"]["Header"] ?></label>
                              </div>
                              <div class="specs">
                                <product-spec v-for="(spec, index) in filteredSpecs" :key="index" :index="index" :item="spec" lang="<?= $language ?>" v-on:remove="!spec.readonly ? addedspecs.splice(index - defaultspecs.length, 1) : log(index);" item-title="<?= $lang["Admin"]["AddProduct"]["Specs"]["Title"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Specs"]["Argument"] ?>"></product-spec>
                              </div>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Opts"]["Header"] ?></label>
                                <p class="help"><?= $lang["Admin"]["AddProduct"]["PriceDisclamer"] ?> (1 USD = <?= $userconfig["exchangerate"] ?> CAD)</p>
                              </div>
															<div class="opts">
																<product-options v-for="(opt, index) in filteredOptions" :key="index" :index="index" :item="opt" lang="<?= $language ?>" v-on:remove="!opt.readonly ? addedopts.splice(index - defaultopts.length, 1) : log(defaultopts.length);" item-title="<?= $lang["Admin"]["AddProduct"]["Opts"]["Title"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Opts"]["Argument"] ?>"></product-opt>
															</div>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Documents"]["Header"] ?> <a style="margin-left: 15px;" @click="additem('docs')"><i class="fa fa-plus"></i></a></label>
                              </div>
															<div class="docs">
															<?php foreach ($availableLanguages as $key => $value) { ?>
                                <div class="field-label is-small">
                                  <label class="label"><?= $lang[$value] ?></label>
                                </div>
                                <div class="docs doc_<?= $value ?>" data-lang="<?= $value ?>">
                                  <product-doc v-for="(doc, index) in addeddocs" :key="index" :index="index" :item="doc" lang="<?= $value ?>" v-on:remove="addeddocs.splice(index, 1)" item-title="<?= $lang["Admin"]["AddProduct"]["Documents"]["Name"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Documents"]["Upload"] ?>"></product-doc>
                                </div>
                              <?php } ?>
															</div>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Image"]["Header"] ?></label>
                              </div>
                              <div class="image">
                                <div class="field">
                                  <p class="control is-expanded has-icons-left">
                                    <input class="input itemFile" autocomplete="off" type="file" @change="convertToB64($event)">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-file-image"></i>
                                    </span>
                                  </p>
                                </div>
                                <img :src="productimage" style="width:30%;margin: auto;" />
                              </div>
                            </div>
                            <div class="level is-mobile">
                              <div class="level-item"><a class="button is-light is-medium is-fullwidth" href="<?= ROOT_PATH ?>admin/productslist"><?= $lang["Return"] ?></a></div>
                              <div class="level-item"><button class="button is-link is-medium is-fullwidth"><?= $lang["Edit"] ?></button></div>
                            </div>
                            
                          </form>
												</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
      var language = "<?= $language ?>";
      let vmData = {
        makers: <?= json_encode($makers) ?>,
        categories: <?= json_encode($categories) ?>,
        types: <?= json_encode($types) ?>,
        series: <?= json_encode($series) ?>,
        subseries: <?= json_encode($subseries) ?>,
        branches: <?= json_encode($branches) ?>,
        optionsset: <?= json_encode($optionsset) ?>,
        specset: <?= json_encode($specset) ?>,
        productname: "<?= $product["product_name"] ?>",
        productdesc: <?= $product["desc"] ?>,
        productprice: "<?= $product["base_price"] ?>",
        productimage: "<?= $product["img"] ?>",
        defaultspecs: <?= json_encode($specs) ?>,
        addedspecs: [],
				defaultopts: <?= json_encode($options) ?>,
        addedopts: [],
        addeddocs: <?= $product["docs"] ?>,
        sequence: <?= $product["sequence"] ?>,
        selected: {
          makers: <?= $product["maker_id"] ?>,
          categories: <?= $product["category_id"] ?>,
          types: <?= $product["type_id"] ?>,
          series: <?= $product["serie_id"] ?>,
          subseries: <?= $product["subserie_id"] ?>,
          branches: <?= $product["branches"] ?>
        },
      }

			Vue.component('parameters-list', {
        props: ['parameter', 'paramType', 'inputType', 'value'],
        data: function () {
          return {
            selectedParam: false,
          }
        },
        mounted() {
          this.$nextTick(function () {
            if(this.inputType == 'radio') {
              if(this.parameter.id == vmData.selected[this.paramType]) {
                $('#' + this.paramType + '_' + this.parameter.id).prop('checked', true);
              }
            }
            if(this.inputType == 'checkbox') {
              if(vmData.selected[this.paramType][this.parameter.id] == 'true') {
                $('#' + this.paramType + '_' + this.parameter.id).prop('checked', true);
              }
            }
          });
        },
        methods: {
          select(){
						let _this = this;
						let i = 0;
						let index;
						$.each(vmData.selected, function(k, v) {
							if(k == _this.paramType) {
								return index = i;
							}
							i++;
						});
						i = 0;
					
						$.each(vmData.selected, function(k, v) {
							if(i > index) {
								if(k != 'branches') {
									vmData.selected[k] = '';
									$('input[name="'+ k +'"]').prop('checked', false);
								}
							}
							i++;
						});

						if(_this.paramType == "categories" || _this.paramType == "series") {
							let myFilter;
							vmData[_this.paramType].forEach(element => {
								if(element.id == vmData.selected[_this.paramType]) {
									return myFilter = element;
								}
							});
							if(_this.paramType == "categories") {
                vmData['defaultspecs'] = vmData.specset.filter((spec) => {
                  return myFilter.specs.includes(spec.id);
                });
								vmData.defaultspecs.forEach(element => {
									element["name"] = JSON.parse(element["name"]);
									element["specs"] = JSON.parse(element["specs"]);
									element["specs"].forEach(element2 => {
										element2.arg = { "can": "", "us": "" };
									});
								});
							} else if(_this.paramType == "series") {
								vmData['defaultopts'] = vmData.optionsset.filter((option) => {
                  return myFilter.specs.includes(option.id);
                });
								vmData.defaultopts.forEach(element => {
									element["name"] = JSON.parse(element["name"]);
									element["options"] = JSON.parse(element["options"]);
									element["options"].forEach(element2 => {
										element2.arg = '';
									});
								});
							}
						}
          }
        },
        template: `
          <div class="toggle_child">
            <input v-if="inputType == 'radio'" :type="inputType" autocomplete="off" :name="paramType" @change="$emit('input', $event.target.value); select();" :value="parameter.id" :id="paramType + '_' + parameter.id" />
            <input v-if="inputType == 'checkbox'" :type="inputType" autocomplete="off" :name="paramType" @change="$emit('input', $event.target.checked); select();" :value="parameter.id" :id="paramType + '_' + parameter.id" />
            <label :for="paramType + '_' + parameter.id" :class="paramType + '_' + parameter.id">{{ paramType != 'branches' ? JSON.parse(parameter.name)['${language}'] : parameter.name }}</label>
          </div>
        `
      });

      Vue.component('product-spec', {
        template: `
        <div>
          <label>{{ item['name'][lang] }}</label>
          <div class="field-body is-horizontal" v-for="spec in item['specs']">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemTitle" type="text" autocomplete="off" readonly="true" v-model="spec['title'][lang]" :placeholder="itemTitle">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemArg" type="text" autocomplete="off" v-model="spec['arg']['can']" :placeholder="itemArgument + ' (CANADA)'">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
						<div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemArg" type="text" autocomplete="off" v-model="spec['arg']['us']" :placeholder="itemArgument + ' (US)'">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
            <!-- <a class="fa fa-trash-alt deleteSpec" :style="{ visibility: item.visibility }" @click="$emit('remove')" style="font-size:2rem;"></a> -->
          </div>
        </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			Vue.component('product-options', {
        template: `
          <div>
            <label>{{ item['name'][lang] }}</label>
            <div class="field-body is-horizontal" v-for="opt in item['options']">
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input class="input itemTitle" type="text" autocomplete="off" readonly="true" v-model="opt['title'][lang]" :placeholder="itemTitle">
                  <span class="icon is-small is-left">
                    <i class="fas fa-tag"></i>
                  </span>
                </p>
              </div>
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input class="input itemArg" type="text" autocomplete="off" v-model="opt['arg']" :placeholder="itemArgument">
                  <span class="icon is-small is-left">
                    <i class="fas fa-dollar-sign"></i>
                  </span>
                </p>
              </div>
            </div>
          </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			Vue.component('product-doc', {
        template: `
          <div class="field-body is-horizontal">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemTitle" type="text" autocomplete="off" v-model="item['name'][lang]" :placeholder="itemTitle">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <div class="field">
							<p class="control is-expanded has-icons-left">
                <input class="input itemFile" type="text" autocomplete="off" v-model="item['file']" :placeholder="itemArgument">
                <span class="icon is-small is-left">
                  <i class="fas fa-file-alt"></i>
                </span>
              </p>
            </div>
            <a class="fa fa-trash-alt deleteSpec" @click="$emit('remove')" style="font-size:2rem;"></a>
          </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			let arrayTypes = {
				// specs: {
				// 	title: {
				// 		<?php //foreach ($availableLanguages as $key => $value) {
				// 			echo '"'.$value.'": "",';
				// 		} ?>
				// 	},
				// 	arg: {
				// 		can: '', 
				// 		us: '',
				// 	}
				// },
				docs: {
					name: {
						<?php foreach ($availableLanguages as $key => $value) {
							echo '"'.$value.'": "",';
						} ?>
					},
					file: ''
				}
			}

			var vm = new Vue({
				el: '#productProperties',
				data: vmData,
        computed: {
          filteredOptions() {
            return this.defaultopts;
          },
          filteredSpecs() {
            return this.defaultspecs;
          }
        },
				methods: {
          log: function(id) {
            console.log(id);
          },
          additem: function (type) {
            this['added'+type].push(JSON.parse(JSON.stringify(arrayTypes[type])));
          },
          filtereditem: function(type, filter) {
            var _this = this;
						let items = [];
            let specField = JSON.parse(JSON.stringify(arrayTypes[type]));
            if(_this.selected[filter] == '') {
              return _this['added'+type];
            }

            let myFilter;
            _this[filter].forEach(element => {
              if(element.id == _this.selected[filter]) {
                return myFilter = element;
              }
            });

            _this['default'+type].forEach(element => {
							element.readonly = true;
							element.visibility = 'hidden';
							items.push(element);
            });
            _this['added'+type].forEach(element => {
              element.readonly = false;
              element.visibility = '';
              items.push(element);
            });
            return items;
          },
					filtered: function(paramType, filter, subfilter = '') {
            var _this = this;
            if(_this.selected[filter] == '') return null;

            let myFilter = null;
            let mySubFilter = null;
            /**
             * @myFilter represent the filter object on it own
             * @filter helps get the myFilter by specifing what object we are looping throught
             * The loop check if somewhere in the array of objects pointed by the @filter is the one selected by the parameter (@select)
             */
            this[filter].forEach(element => {
              if(element.id == _this.selected[filter]) {
                return myFilter = element;
              }
            });
            if(subfilter != '') {
              this[subfilter].forEach(element => {
              if(element.id == _this.selected[subfilter]) {
                return mySubFilter = element;
              }
            });
            }
            //This returns an array of objects of base on the filter parameter
            if(mySubFilter == null) {
              return this[paramType].filter(function (item) {
                if(item.availCategories.includes(myFilter.id)) return item
              });
            } else {
              let firstPass = this[paramType].filter(function (item) {
                if(item.availCategories.includes(myFilter.id)) return item
              });
              return firstPass.filter(function (item) {
                if(item.makers.includes(mySubFilter.id)) return item
              });
            }
          },
          onSubmit: function() {
            let product = JSON.parse(JSON.stringify(vmData));
            //Remove the unessessary objects from the vmData
            delete product["makers"];
            delete product["categories"];
            delete product["types"];
            delete product["series"];
            delete product["subseries"];
            delete product["branches"];
            product["defaultspecs"].forEach(element => {
              delete element.visibility;
              delete element.readonly;
            });
            product["addedspecs"].forEach(element => {
              delete element.visibility;
              delete element.readonly;
            });
            product["defaultopts"].forEach(element => {
              delete element.visibility;
              delete element.readonly;
            });
            product["addedopts"].forEach(element => {
              delete element.visibility;
              delete element.readonly;
            });
            let data = { 
							'userID': "<?= $user["id"] ?>",
							"userPassword": "<?= $user["password"] ?>",
							'productID': <?= htmlspecialchars($_GET["id"]) ?>,
							'data': {
								'type': 'product_update',
                'product': product
							}
						}
						$.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeproduct",
							data: data,
							dataType: 'json',
							success: function (response) {
								swal({
									title: `${response.text}`,
									type: `${response.type}`,
								}).then(function() {
									if(response.type == 'success') {
										location.replace('<?= ROOT_PATH ?>admin/productslist');
									}
								});
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
          },
          convertToB64(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;

            var reader = new FileReader();
            reader.onload = (e) => {
              vmData.productimage = e.target.result;
            };
            reader.readAsDataURL(files[0]);
          }
				}
			});
    </script>
  </body>
</html>