<?php
  include('../shared/config.php');
  $activeURL = "employee";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			if(isset($_GET["employee"]) && !empty($_GET["employee"]) && is_numeric($_GET["employee"])) {
				$reqemployee = $bdd->prepare("SELECT * FROM users WHERE id = ?");
				$reqemployee->execute(array(htmlspecialchars($_GET["employee"])));
				$employee = $reqemployee->fetch();

				// $reqPermissions = $bdd->prepare("SELECT * FROM user_permissions WHERE user_id = ?");
				// $reqPermissions->execute(array(htmlspecialchars($_GET["employee"])));
				// $permissions = $reqPermissions->fetchAll();

				$reqRoles = $bdd->query("SELECT * FROM phprbac_roles");
				$roles = $reqRoles->fetchAll();

				if($rbac->Users->hasRole('root', $employee["id"])) {
					$error = [
						"type" => "danger",
						"msg" => $lang["Admin"]["Permissions"]["IsAdminWarning"]
					];
				}
			} else {
				header('Location: '. ROOT_PATH .'admin/employees');
			}
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">

<head>
	<title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["Permissions"]["PageTitle"] ?></title>
	<?php include_once('../shared/head.php') ?>
	<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
	<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/switch.css">
	<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
</head>

<body>
	<?php include_once('../shared/navbar.php'); ?>
	<div class="container">
		<div class="columns">
			<?php include_once('sidebar.php'); ?>
			<div class="column is-9">
				<div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="z-index:10;<?php if(!isset($error)) { echo 'display:none;'; } ?>">
					<button class="delete"></button>
					<span><?php if(isset($error)) { echo $error["msg"]; } ?></span>
				</div>
					<div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["Permissions"]["Employee"] . ' ' . $employee["firstname"] . ' ' . $employee["lastname"] . ' (' . $employee["username"] . ')' ?></h2>
          </div>
					<div class="columns is-mutiline is-centered">
						<div class="column">
							<article class="message is-warning">
								<div class="message-header">
									<p><?= $lang['Admin']["Branches"] ?></p>
								</div>
								<div class="message-body checkboxes-and-radios">
									<!-- REPEAT FROM HERE -->
										<?php 
											foreach ($roles as $role) { 
												if(strpos($role["Title"], 'branch_') === 0) {
													$roleValue = $rbac->Users->hasRole($role['ID'], $employee['id']);
										?>
											<div class="field">
												<input type="checkbox" name="branch_auth_<?= $role["ID"] ?>" id="branch_auth_<?= $role["ID"] ?>" onchange="updateRole($(this), <?= $role["ID"] ?>);" data-employeeID="<?= $employee["id"] ?>" value="<?= $roleValue ?>" <?php if($roleValue == true) { echo 'checked'; } ?>>
												<label for="branch_auth_<?= $role["ID"] ?>"><?= $role["Description"] ?></label>
												<p class="help" style="margin-top: 0;"><?= $lang["Admin"]["Permissions"]["AccessToBranch"] . ' ' . $role["Description"] ?></p>
											</div>
										<?php
												}
											}
										?>
									<!-- UNTIL HERE -->
								</div>
							</article>
						</div>
						<div class="column">
							<article class="message is-link">
								<div class="message-header">
									<p><?= $lang['Admin']["Permissions"]["Group"] ?></p>
								</div>
								<div class="message-body checkboxes-and-radios">
									<!-- REPEAT FROM HERE -->
									<?php 
											foreach ($roles as $group) { 
												//Remove the root group and the branches group from the list
												if(strpos($group["Title"], 'branch_') !== 0) {
													$groupValue = $rbac->Users->hasRole($group['ID'], $employee['id']);
										?>
											<div class="field">
												<input type="checkbox" name="group_auth_<?= $group["ID"] ?>" autocomplete="off" id="group_auth_<?= $group["ID"] ?>" onchange="updateRole($(this), <?= $group["ID"] ?>);" data-employeeID="<?= $employee["id"] ?>" value="<?= $groupValue ?>" <?php if($groupValue == true) { echo 'checked'; } ?>>
												<label for="group_auth_<?= $group["ID"] ?>"><?= $group["Title"] ?></label>
												<p class="help" style="margin-top: 0;"><?= $group["Description"] ?></p>
											</div>
										<?php
												}
											}
										?>
									<!-- UNTIL HERE -->
								</div>
							</article>
						</div>

					</div>
			</div>
		</div>
	</div>
	<?php include_once('../shared/scripts.php'); ?>
	<script type="text/javascript">
		function updateRole(element, permID) {
			let employeeID = element.attr('data-employeeID');
			if(element.is(':checked')) {
				element.val('1');
			} else {
				element.val('0');
			}

			let data = { 
				'adminID': '<?= $user["id"] ?>',
				'adminPassword': '<?= $user["password"] ?>',
				'employeeID': employeeID,
				'data': {
					'type': 'user_autorizations',
					'permID': permID,
					'value': element.is(':checked'),
				}
			}
			$.ajax({
				type: "POST",
				url: "<?= ROOT_PATH ?>admin/ajax/changeemployee",
				data: data,
				dataType: "json",
				success: function (response) {
					swal({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000,
						type: `${response.type}`,
						title: `${response.text}`
					});
				}
			}).fail(function( jqXHR, textStatus ) {
				alert(`Request failed: ${textStatus}`)
			});
		}
	</script>
</body>

</html>