<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["adminID"], $_POST["adminPassword"], $_POST["parameterID"], $_POST["data"]) && !empty($_POST["adminID"]) && !empty($_POST["adminPassword"]) && !empty($_POST["parameterID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["adminID"]), htmlspecialchars($_POST["adminPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["adminID"])){
        //The user making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $parameterID = htmlspecialchars($_POST["parameterID"]);

        if($data["type"] == "parameter_update") {
          updateParameter($data, $parameterID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "parameter_delete") {
          deleteParameter($data, $parameterID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "parameter_add") {
          addParameter($data, $lang, $bdd, $adminInfos);
        } else {
          echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
        }
      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
  }

  function updateParameter($data, $parameterID, $lang, $bdd, $adminInfos) {
    $data["jsonNames"] = json_encode($data["names"]);
    $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET name = ?, img = ? WHERE id = ?");
    $updateparameter->execute(array($data["jsonNames"], htmlspecialchars($data["img"]), $parameterID));
    if(isset($data["cats"])) {
      $data["cats"] = json_encode($data["cats"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET availCategories = ? WHERE id = ?");
      $updateparameter->execute(array($data["cats"], $parameterID));
    }
    if(isset($data["makers"])) {
      $data["makers"] = json_encode($data["makers"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET makers = ? WHERE id = ?");
      $updateparameter->execute(array($data["makers"], $parameterID));
    }
    if($data["param"] == "categories" || $data["param"] == "series") {
      if(!isset($data["specs"]) || empty($data["specs"])) {
        if($data["param"] == "categories") {
          $data["specs"] = array(["title" => ["fr" => "","en" => ""],"arg" => ["can" => "", "us" => ""]]);
        } else if($data["param"] == "series") {
          $data["specs"] = array(["title" => ["fr" => "","en" => ""],"arg" => ""]);
        }
      }
      $data["specs"] = json_encode($data["specs"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET specs = ? WHERE id = ?");
      $updateparameter->execute(array($data["specs"], $parameterID));
    }
    if($data["param"] == "series") {
      $data["discount"] = json_encode($data["discount"]);
      $data["multiplier"] = json_encode($data["multiplier"]);
      if(is_empty($data["transport"]) || !is_numeric($data["transport"])) {
        $data["transport"] = 0;
      }
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET discounts = ?, multipliers = ?, transport = ? WHERE id = ?");
      $updateparameter->execute(array($data["discount"], $data["multiplier"], $data["transport"], $parameterID));
    }

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "parameter" => $data["param"].' #'.$parameterID,
    ];
    $insertEvent->execute(array("parameterUpdated", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["ParametersList"]["Edit".ucfirst($data["param"])]["Done"], 'data' => $data ]);
  }

  function deleteParameter($data, $parameterID, $lang, $bdd, $adminInfos) {
    if($data["delete"]) {
      //Delete the parameter
      $deleteparameter = $bdd->prepare("DELETE FROM products_". $data['param'] ." WHERE id = ?");
      $deleteparameter->execute(array($parameterID));

      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "parameter" => $data["param"].' #'.$parameterID,
      ];
      $insertEvent->execute(array("parameterDeleted", json_encode($details), date("Y-m-d H:i:s")));

      echo json_encode([ "type" => 'success', "text" => $lang['Admin']["ParametersList"]["Delete".ucfirst($data["param"])]["Done"] ]);
    }
  }

  function addParameter($data, $lang, $bdd, $adminInfos){
    //Insert the parameter
    $data["jsonNames"] = json_encode($data["names"]);
    $insertparameter = $bdd->prepare("INSERT INTO products_". $data['param'] ." (name, img) VALUES (?, ?)");
    $insertparameter->execute(array($data["jsonNames"], htmlspecialchars($data["img"])));
    $parameterID = $bdd->lastInsertId();
    if(isset($data["cats"])) {
      $data["cats"] = json_encode($data["cats"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET availCategories = ? WHERE id = ?");
      $updateparameter->execute(array($data["cats"], $parameterID));
    }
    if(isset($data["makers"])) {
      $data["makers"] = json_encode($data["makers"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET makers = ? WHERE id = ?");
      $updateparameter->execute(array($data["makers"], $parameterID));
    }
    if($data["param"] == "categories" || $data["param"] == "series") {
      if(!isset($data["specs"]) || empty($data["specs"])) {
        if($data["param"] == "categories") {
          $data["specs"] = array(["title" => ["fr" => "","en" => ""],"arg" => ["can" => "", "us" => ""]]);
        } else if($data["param"] == "series") {
          $data["specs"] = array(["title" => ["fr" => "","en" => ""],"arg" => ""]);
        }
      }
      $data["specs"] = json_encode($data["specs"]);
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET specs = ? WHERE id = ?");
      $updateparameter->execute(array($data["specs"], $parameterID));
    }
    if($data["param"] == "series") {
      $data["discount"] = json_encode($data["discount"]);
      $data["multiplier"] = json_encode($data["multiplier"]);
      if(is_empty($data["transport"]) || !is_numeric($data["transport"])) {
        $data["transport"] = 0;
      }
      $updateparameter = $bdd->prepare("UPDATE products_". $data['param'] ." SET discounts = ?, multipliers = ?, transport = ? WHERE id = ?");
      $updateparameter->execute(array($data["discount"], $data["multiplier"], $data["transport"], $parameterID));
    }

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "parameter" => $data["param"].' #'.$parameterID,
    ];
    $insertEvent->execute(array("parameterAdded", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["ParametersList"]["Add".ucfirst($data["param"])]["Done"], 'data' => $data ]);
  }

?>