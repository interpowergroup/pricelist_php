<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["adminID"], $_POST["adminPassword"], $_POST["employeeID"], $_POST["data"]) && !empty($_POST["adminID"]) && !empty($_POST["adminPassword"]) && !empty($_POST["employeeID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["adminID"]), htmlspecialchars($_POST["adminPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["adminID"])){
        //The user making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $employeeID = htmlspecialchars($_POST["employeeID"]);

        if($data["type"] == "password_change") {
          updatePassword($data, $employeeID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "user_update") {
          updateUser($data, $employeeID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "user_delete") {
          deleteUser($data, $employeeID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "user_autorizations") {
          setAutorizationsUser($data, $employeeID, $lang, $rbac, $bdd, $adminInfos);
        } else if($data["type"] == "user_add") {
          addUser($data, $lang, $bdd, $adminInfos);
        } else {
          echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
        }

      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'3' ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'2' ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'1' ]);
  }

  function updatePassword($data, $employeeID, $lang, $bdd, $adminInfos) {
    $hash = password_hash(htmlspecialchars($data["password"]), PASSWORD_DEFAULT, [ 'cost' => 10 ]);
    $updateuser = $bdd->prepare("UPDATE users SET password = ? WHERE id = ?");
    $updateuser->execute(array($hash, $employeeID));

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "user" => '#'.$employeeID,
    ];
    $insertEvent->execute(array("userPasswordUpdated", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["EmployeesList"]["ResetPassword"]["Done"] ]);
  }

  function updateUser($data, $employeeID, $lang, $bdd, $adminInfos) {
    $updateuser = $bdd->prepare("UPDATE users SET firstname = ?, lastname = ?, username = ?, email = ? WHERE id = ?");
    $updateuser->execute(array(htmlspecialchars($data["firstname"]), htmlspecialchars($data["lastname"]), htmlspecialchars($data["username"]), htmlspecialchars($data["mail"]), $employeeID));
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "user" => '#'.$employeeID,
    ];
    $insertEvent->execute(array("userUpdated", json_encode($details), date("Y-m-d H:i:s")));
    
    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["EmployeesList"]["EditEmployee"]["Done"], 'data' => $data ]);
  }

  function deleteUser($data, $employeeID, $lang, $bdd, $adminInfos) {
    if($data["delete"]) {
      $deleteuser = $bdd->prepare("DELETE FROM users WHERE id = ?");
      $deleteuser->execute(array($employeeID));

      //TODO: Remove perm with rbac
      // $deleteperm = $bdd->prepare("DELETE FROM user_permissions WHERE user_id = ?");
      // $deleteperm->execute(array($employeeID));

      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "user" => '#'.$employeeID,
      ];
      $insertEvent->execute(array("userDeleted", json_encode($details), date("Y-m-d H:i:s")));

      echo json_encode([ "type" => 'success', "text" => $lang['Admin']["EmployeesList"]["DeleteEmployee"]["Done"] ]);
    }
  }

  function setAutorizationsUser($data, $employeeID, $lang, $rbac, $bdd, $adminInfos) {
    if($data["value"] == 'true') {
      $rbac->Users->assign($data["permID"], $employeeID);
    } else {
      $rbac->Users->unassign($data["permID"], $employeeID);
    }

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "user" => '#'.$employeeID,
    ];
    $insertEvent->execute(array("userAuthChanged", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["EmployeesList"]["AutorizationsEmployee"]["Done"], "data" => $data ]);
  }

  function addUser($data, $lang, $bdd, $adminInfos){
    //Insert User
    $hash = password_hash(htmlspecialchars($data["password"]), PASSWORD_DEFAULT, [ 'cost' => 10 ]);
    $insertuser = $bdd->prepare("INSERT INTO users (firstname, lastname, username, email, password) VALUES (?, ?, ?, ?, ?)");
    $insertuser->execute(array(htmlspecialchars($data["firstname"]), htmlspecialchars($data["lastname"]), htmlspecialchars($data["username"]), htmlspecialchars($data["mail"]), $hash));
    $data["id"] = $bdd->lastInsertId();

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "user" => '#0',
    ];
    $insertEvent->execute(array("userAdded", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["EmployeesList"]["AddEmployee"]["Done"], 'data' => $data ]);
  }

?>