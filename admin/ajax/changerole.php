<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["adminID"], $_POST["adminPassword"], $_POST["roleID"], $_POST["data"]) && !empty($_POST["adminID"]) && !empty($_POST["adminPassword"]) && !empty($_POST["roleID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["adminID"]), htmlspecialchars($_POST["adminPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["adminID"])){
        //The role making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $roleID = htmlspecialchars($_POST["roleID"]);

        if($data["type"] == "role_update") {
          updateRole($data, $roleID, $lang, $rbac, $bdd, $adminInfos);
        } else if($data["type"] == "role_delete") {
          deleteRole($data, $roleID, $lang, $rbac, $bdd, $adminInfos);
        } else if($data["type"] == "role_add") {
          addRole($data, $lang, $rbac, $bdd, $adminInfos);
        } else {
          echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
        }

      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin" ] ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
  }

  function updateRole($data, $roleID, $lang, $rbac, $bdd, $adminInfos) {
		//Update name and desc
		$rbac->Roles->edit($roleID, $data["Title"], $data["Desc"]);
    
    foreach ($data["perms"] as $perm) {
      if($perm["value"] == 'true') {
				$rbac->Roles->assign($roleID, $perm["ID"]);
			} else {
				$rbac->Roles->unassign($roleID, $perm["ID"]);
			}
    }

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "group" => '#'.$roleID,
    ];
    $insertEvent->execute(array("roleUpdated", json_encode($details), date("Y-m-d H:i:s")));


    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["RolesList"]["EditRole"]["Done"], 'data' => $data ]);
  }

	function deleteRole($data, $roleID, $lang, $rbac, $bdd, $adminInfos) {
    if($data["delete"]) {
      $rbac->Roles->remove($roleID ,true);

      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "group" => '#'.$roleID,
      ];
      $insertEvent->execute(array("roleDeleted", json_encode($details), date("Y-m-d H:i:s")));


      echo json_encode([ "type" => 'success', "text" => $lang['Admin']["RolesList"]["DeleteRole"]["Done"] ]);
		}
	}

  function addRole($data, $lang, $rbac, $bdd, $adminInfos){
		$roleID = $rbac->Roles->add($data["Title"], $data["Desc"]);
		
		foreach ($data["perms"] as $key => $value) {
			$rbac->Roles->assign($roleID, $value);
    }
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "group" => '#'.$roleID,
    ];
    $insertEvent->execute(array("roleAdded", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["RolesList"]["AddRole"]["Done"], 'data' => $data ]);
  }

?>