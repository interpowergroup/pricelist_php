<?php 
include('../../shared/config.php');

if(isset($_GET["id"]) && !empty($_GET["id"]) && is_numeric($_GET["id"])) {
  $updateLastSeen = $bdd->prepare("UPDATE users SET last_seen = NOW() WHERE id = ?");
  $updateLastSeen->execute(array(htmlspecialchars($_GET["id"])));
}

?>