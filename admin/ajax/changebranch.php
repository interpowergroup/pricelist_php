<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["adminID"], $_POST["adminPassword"], $_POST["branchID"], $_POST["data"]) && !empty($_POST["adminID"]) && !empty($_POST["adminPassword"]) && !empty($_POST["branchID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["adminID"]), htmlspecialchars($_POST["adminPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["adminID"])){
        //The user making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $branchID = htmlspecialchars($_POST["branchID"]);

        if($data["type"] == "branch_update") {
          updateBranch($data, $branchID, $lang, $bdd, $rbac, $adminInfos);
        } else if($data["type"] == "branch_delete") {
          deleteBranch($data, $branchID, $lang, $bdd, $rbac, $adminInfos);
        } else if($data["type"] == "branch_add") {
          addBranch($data, $lang, $bdd, $rbac, $adminInfos);
        } else {
          echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
        }

      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'3' ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'2' ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'1' ]);
  }

  function updateBranch($data, $branchID, $lang, $bdd, $rbac, $adminInfos) {
    $updatebranch = $bdd->prepare("UPDATE branches SET name = ?, location = ?, spectype = ?, logo = ? WHERE id = ?");
    $updatebranch->execute(array(htmlspecialchars($data["name"]), htmlspecialchars($data["location"]), htmlspecialchars($data["spectype"]), htmlspecialchars($data["logo"]), $branchID));
		
		$roleID = $rbac->Roles->returnId('branch_'.$branchID);
		$permID = $rbac->Permissions->returnId('branch_'.$branchID);
		$rbac->Permissions->edit($permID, 'branch_'.$branchID, htmlspecialchars($data["name"]));
    $rbac->Roles->edit($roleID, 'branch_'.$branchID, htmlspecialchars($data["name"]));
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "branch" => '#'.$branchID,
    ];
    $insertEvent->execute(array("branchUpdated", json_encode($details), date("Y-m-d H:i:s")));

		echo json_encode([ "type" => 'success', "text" => $lang['Admin']["BranchesList"]["EditBranch"]["Done"], 'data' => $data ]);
  }

  function deleteBranch($data, $branchID, $lang, $bdd, $rbac, $adminInfos) {
    if($data["delete"]) {
      //Delete the branch
      $deletebranch = $bdd->prepare("DELETE FROM branches WHERE id = ?");
      $deletebranch->execute(array($branchID));
			
			$roleID = $rbac->Roles->returnId('branch_'.$branchID);
			$permID = $rbac->Permissions->returnId('branch_'.$branchID);
			$rbac->Permissions->remove($permID);
      $rbac->Roles->remove($roleID);
      
      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "branch" => '#'.$branchID,
      ];
      $insertEvent->execute(array("branchDeleted", json_encode($details), date("Y-m-d H:i:s")));
      
      echo json_encode([ "type" => 'success', "text" => $lang['Admin']["BranchesList"]["DeleteBranch"]["Done"] ]);
    }
  }

  function addBranch($data, $lang, $bdd, $rbac, $adminInfos){
    //Insert the branch
    $insertbranch = $bdd->prepare("INSERT INTO branches (name, location, spectype, logo) VALUES (?, ?, ?, ?)");
    $insertbranch->execute(array(htmlspecialchars($data["name"]), htmlspecialchars($data["location"]), htmlspecialchars($data["spectype"]), htmlspecialchars($data["logo"])));
    $branchID = $bdd->lastInsertId();
		
		$permID = $rbac->Permissions->add('branch_'.$branchID, htmlspecialchars($data["name"]));
		$roleID = $rbac->Roles->add('branch_'.$branchID, htmlspecialchars($data["name"]));
    $rbac->Roles->assign($roleID, $permID);
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "branch" => '#'.$branchID,
    ];
    $insertEvent->execute(array("branchAdded", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["BranchesList"]["AddBranch"]["Done"], 'data' => $data ]);
  }

?>