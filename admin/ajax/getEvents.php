<?php
  include('../../shared/config.php');

  $limitIn = '';
  if(isset($_GET["limit_in"]) && !empty($_GET["limit_in"]) && is_numeric($_GET["limit_in"])) {
    $limitIn = htmlspecialchars($_GET["limit_in"]);
  }
  $limitOut = '';
  if(isset($_GET["limit_out"]) && !empty($_GET["limit_out"]) && is_numeric($_GET["limit_out"])) {
    $limitOut = htmlspecialchars($_GET["limit_out"]);
  }
  if(!empty($limitIn)) {
    $limit = ' LIMIT '.$limitIn;
  } else {
    $limit = '';
  }

  if(isset($_GET["sort"]) && !empty($_GET["sort"])) {
    $sort = htmlspecialchars($_GET["sort"]);
    if($sort == "Logins") {
      $reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%login%' ORDER BY `time` DESC".$limit);
    } elseif($sort == "Modifications") {
      $reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%updated%' ORDER BY `time` DESC".$limit);
    } elseif($sort == "Deletions") {
      $reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%deleted%' ORDER BY `time` DESC".$limit);
    } elseif($sort == "Stocks") {
      $reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%inventory%' ORDER BY `time` DESC".$limit);
    } else {
      $reqEvents = $bdd->query("SELECT * FROM events ORDER BY `time` DESC".$limit);
    }
  } else {
    $sort = 'All';
    $reqEvents = $bdd->query("SELECT * FROM events ORDER BY `time` DESC".$limit);
  }
  $events = $reqEvents->fetchAll();
  $reqActiveCount = $bdd->query("SELECT username FROM users WHERE last_seen >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)");
  $activeEmployee = $reqActiveCount->fetchAll();
  $event = array($events, $activeEmployee);
  echo json_encode($event);
?>