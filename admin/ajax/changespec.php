<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["adminID"], $_POST["adminPassword"], $_POST["setID"], $_POST["data"]) && !empty($_POST["adminID"]) && !empty($_POST["adminPassword"]) && !empty($_POST["setID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["adminID"]), htmlspecialchars($_POST["adminPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["adminID"])){
        //The user making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $setID = htmlspecialchars($_POST["setID"]);

        if($data["type"] == "set_edit") {
          updateSet($data["specset"], $setID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "set_delete") {
          deleteSet($data, $setID, $lang, $bdd, $adminInfos);
        } else if($data["type"] == "set_add") {
          addSet($data["specset"], $lang, $bdd, $adminInfos);
        } else {
          echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
        }

      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'3' ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'2' ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"].'1' ]);
  }

  function updateSet($data, $setID, $lang, $bdd, $adminInfos) {
    $updateset = $bdd->prepare("UPDATE products_specset SET name = ?, specs = ? WHERE id = ?");
    $updateset->execute(array(json_encode($data["name"]), json_encode($data["specs"]), $setID));
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "set" => '#'.$setID,
    ];
    $insertEvent->execute(array("setUpdated", json_encode($details), date("Y-m-d H:i:s")));

		echo json_encode([ "type" => 'success', "text" => $lang['Admin']["SpecsSet"]["EditSet"]["Done"], 'data' => $data ]);
  }

  function deleteSet($data, $setID, $lang, $bdd, $adminInfos) {
    if($data["delete"]) {
      //Delete the set
      $deleteset = $bdd->prepare("DELETE FROM products_specset WHERE id = ?");
      $deleteset->execute(array($setID));
      
      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "set" => '#'.$setID,
      ];
      $insertEvent->execute(array("setDeleted", json_encode($details), date("Y-m-d H:i:s")));
      
      echo json_encode([ "type" => 'success', "text" => $lang['Admin']["SpecsSet"]["DeleteSet"]["Done"] ]);
    }
  }

  function addSet($data, $lang, $bdd, $adminInfos){
    //Insert the set
    $insertset = $bdd->prepare("INSERT INTO products_specset (`name`, `specs`) VALUES (?, ?)");
    $insertset->execute(array(json_encode($data["name"]), json_encode($data["specs"])));
    $setID = $bdd->lastInsertId();
    
    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "set" => '#'.$setID,
    ];
    $insertEvent->execute(array("setAdded", json_encode($details), date("Y-m-d H:i:s")));

    echo json_encode([ "type" => 'success', "text" => $lang['Admin']["SpecsSet"]["AddSet"]["Done"], 'data' => $data ]);
  }

?>