<?php
  include('../../shared/config.php');

  header('Content-Type: application/json');

  if(isset($_POST["userID"], $_POST["userPassword"], $_POST["productID"], $_POST["data"]) && !empty($_POST["userID"]) && !empty($_POST["userPassword"]) && !empty($_POST["productID"]) && !empty($_POST["data"])) {
    $checkAdminExistance = $bdd->prepare("SELECT * FROM users WHERE id = ? AND password = ?");
    $checkAdminExistance->execute(array(htmlspecialchars($_POST["userID"]), htmlspecialchars($_POST["userPassword"])));
    if($checkAdminExistance->rowCount() == 1) {
      if($rbac->check('view_admin_panel', $_POST["userID"])){
        //The user making this request is an admin, update everyting
        $adminInfos = $checkAdminExistance->fetch();
        $data = $_POST["data"];
        $productID = htmlspecialchars($_POST["productID"]);

          if($data["type"] == "product_update") {
            updateProduct($data, $productID, $lang, $bdd, $adminInfos);
            echo json_encode([ "type" => 'success', "text" => $lang['Admin']["ProductsList"]["EditProduct"]["Done"] ]);
          } else if($data["type"] == "product_delete") {
            deleteProduct($data, $productID, $lang, $bdd, $adminInfos);
            echo json_encode([ "type" => 'success', "text" => $lang['Admin']["ProductsList"]["DeleteProduct"]["Done"] ]);
          } else if($data["type"] == "product_add") {
            addProduct($data, $lang, $bdd, $adminInfos);
            echo json_encode([ "type" => 'success', "text" => $lang['Admin']["AddProduct"]["Done"], 'data' => $data ]);
          }  else if($data["type"] == "product_add_mass") {
            addProductMass($data, $lang, $bdd, $adminInfos);
            echo json_encode([ "type" => 'success', "text" => $lang['Admin']["AddProduct"]["Done"], 'data' => $data ]);
          } else if($data["type"] == "product_inventory_update") {
            updateInventory($data, $productID, $lang, $bdd, $adminInfos);
            echo json_encode([ "type" => 'success', 'data' => $data ]);
          } else {
            echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["UnknownType"] ]);
          }

      } else {
        echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
      }
    } else {
      echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
    }
  } else {
    echo json_encode([ "type" => 'error', "text" => $lang["Errors"]["NotAnAdmin"] ]);
  }

  function updateProduct($data, $productID, $lang, $bdd, $adminInfos) {
    //Insert the product
    if($data["product"]["selected"]["subseries"] == 0) { $data["product"]["selected"]["subseries"] = 1; }

    $insertProduct = $bdd->prepare("UPDATE `products` SET `product_name` = ?, `sequence` = ?, `maker_id` = ?, `category_id` = ?, `type_id` = ?, `serie_id` = ?, `subserie_id` = ?, `desc` = ?, `base_price` = ?, `branches` = ?, `img` = ? WHERE id = ?");
    $insertProduct->execute(array($data["product"]["productname"], $data["product"]["sequence"], $data["product"]["selected"]["makers"], $data["product"]["selected"]["categories"], $data["product"]["selected"]["types"], $data["product"]["selected"]["series"], $data["product"]["selected"]["subseries"], json_encode($data["product"]["productdesc"]), $data["product"]["productprice"], json_encode($data["product"]["selected"]["branches"]), $data["product"]["productimage"], $productID));
    //Insert details
    $productDetails = [];
    if(isset($data["product"]["defaultspecs"])) {
      foreach ($data["product"]["defaultspecs"] as $key => $spec) {
        $spec_id = $spec["id"];
        $args = [];
        foreach ($spec["specs"] as $key2 => $value) {
          array_push($args, $value["arg"]);
        }
        array_push($productDetails, ["id" => $spec_id, "args" => $args]);
      }
    }
    $productDetails = json_encode($productDetails);
		$insertDetails = $bdd->prepare("UPDATE `products_details` SET `product_id` = ?, `details` = ? WHERE product_id = ?");
		$insertDetails->execute(array($productID, $productDetails, $productID));
		//Insert options
    $productOptions = [];
    if(isset($data["product"]["defaultopts"])) {
      foreach ($data["product"]["defaultopts"] as $key => $option) {
        $opt_id = $option["id"];
        $args = [];
        foreach ($option["options"] as $key2 => $value) {
          array_push($args, $value["arg"]);
        }
        array_push($productOptions, ["id" => $opt_id, "args" => $args]);
      }
    }
    $productOptions = json_encode($productOptions);
    $insertOptions = $bdd->prepare("UPDATE `products_options` SET `product_id` = ?, `options` = ? WHERE product_id = ?");
    $insertOptions->execute(array($productID, $productOptions, $productID));

		//Save the documents
		$productDocs = [];
		if(isset($data["product"]["addeddocs"])) {
			foreach ($data["product"]["addeddocs"] as $key => $value) {
				if(!empty($value["name"][$lang['Short']]) && !empty($value["file"])) {
          array_push($productDocs, $value);
        }
			}
		}
    $productDocs = json_encode($productDocs);
		$insertDocs = $bdd->prepare("UPDATE `products_docs` SET `product_id` = ?, `docs` = ? WHERE product_id = ?");
    $insertDocs->execute(array($productID, $productDocs, $productID));

    //Update inventory
    //get current inv
    $reqinv = $bdd->prepare("SELECT * FROM `products_inventory` WHERE product_id = ?");
    $reqinv->execute(array($productID));
    $currInv = $reqinv->fetch();
    $currStock = json_decode($currInv["in_stock"], true);
    $currOrder = json_decode($currInv["on_order"], true);
    $inventoryStock = "{";
    $inventoryOrder = "{";
    foreach ($data["product"]["selected"]["branches"] as $key => $value) {
      if($value == 'true') {
        if(isset($currStock[$key])) {
          $inventoryStock .= '"'.$key.'":"'.$currStock[$key].'",';
        } else {
          $inventoryStock .= '"'.$key.'":"0",';
        }
        if(isset($currOrder[$key])) {
          $inventoryOrder .= '"'.$key.'":"'.$currOrder[$key].'",';
        } else {
          $inventoryOrder .= '"'.$key.'":"0",';
        }
      }
    }
    $inventoryStock = substr($inventoryStock, 0, strlen($inventoryStock) - 1)."}";
    $inventoryOrder = substr($inventoryOrder, 0, strlen($inventoryOrder) - 1)."}";
    $insertInv = $bdd->prepare("UPDATE `products_inventory` SET `in_stock` = ?, `on_order` = ? WHERE product_id = ?");
    $insertInv->execute(array($inventoryStock, $inventoryOrder, $productID));

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "product" => '#'.$productID,
    ];
    $insertEvent->execute(array("productUpdated", json_encode($details), date("Y-m-d H:i:s")));
  }

  function updateInventory($data, $productID, $lang, $bdd, $adminInfos) {
    if($data["inventory"] == "stock") {
      $updateInv = $bdd->prepare("UPDATE `products_inventory` SET `in_stock` = ? WHERE `product_id` = ?");
    } elseif($data["inventory"] == "order") {
      $updateInv = $bdd->prepare("UPDATE `products_inventory` SET `on_order` = ? WHERE `product_id` = ?");
    }
    $updateInv->execute(array(json_encode($data["stock"]), $productID));

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "product" => '#'.$productID,
    ];
    $insertEvent->execute(array("inventoryUpdated", json_encode($details), date("Y-m-d H:i:s")));
  }

  function deleteProduct($data, $productID, $lang, $bdd, $adminInfos) {
    if($data["delete"]) {
      //Delete the product
      $deleteproduct = $bdd->prepare("DELETE FROM products WHERE id = ?");
      $deleteproduct->execute(array($productID));
      //Delete the details
      $deleteproduct = $bdd->prepare("DELETE FROM products_details WHERE product_id = ?");
      $deleteproduct->execute(array($productID));
      //Delete the options
      $deleteproduct = $bdd->prepare("DELETE FROM products_options WHERE product_id = ?");
      $deleteproduct->execute(array($productID));
      //Delete the inventory
      $deleteproduct = $bdd->prepare("DELETE FROM products_inventory WHERE product_id = ?");
      $deleteproduct->execute(array($productID));
      //Delete the docs
      $deleteproduct = $bdd->prepare("DELETE FROM products_docs WHERE product_id = ?");
      $deleteproduct->execute(array($productID));

      //Insert the success event
      $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
      $details = [
        "username" => $adminInfos["username"],
        "product" => '#'.$productID,
      ];
      $insertEvent->execute(array("productDeleted", json_encode($details), date("Y-m-d H:i:s")));
    }
  }

  function addProduct($data, $lang, $bdd, $adminInfos){
    //Insert the product
    //subserie need to be at least 1 (none)
    if($data["product"]["selected"]["subseries"] == 0) $data["product"]["selected"]["subseries"] = 1;

    $insertProduct = $bdd->prepare("INSERT INTO `products`(`product_name`, `sequence`, `maker_id`, `category_id`, `type_id`, `serie_id`, `subserie_id`, `desc`, `base_price`, `branches`, `img`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $insertProduct->execute(array($data["product"]["productname"], $data["product"]["sequence"], $data["product"]["selected"]["makers"], $data["product"]["selected"]["categories"], $data["product"]["selected"]["types"], $data["product"]["selected"]["series"], $data["product"]["selected"]["subseries"], json_encode($data["product"]["productdesc"]), $data["product"]["productprice"], json_encode($data["product"]["selected"]["branches"]), $data["product"]["productimage"]));
    $productID = $bdd->lastInsertId();
    //Insert details
    $productDetails = [];
    if(isset($data["product"]["defaultspecs"])) {
      foreach ($data["product"]["defaultspecs"] as $key => $spec) {
        $spec_id = $spec["id"];
        $args = [];
        foreach ($spec["specs"] as $key2 => $value) {
          array_push($args, $value["arg"]);
        }
        array_push($productDetails, ["id" => $spec_id, "args" => $args]);
      }
    }
    $productDetails = json_encode($productDetails);
		$insertDetails = $bdd->prepare("INSERT INTO `products_details`(`product_id`, `details`) VALUES (?, ?)");
		$insertDetails->execute(array($productID, $productDetails));
		//Insert options
    $productOptions = [];
    if(isset($data["product"]["defaultopts"])) {
      foreach ($data["product"]["defaultopts"] as $key => $option) {
        $opt_id = $option["id"];
        $args = [];
        foreach ($option["options"] as $key2 => $value) {
          array_push($args, $value["arg"]);
        }
        array_push($productOptions, ["id" => $opt_id, "args" => $args]);
      }
    }
    $productOptions = json_encode($productOptions);
    $insertOptions = $bdd->prepare("INSERT INTO `products_options`(`product_id`, `options`) VALUES (?, ?)");
    $insertOptions->execute(array($productID, $productOptions));

		//Save the documents
		$productDocs = [];
		if(isset($data["product"]["addeddocs"])) {
			foreach ($data["product"]["addeddocs"] as $key => $value) {
				if(!empty($value["name"][$lang['Short']])) {
          array_push($productDocs, $value);
        }
			}
		}
    $productDocs = json_encode($productDocs);
    //Insert documents
		$insertDocs = $bdd->prepare("INSERT INTO `products_docs`(`product_id`, `docs`) VALUES (?, ?)");
    $insertDocs->execute(array($productID, $productDocs));
    //Insert inventory (empty)
    $inventory = "{";
    foreach ($data["product"]["selected"]["branches"] as $key => $value) {
      if($value == 'true') {
        $inventory .= '"'.$key.'":"0",';
      }
    }
    $inventory = substr($inventory, 0, strlen($inventory) - 1)."}";
    $insertInv = $bdd->prepare("INSERT INTO `products_inventory`(`product_id`, `in_stock`, `on_order`) VALUES (?, ?, ?)");
    $insertInv->execute(array($productID, $inventory, $inventory));

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "product" => '#'.$productID,
    ];
    $insertEvent->execute(array("productAdded", json_encode($details), date("Y-m-d H:i:s")));
  }

  function addProductMass($data, $lang, $bdd, $adminInfos){
    //Insert the product
    //subserie need to be at least 1 (none)
    if($data["product"]["subserie_id"] == 0) $data["product"]["subserie_id"] = 1;
    if($data["product"]["serie_id"] == 0) $data["product"]["serie_id"] = 1;

    $insertProduct = $bdd->prepare("INSERT INTO `products`(`product_name`, `maker_id`, `category_id`, `type_id`, `serie_id`, `subserie_id`, `desc`, `base_price`, `branches`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $insertProduct->execute(array($data["product"]["product_name"], $data["product"]["maker_id"], $data["product"]["category_id"], $data["product"]["type_id"], $data["product"]["serie_id"], $data["product"]["subserie_id"], $data["product"]["desc"], $data["product"]["listprice"], $data["product"]["branches"]));
    $productID = $bdd->lastInsertId();
    //Insert details
    $productDetails = $data["product"]["specs"];
		$insertDetails = $bdd->prepare("INSERT INTO `products_details`(`product_id`, `details`) VALUES (?, ?)");
		$insertDetails->execute(array($productID, $productDetails));
		//Insert options
    $productOptions = $data["product"]["options"];
    $insertOptions = $bdd->prepare("INSERT INTO `products_options`(`product_id`, `options`) VALUES (?, ?)");
    $insertOptions->execute(array($productID, $productOptions));

		//Save the documents
    $productDocs = $data["product"]["docs"];
    //Insert documents
		$insertDocs = $bdd->prepare("INSERT INTO `products_docs`(`product_id`, `docs`) VALUES (?, ?)");
    $insertDocs->execute(array($productID, $productDocs));
    //Insert inventory (empty)
    $inventory = "{";
    foreach (json_decode($data["product"]["branches"], true) as $key => $value) {
      if($value == 'true') {
        $inventory .= '"'.$key.'":"0",';
      }
    }
    $inventory = substr($inventory, 0, strlen($inventory) - 1)."}";
    $insertInv = $bdd->prepare("INSERT INTO `products_inventory`(`product_id`, `in_stock`, `on_order`) VALUES (?, ?, ?)");
    $insertInv->execute(array($productID, $inventory, $inventory));

    //Insert the success event
    $insertEvent = $bdd->prepare("INSERT INTO events (type, details, time) VALUES (?, ?, ?)");
    $details = [
      "username" => $adminInfos["username"],
      "product" => '#'.$productID,
    ];
    $insertEvent->execute(array("productAdded", json_encode($details), date("Y-m-d H:i:s")));
  }

?>