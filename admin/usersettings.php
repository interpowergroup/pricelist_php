<?php
  include('../shared/config.php');
  $activeURL = "usersettings";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {

      if(isset($_GET["success"])) {
        $error = [
          "type" => "success",
          "msg" => $lang["Errors"]["UpdateSucessful"]
        ];
      }

      //Update
      if(isset($_POST['update_usersettings'])) {
        $settings = [
          "attemptBefore_recapchat" => htmlspecialchars($_POST['attemptBefore_recapchat']),
          "passwordComplexity" => htmlspecialchars($_POST['passwordComplexity']),
          "exchangerate" => htmlspecialchars($_POST["exchangerate"]),
        ];

        if(!empty($settings["attemptBefore_recapchat"]) && !empty($settings["passwordComplexity"])) {
          if(is_numeric($settings["attemptBefore_recapchat"]) && is_numeric($settings["exchangerate"])) {
            if($settings["attemptBefore_recapchat"] >= 1 && ($settings["passwordComplexity"] == "L1" || $settings["passwordComplexity"] == "L2" || $settings["passwordComplexity"] == "L3")) {
              foreach ($settings as $key => $value) {
                $updatesetting = $bdd->prepare("UPDATE user_parameters SET value = ? WHERE name = ?");
                $updatesetting->execute(array($value, $key));
              }
              header("Refresh:0; url=?success"); 
            } else {
              $error = [
                "type" => "danger",
                "msg" => $lang["Errors"]["UnexpectedValue"]
              ];
            }
          } else {
            $error = [
              "type" => "danger",
              "msg" => $lang["Errors"]["FieldAreNotNumerical"]
            ];
          }
        } else {
          $error = [
            "type" => "danger",
            "msg" => $lang["Errors"]["SomethingIsMissing"]
          ];
        }
      }

    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["UserSettings"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;margin-bottom:20px;">
            <h2 class="title"><?= $lang["Admin"]["UserSettings"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
            <div class="column is-12">
              <div class="card">
                <div class="card-content">
                  <div class="content">
                    <form method="POST" name="setting_form">
                      <div class="field">
                        <div class="field-label is-normal">
                          <label class="label"><?= $lang["Admin"]["UserSettings"]["MaxPasswordAttemps"]["Number"] ?></label>
                        </div>
                        <div class="field-body is-horizontal">
                          <div class="field">
                            <p class="control is-expanded has-icons-left">
                              <input class="input" type="number" autocomplete="off" name="attemptBefore_recapchat" min="1" placeholder="<?= $lang["Admin"]["UserSettings"]["MaxPasswordAttemps"]["Number"] ?>" oldValue="<?= $userconfig['attemptBefore_recapchat'] ?>" value="<?= $userconfig['attemptBefore_recapchat'] ?>">
                              <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                              </span>
                            </p>
                            <p class="help is-danger"></p>
                          </div>
                        </div>
                      </div>
                      <div class="field">
                        <div class="field-label is-normal">
                          <label class="label"><?= $lang["Admin"]["UserSettings"]["PasswordComplexity"]["Header"] ?></label>
                        </div>
                        <div class="field-body is-horizontal">
                          <div class="field">
                            <div class="control is-expanded has-icons-left" style="width:100%;">
                              <div class="select" style="width:100%;">
                                <select style="width:100%;" name="passwordComplexity" oldValue="<?= $userconfig["passwordComplexity"] ?>">
                                  <option <?php if($userconfig["passwordComplexity"] == "L1") { echo 'selected'; } ?> value="L1"><?= $lang["Admin"]["UserSettings"]["PasswordComplexity"]["L1"] ?></option>
                                  <option <?php if($userconfig["passwordComplexity"] == "L2") { echo 'selected'; } ?> value="L2"><?= $lang["Admin"]["UserSettings"]["PasswordComplexity"]["L2"] ?></option>
                                  <option <?php if($userconfig["passwordComplexity"] == "L3") { echo 'selected'; } ?> value="L3"><?= $lang["Admin"]["UserSettings"]["PasswordComplexity"]["L3"] ?></option>
                                </select>
                              </div>
                              <div class="icon is-small is-left">
                                <i class="fas fa-key"></i>
                              </div>
                              <p class="help is-danger"></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="field">
                        <div class="field-label is-normal">
                          <label class="label"><?= $lang["Admin"]["UserSettings"]["ExchangeRate"]["Header"] ?></label>
                        </div>
                        <div class="field-body is-horizontal">
                          <div class="field">
                            <p class="control is-expanded has-icons-left">
                              <input class="input" type="number" step="any" autocomplete="off" name="exchangerate" min="0" placeholder="<?= $lang["Admin"]["UserSettings"]["ExchangeRate"]["Header"] ?>" oldValue="<?= $userconfig['exchangerate'] ?>" value="<?= $userconfig['exchangerate'] ?>">
                              <span class="icon is-small is-left">
                                <i class="fas fa-exchange-alt"></i>
                              </span>
                            </p>
                            <p class="help"><?= $lang["Admin"]["UserSettings"]["ExchangeRate"]["Helper"] ?></p>
                          </div>
                        </div>
                      </div>
                      <div class="field is-grouped is-grouped-centered" style="margin-top:20px;">
                        <p class="control update_profile">
                          <input type="submit" class="button is-info update_profile" name="update_usersettings" value="<?= $lang['Update'] ?>">
                        </p>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
  </body>
</html>