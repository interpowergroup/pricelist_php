<?php
  include('../shared/config.php');
  $activeURL = "dashboard";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $reqEmployeeCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM users");
      $employeeCount = $reqEmployeeCount->fetch();
      $reqActiveCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM users WHERE `last_seen` >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)");
      $activeEmployee = $reqActiveCount->fetch();
      $reqBranchesCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM branches");
      $branchesCount = $reqBranchesCount->fetch();
      $reqProductsCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM products");
      $productsCount = $reqProductsCount->fetch();

      $reqEvents = $bdd->query("SELECT * FROM events ORDER BY time DESC LIMIT 25");
    } else {
      header('Location: '. ROOT_PATH .'/logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'/logout?err=NoSession');
  }

  function replacePlaceholder($text, $details) {

      $details = json_decode($details, true);

      if($details == null) {
        $details = ["" => ""];
      }
    
      foreach ($details as $key => $value) {
        $text = str_replace("{".$key."}", $value, $text);
      }
    
    return $text;
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <section class="info-tiles">
            <div class="tile is-ancestor has-text-centered">
              <div class="tile is-parent">
                <article class="tile is-child box">
                  <p class="title"><span class="activeEmployees"><?= $activeEmployee["COUNT"] ?></span>/<?= $employeeCount["COUNT"] ?></p>
                  <p class="subtitle"><?= $lang["Admin"]["Employees"] ?></p>
                </article>
              </div>
              <div class="tile is-parent">
                <article class="tile is-child box">
                  <p class="title"><?= $branchesCount["COUNT"] ?></p>
                  <p class="subtitle"><?= $lang["Admin"]["Branches"] ?></p>
                </article>
              </div>
              <div class="tile is-parent">
                <article class="tile is-child box">
                  <p class="title"><?= $productsCount["COUNT"] ?></p>
                  <p class="subtitle"><?= $lang["Admin"]["Products"] ?></p>
                </article>
              </div>
            </div>
          </section>
          <div class="columns" style="margin-left: 0;">
            <div class="column is-6">
              <div class="card events-card">
                <header class="card-header">
                  <p class="card-header-title">
                  <?= $lang["Admin"]["Events"]["Header"] ?>
                  </p>
                </header>
                <div class="card-table">
                  <div class="content">
                    <table class="table is-fullwidth is-striped">
                      <tbody class="allEvents">
                        <?php while($event = $reqEvents->fetch()) { ?>
                          <tr data-eventid="<?= $event["id"] ?>">
                            <td width="5%">
                              <i class="<?= $lang["Admin"]["Events"][$event["type"]]["Icon"] ?>"></i>
                            </td>
                            <td><?= replacePlaceholder($lang["Admin"]["Events"][$event["type"]]["DisplayText"], $event["details"]) ?></td>
                            <td><?= $event["time"] ?></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <footer class="card-footer">
                  <a href="<?= ROOT_PATH ?>admin/logs" class="card-footer-item"><?= $lang["Admin"]["Events"]["ViewAll"] ?></a>
                </footer>
              </div>
            </div>
            <div class="column is-6">
              <div class="card">
                <header class="card-header">
                  <p class="card-header-title">
                    <?= $lang["Admin"]["ProductSearch"] ?>
                  </p>
                </header>
                <div class="card-content">
                  <div class="content">
                    <div class="control has-icons-left">
                      <form action="productslist" method="get">
                        <input class="input is-large" type="text" autocomplete="off" name="product" placeholder="">
                        <span class="icon is-medium is-left">
                          <i class="fa fa-search"></i>
                        </span>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <header class="card-header">
                  <p class="card-header-title">
                    <?= $lang["Admin"]["EmployeeSearch"] ?>
                  </p>
                </header>
                <div class="card-content">
                  <div class="content">
                    <div class="control has-icons-left">
											<form action="employees" method="get">
												<input class="input is-large" type="text" autocomplete="off" name="employee" placeholder="">
												<span class="icon is-medium is-left">
													<i class="fa fa-search"></i>
												</span>
											</from>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include_once('../shared/scripts.php'); ?>
    <script type="text/javascript">
      $(document).ready(function () {
        var eventType = <?php echo json_encode($lang["Admin"]["Events"]) ?>;
        setInterval(function() { 
          $.ajax({
            type: "get",
            url: "ajax/getEvents",
            data: {"limit_in": 25},
            success: function (response) {
              response = JSON.parse(response);
              response[0].forEach(element => {
                if($('.allEvents tr[data-eventid='+(element["id"])+']').length == 0) {
                  $('.allEvents').prepend(
                      '<tr data-eventid="' + element["id"] + '">' +
                      '  <td width="5%">' +
                      '    <i class="' + eventType[element["type"]]["Icon"] + '"></i>' +
                      '  </td>' +
                      '  <td>' + replacePlaceholder(eventType[element["type"]]["DisplayText"], element["details"]) + '</td>' +
                      '  <td>' + element["time"] + '</td>' +
                      '</tr>'
                   );
                }
              });
              $('.activeEmployees').text(response[1].length);
            }
          });
        }, 5000); //Update each 5secs

        function replacePlaceholder(text, details) {
          try {
            details = JSON.parse(details);
          } catch(e) { }
          let i = 0;
          jQuery.each(details, function(i, val) {
            text = text.replace("{" + i + "}", val);
          });
          return text;
        }
      });
    </script>
  </body>
</html>