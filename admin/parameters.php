<?php
  include('../shared/config.php');
 

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			
			if(isset($_GET["type"]) && !empty($_GET["type"])) {
				$paramType = htmlspecialchars($_GET["type"]);
				$activeURL = $paramType;

				$reqparameters = $bdd->query("SELECT * FROM products_".$paramType);
				$parameters = $reqparameters->fetchAll();

				$reqbranches = $bdd->query("SELECT * FROM branches");
				$branches = $reqbranches->fetchAll();

				$reqoptionsset = $bdd->query("SELECT * FROM products_optionset");
				$options = $reqoptionsset->fetchAll();

				$reqspecsset = $bdd->query("SELECT * FROM products_specset");
				$specs = $reqspecsset->fetchAll();

				if($paramType != 'makers'){
					$product = 'categories';
					if($paramType == 'series') {
						$product = 'types';
						$reqmakers = $bdd->query("SELECT * FROM products_makers");
						$makers = $reqmakers->fetchAll();
					} elseif($paramType == 'subseries') {
						$product = 'series';
					} elseif($paramType == 'categories') {
							$product = 'makers';
					} else {
						$makers = [];
					}
					$reqcats = $bdd->query("SELECT * FROM products_".$product);
					$categories = $reqcats->fetchAll();
				}
			}
    } else {
      header('Location: '. ROOT_PATH .'imgut?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'imgut?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["ParametersList"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/toggle.css" type="text/css" />
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap-multiselect.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["ParametersList"][ucfirst($paramType)] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["ParametersList"]["Table"]["ID"] ?></th>
                              <th data-label="Name"><?= $lang["Admin"]["ParametersList"]["Table"]["Name"] ?></th>
                              <th data-label="Image"><?= $lang["Admin"]["ParametersList"]["Table"]["Image"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["ParametersList"]["Table"]["Actions"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-parameterID="0">
															<td scope="row" data-label="ID"></td>
															<td data-label="Name"></td> 
															<td data-label="Image"></td>
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth addParameter"><i class="fa fa-plus"></i></a></td>
                            </tr>
                            <?php foreach ($parameters as $parameter) { ?>
															<?php
                                $nameWithLanguage = json_decode($parameter["name"], true)[$language];
                              ?>
                              <tr data-parameterID="<?= $parameter["id"] ?>">
                                <td scope="row" data-label="ID"><?= $parameter["id"] ?></td>
                                <td data-label="Name"><?= $nameWithLanguage ?></td> 
                                <td data-label="Image"><img src="<?= $parameter["img"] ?>" style="max-height: 1.75rem;" alt="Image"></td>
                                <td data-label="Actions">
																	<a class="button is-warning is-small editParameter" data-parameterID="<?= $parameter["id"] ?>" data-parameterName="<?= htmlentities($nameWithLanguage, ENT_QUOTES, 'UTF-8') ?>" data-name='<?= htmlentities($parameter["name"], ENT_QUOTES, 'UTF-8') ?>' data-img="<?= $parameter["img"] ?>" data-cats='<?= htmlentities($parameter["availCategories"], ENT_QUOTES, 'UTF-8') ?>' data-discounts='<?php if(isset($parameter["discounts"])) { echo htmlentities($parameter["discounts"], ENT_QUOTES, 'UTF-8'); } ?>' data-multipliers='<?php if(isset($parameter["multipliers"])) { echo htmlentities($parameter["multipliers"], ENT_QUOTES, 'UTF-8'); } ?>' data-specs='<?php if(isset($parameter["specs"])) { echo htmlentities($parameter["specs"], ENT_QUOTES, 'UTF-8'); } ?>' data-makers='<?php if(isset($parameter["makers"])) { echo htmlentities($parameter["makers"], ENT_QUOTES, 'UTF-8'); } ?>' data-transport='<?php if(isset($parameter["transport"])) { echo$parameter["transport"]; } ?>' onclick="editParameter($(this));">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-danger is-small deleteParameter" onclick="deleteParameter($(this));" data-parameterID="<?= $parameter["id"] ?>" data-parameterName="<?= htmlentities($nameWithLanguage, ENT_QUOTES, 'UTF-8') ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			var language = "<?= $language ?>";
			let defaultData = { 
				productspecs: [{
					title: {
						<?php foreach ($availableLanguages as $key => $value) {
							echo '"'.$value.'": "",';
						} ?>
					},
					arg: {
						can: '', us: '',
					}
				}],
			}
			let vmData = {
				productspecs: [
					{
						title: {
							<?php foreach ($availableLanguages as $key => $value) {
								echo '"'.$value.'": "",';
							} ?>
						},
						arg: {
							can: '',
							us: '',
						}
					}
				],
			}
			Vue.component('product-spec', {
        template: `
          <div class="field-body is-horizontal">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input" type="text" autocomplete="off" v-model="spec['title'][lang]" placeholder="<?= $lang["Admin"]["AddProduct"]["Specs"]["Title"] ?>">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input" type="text" autocomplete="off" v-model="spec['arg']['can']" placeholder="<?= $lang["Admin"]["AddProduct"]["Specs"]["Argument"] ?> (CANADA)">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
						<div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input" type="text" autocomplete="off" v-model="spec['arg']['us']" placeholder="<?= $lang["Admin"]["AddProduct"]["Specs"]["Argument"] ?> (US)">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
            <a class="fa fa-trash-alt" @click="$emit('remove')" style="font-size:2rem;"></a>
          </div>
        `,
        props: ['spec', 'index', 'lang']
      });
			
			function editParameter(element) {
				let parameterID = element.attr('data-parameterID');
				let parameterName = element.attr('data-parameterName');
				swal({
					title: "<?= $lang["Admin"]["ParametersList"]["Edit".ucfirst($paramType)]["Header"] ?>",
					html: 
						"<div id=\"productProperties\">" +
						"<div id=\"swal2-content\" style=\"display: block;\"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"][ucfirst($paramType)] ?>: " + parameterName + "</div>" +
						<?php foreach ($availableLanguages as $key => $value) { ?>
							"<input class=\"swal2-input editParameter_name\" autocomplete=\"off\" data-lang=\"<?= $value ?>\" type=\"text\" name=\"parameterName\" placeholder=\"<?= $lang['Admin']["ParametersList"]["Table"]["Name"] ?> (<?= $value ?>)\">" +
						<?php } ?>
						<?php if($paramType != 'makers') { ?>
							"<label for=\"categories\"><?= $lang["Admin"]["ParametersList"][ucfirst($product)] ?></label>" +
							"<select id=\"categories\" multiple=\"multiple\">" +
							<?php foreach ($categories as $cat) { ?>
								"<option value=\"<?= $cat["id"] ?>\"><?= json_decode($cat["name"], true)[$language] ?></option>" +
							<?php } ?>
							"</select>" +
						<?php } ?>
						<?php if($paramType == 'categories') { ?>
							`<label for="specsSet"><?= $lang["Admin"]["AddProduct"]["Specs"]["Header"] ?></label>
							<select id="specsSet" multiple="multiple">
							<?php foreach ($specs as $spec) { ?>
								<option value="<?= $spec["id"] ?>"><?= json_decode($spec["name"], true)[$language] ?></option>
							<?php } ?>
							</select>
							` +		
						<?php } ?>
						<?php if($paramType == 'series') { ?>
							"<label for=\"makers\"><?= $lang["Admin"]["ParametersList"]["Makers"] ?></label>" +
							"<select id=\"makers\" multiple=\"multiple\">" +
							<?php foreach ($makers as $maker) { ?>
								"<option value=\"<?= $maker["id"] ?>\"><?= json_decode($maker["name"], true)[$language] ?></option>" +
							<?php } ?>
							"</select>" +
							`
							<div class="field-label is-normal has-text-centered" style="margin-bottom: 0;margin-top: 1rem;">
								<label><?= $lang["Admin"]["AddProduct"]["Branches"]["Header"] ?></label>
								<p class="help"><?= $lang["Admin"]["AddProduct"]["Branches"]["Help"] ?></p>
							</div>
							<?php foreach ($branches as $branch) { ?>
								<div class="field-label is-small">
									<label class="label"><?= $branch["name"] ?></label>
								</div>
								<div class="field-body is-horizontal">
									<div class="field">
										<p class="control is-expanded has-icons-left">
											<input class="input discount" type="text" autocomplete="off" name="dicount_<?= $branch["id"] ?>" data-branchID="<?= $branch["id"] ?>" placeholder="<?= $lang["Admin"]["AddProduct"]["Branches"]["Discount"] ?>">
											<span class="icon is-small is-left">
												<i class="fas fa-percentage"></i>
											</span>
										</p>
									</div>
									<div class="field">
										<p class="control is-expanded has-icons-left">
											<input class="input multiplier" type="text" autocomplete="off" name="dicount_<?= $branch["id"] ?>" data-branchID="<?= $branch["id"] ?>" placeholder="<?= $lang["Admin"]["AddProduct"]["Branches"]["Multiplier"] ?>">
											<span class="icon is-small is-left">
												<i class="fas fa-percentage"></i>
											</span>
										</p>
									</div>
								</div>
							<?php } ?>
							<label for="optionsSet"><?= $lang["Admin"]["ParametersList"]["OptionsSet"] ?></label>
							<select id="optionsSet" multiple="multiple">
							<?php foreach ($options as $opt) { ?>
								<option value="<?= $opt["id"] ?>"><?= json_decode($opt["name"], true)[$language] ?></option>
							<?php } ?>
							</select>
							<div class="field-label is-normal has-text-centered" style="margin-bottom: 0;margin-top: 1rem;">
								<label><?= $lang["Admin"]["AddProduct"]["TransportPrice"] ?></label>
								<p class="help"><?= $lang["Admin"]["AddProduct"]["Branches"]["Help"] ?></p>
							</div>
								<div class="field-body is-horizontal">
									<div class="field">
										<p class="control is-expanded has-icons-left">
											<input class="input transport" type="text" autocomplete="off" name="transport_price" placeholder="<?= $lang["Admin"]["AddProduct"]["TransportPrice"] ?>">
											<span class="icon is-small is-left">
												<i class="fas fa-percentage"></i>
											</span>
										</p>
									</div>
								</div>
							` +
						<?php } ?>
						"<input class=\"swal2-input editParameter_img\" data-b64=\"none\" autocomplete=\"off\" type=\"file\" onchange=\"encodeImageFileAsURL(this)\" name=\"img\" placeholder=\"<?= $lang['Admin']["ParametersList"]["Table"]["Image"] ?>\">" +
						"<div class=\"upload-image-preview\"><img class=\"preview-image\" src=\"\"></div></div>",
					showCancelButton: true,
					showLoaderOnConfirm: true,
					onOpen: () => {
						<?php if($paramType == 'categories' || $paramType == 'series') { ?>
							var vm = new Vue({
								el: '#productProperties',
								data: vmData,
								methods: {
									addSpecs: function () {
										vmData.productspecs.push({
											title: {
												<?php foreach ($availableLanguages as $key => $value) {
													echo '"'.$value.'": "",';
												} ?>
											},
											arg: {
												can: '',
												us: '',
											}
										});
									},
								}
							});
							<?php if($paramType == 'categories') { ?>
								$('#specsSet').multiselect({
									includeSelectAllOption: true,
									enableFiltering: true
								});
								$('#specsSet').multiselect('select', JSON.parse(element.attr('data-specs')));
							<?php } ?>
							<?php if($paramType == 'series') { ?>
								$('#optionsSet').multiselect({
								includeSelectAllOption: true,
								enableFiltering: true
							});
							$('#optionsSet').multiselect('select', JSON.parse(element.attr('data-specs')));
							$('.transport').val(element.attr('data-transport'));
							<?php } ?>
						<?php } ?>
						<?php if($paramType != 'makers') { ?>
							$('#categories').multiselect({
								includeSelectAllOption: true,
								enableFiltering: true
							});
							$('#categories').multiselect('select', JSON.parse(element.attr('data-cats')));
						<?php } ?>
						<?php if($paramType == 'series') { ?>
							$('#makers').multiselect({
								includeSelectAllOption: true,
								enableFiltering: true
							});
							$('#makers').multiselect('select', JSON.parse(element.attr('data-makers')));
							$('input.discount').each(function() {
								$(this).val(JSON.parse(element.attr('data-discounts'))[$(this).attr('data-branchID')]);
							});
							$('input.multiplier').each(function() {
								$(this).val(JSON.parse(element.attr('data-multipliers'))[$(this).attr('data-branchID')]);
							});
						<?php } ?>
            let titles = JSON.parse(element.attr('data-name'));
						$('.editParameter_name').each(function() {
              $(this).val(titles[$(this).attr('data-lang')]);
            });
						$('.preview-image').attr('src', element.attr('data-img'));
						$('.editParameter_img').attr('data-b64', element.attr('data-img'));
					},
					preConfirm: () => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'parameterID': parameterID,
							'data': {
								'type': 'parameter_update',
								'param': '<?= $paramType ?>',
								'names': { },
								'img': $('.editParameter_img').attr('data-b64'),
							}
						}
						let langNames = new Object();
						$('.editParameter_name').each(function() {
							langNames[$(this).attr('data-lang')] = $(this).val();
						}); 
						data.data.names = langNames;
						<?php if($paramType != 'makers') { ?>
							data.data.cats = $('#categories').val();
						<?php } ?>
						<?php if($paramType == 'series') { ?>
							data.data.makers = $('#makers').val();
							data.data.specs = $('#optionsSet').val();
							data.data.discount = new Object();
							data.data.multiplier = new Object();
							data.data.transport = $('.transport').val();
							$('input.discount').each(function() {
								data.data.discount[$(this).attr('data-branchID')] = $(this).val();
							});
							$('input.multiplier').each(function() {
								data.data.multiplier[$(this).attr('data-branchID')] = $(this).val();
							});
						<?php } ?>
						<?php if($paramType == 'categories') { ?>
							data.data.specs = $('#specsSet').val();
						<?php } ?>
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeparameter",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-parameterID="' + parameterID + '"]';
							$(tr).find('td[data-label="Name"]').text(result.value.data.names.<?= $language ?>);
							$(tr).find('td[data-label="Location"]').text(result.value.data.location);
							$(tr).find('td[data-label="Image"]').find('img').attr('src', result.value.data.img);
							<?php if($paramType != 'makers') { ?>
								$(tr).find('.editParameter').attr('data-cats', result.value.data.cats);
							<?php } ?>
							<?php if($paramType == 'series') { ?>
								$(tr).find('.editParameter').attr('data-makers', result.value.data.makers);
								$(tr).find('.editParameter').attr('data-specs', result.value.data.specs);
								$(tr).find('.editParameter').attr('data-discounts', result.value.data.discount);
								$(tr).find('.editParameter').attr('data-multipliers', result.value.data.multiplier);
								$(tr).find('.editParameter').attr('data-transport', result.value.data.transport);
							<?php } ?>
							<?php if($paramType == 'categories') { ?>
								$(tr).find('.editParameter').attr('data-specs', result.value.data.specs);
							<?php } ?>
							$(tr).find('.editParameter').attr('data-parameterName', result.value.data.names.<?= $language ?>);
							$(tr).find('.editParameter').attr('data-name', JSON.stringify(result.value.data.names));
							$(tr).find('.deleteParameter').attr('data-parameterName', result.value.data.names.<?= $language ?>);
							$(tr).find('.deleteParameter').attr('data-name', JSON.stringify(result.value.data.names));
							$(tr).find('.editParameter').attr('data-img', result.value.data.img);
						}
					}
				});
			}

			function deleteParameter(element) {
				let parameterID = element.attr('data-parameterID');
				let parameterName = element.attr('data-parameterName');
				swal({
					title: "<?= $lang["Admin"]["ParametersList"]["Delete".ucfirst($paramType)]["Header"] ?>",
					text: "<?= $lang['Admin']["SideMenu"]["Products"]["Settings"][ucfirst($paramType)] ?>: " + parameterName,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'parameterID': parameterID,
							'data': {
								'type': 'parameter_delete',
								'param': '<?= $paramType ?>',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeparameter",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						}).then(function() {
							location.reload();
						});
					}
				});
			}

			//TO laod the image in b64
			function encodeImageFileAsURL(element) {
				var file = element.files[0];
				var reader = new FileReader();
				reader.onloadend = function() {
					$(element).attr('data-b64', reader.result);
					$(element).parent().find('.preview-image').attr('src', reader.result);
				}
				reader.readAsDataURL(file);
			}

      $(document).ready(function () {
				$('.addParameter').click(function() {
          swal({
            title: "<?= $lang["Admin"]["ParametersList"]["Add".ucfirst($paramType)]["Header"] ?>",
						html: 
							"<div id=\"productProperties\">" +
							<?php foreach ($availableLanguages as $key => $value) { ?>
								"<input class=\"swal2-input addParameter_name\" autocomplete=\"off\" data-lang=\"<?= $value ?>\" type=\"text\" name=\"parameterName\" placeholder=\"<?= $lang['Admin']["ParametersList"]["Table"]["Name"] ?> (<?= $value ?>)\">" +
							<?php } ?>
							<?php if($paramType != 'makers') { ?>
								"<label for=\"categories\"><?= $lang["Admin"]["ParametersList"][ucfirst($product)] ?></label>" +
								"<select id=\"categories\" multiple=\"multiple\">" +
								<?php foreach ($categories as $cat) { ?>
									"<option value=\"<?= $cat["id"] ?>\"><?= json_decode($cat["name"], true)[$language] ?></option>" +
								<?php } ?>
								"</select>" +
							<?php } ?>
							<?php if($paramType == 'categories') { ?>
								`<label for="specsSet"><?= $lang["Admin"]["AddProduct"]["Specs"]["Header"] ?></label>
								<select id="specsSet" multiple="multiple">
								<?php foreach ($specs as $spec) { ?>
									<option value="<?= $spec["id"] ?>"><?= json_decode($spec["name"], true)[$language] ?></option>
								<?php } ?>
								</select>
								` +		
							<?php } ?>
							<?php if($paramType == 'series') { ?>
								"<label for=\"makers\"><?= $lang["Admin"]["ParametersList"]["Makers"] ?></label>" +
								"<select id=\"makers\" multiple=\"multiple\">" +
								<?php foreach ($makers as $maker) { ?>
									"<option value=\"<?= $maker["id"] ?>\"><?= json_decode($maker["name"], true)[$language] ?></option>" +
								<?php } ?>
								"</select>" +
								`<div class="field-label is-normal has-text-centered" style="margin-bottom: 0;margin-top: 1rem;">
									<label><?= $lang["Admin"]["AddProduct"]["Branches"]["Header"] ?></label>
									<p class="help"><?= $lang["Admin"]["AddProduct"]["Branches"]["Help"] ?></p>
								</div>
								<?php foreach ($branches as $branch) { ?>
									<div class="field-label is-small">
										<label class="label"><?= $branch["name"] ?></label>
									</div>
									<div class="field-body is-horizontal">
										<div class="field">
											<p class="control is-expanded has-icons-left">
												<input class="input discount" type="text" autocomplete="off" name="dicount_<?= $branch["id"] ?>" data-branchID="<?= $branch["id"] ?>" placeholder="<?= $lang["Admin"]["AddProduct"]["Branches"]["Discount"] ?>">
												<span class="icon is-small is-left">
													<i class="fas fa-percentage"></i>
												</span>
											</p>
										</div>
										<div class="field">
											<p class="control is-expanded has-icons-left">
												<input class="input multiplier" type="text" autocomplete="off" name="dicount_<?= $branch["id"] ?>" data-branchID="<?= $branch["id"] ?>" placeholder="<?= $lang["Admin"]["AddProduct"]["Branches"]["Multiplier"] ?>">
												<span class="icon is-small is-left">
													<i class="fas fa-percentage"></i>
												</span>
											</p>
										</div>
									</div>
								<?php } ?>
								<label for="optionsSet"><?= $lang["Admin"]["ParametersList"]["OptionsSet"] ?></label>
								<select id="optionsSet" multiple="multiple">
								<?php foreach ($options as $opt) { ?>
									<option value="<?= $opt["id"] ?>"><?= json_decode($opt["name"], true)[$language] ?></option>
								<?php } ?>
								</select>
								<div class="field-label is-normal has-text-centered" style="margin-bottom: 0;margin-top: 1rem;">
								<label><?= $lang["Admin"]["AddProduct"]["TransportPrice"] ?></label>
								<p class="help"><?= $lang["Admin"]["AddProduct"]["Branches"]["Help"] ?></p>
							</div>
								<div class="field-body is-horizontal">
									<div class="field">
										<p class="control is-expanded has-icons-left">
											<input class="input transport" type="text" autocomplete="off" name="transport_price" placeholder="<?= $lang["Admin"]["AddProduct"]["TransportPrice"] ?>">
											<span class="icon is-small is-left">
												<i class="fas fa-percentage"></i>
											</span>
										</p>
									</div>
								</div>
								` +
							<?php } ?>
							"<input class=\"swal2-input addParameter_img\" data-b64=\"none\" autocomplete=\"off\" type=\"file\" onchange=\"encodeImageFileAsURL(this)\" name=\"img\" placeholder=\"<?= $lang['Admin']["ParametersList"]["Table"]["Image"] ?>\">" +
							"<div class=\"upload-image-preview\"><img class=\"preview-image\" src=\"\"></div></div>",
            showCancelButton: true,
						showLoaderOnConfirm: true,
						onOpen: () => {
							<?php if($paramType == 'categories' || $paramType == 'series') { ?>
								var vm = new Vue({
									el: '#productProperties',
									data: vmData,
									methods: {
										addSpecs: function () {
											vmData.productspecs.push({
												title: {
													<?php foreach ($availableLanguages as $key => $value) {
														echo '"'.$value.'": "",';
													} ?>
												},
												arg: {
													can: '',
													us: '',
												}
											});
										},
									}
								});
								<?php if($paramType == 'categories') { ?>
									$('#specsSet').multiselect({
										includeSelectAllOption: true,
										enableFiltering: true
									});
								<?php } ?>
							<?php } ?>
							<?php if($paramType != 'makers') { ?>
								$('#categories').multiselect({
									includeSelectAllOption: true,
									enableFiltering: true
								});
							<?php } ?>
							<?php if($paramType == 'series') { ?>
								$('#makers').multiselect({
									includeSelectAllOption: true,
									enableFiltering: true
								});
								$('#optionsSet').multiselect({
									includeSelectAllOption: true,
									enableFiltering: true
								});
							<?php } ?>
						},
						preConfirm: (password) => {
							let data = { 
								'adminID': "<?= $user["id"] ?>",
								"adminPassword": "<?= $user["password"] ?>",
                'parameterID': '1',
								'data': {
									'type': 'parameter_add',
									'names': {},
									'param': '<?= $paramType ?>',
									'img': $('.addParameter_img').attr('data-b64'),
								}
							}
							let langNames = new Object();
              $('.addParameter_name').each(function() {
                langNames[$(this).attr('data-lang')] = $(this).val();
              }); 
							data.data.names = langNames;
							<?php if($paramType != 'makers') { ?>
								data.data.cats = $('#categories').val();
							<?php } ?>
							<?php if($paramType == 'series') { ?>
								data.data.makers = $('#makers').val();
								data.data.specs = $('#optionsSet').val();
								data.data.discount = new Object();
								data.data.multiplier = new Object();
								data.data.transport = $('.transport').val();
								$('input.discount').each(function() {
									data.data.discount[$(this).attr('data-branchID')] = $(this).val();
								});
								$('input.multiplier').each(function() {
									data.data.multiplier[$(this).attr('data-branchID')] = $(this).val();
								});
							<?php } ?>
							<?php if($paramType == 'categories') { ?>
								data.data.specs = $('#specsSet').val();
							<?php } ?>
							return $.ajax({
								type: "POST",
								url: "<?= ROOT_PATH ?>admin/ajax/changeparameter",
								data: data,
								dataType: "json",
								success: function (response) {
									return response;
								}
							}).fail(function( jqXHR, textStatus ) {
								alert(`Request failed: ${textStatus}`)
							});
						},
						allowOutsideClick: () => !swal.isLoading()
					}).then((result) => {
						if (result.value) {
							swal({
								title: `${result.value.text}`,
								type: `${result.value.type}`,
							}).then(function() {
								location.reload();
							});
						}
					});
        });

      });
    
    </script>
  </body>
</html>