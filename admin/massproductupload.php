<?php
  include('../shared/config.php');


  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $activeURL = 'massupload';

      $reqmakers = $bdd->query("SELECT * FROM products_makers");
      $makers = $reqmakers->fetchAll();
      $reqcategories = $bdd->query("SELECT * FROM products_categories");
      $categories = $reqcategories->fetchAll();
      $reqtypes = $bdd->query("SELECT * FROM products_types");
      $types = $reqtypes->fetchAll();
      $reqseries = $bdd->query("SELECT * FROM products_series");
      $series = $reqseries->fetchAll();
      $reqsubseries = $bdd->query("SELECT * FROM products_subseries");
      $subseries = $reqsubseries->fetchAll();
      $reqbranches = $bdd->query("SELECT * FROM branches");
      $branches = $reqbranches->fetchAll();
      $reqoptionsset = $bdd->query("SELECT * FROM products_optionset");
      $optionsset = $reqoptionsset->fetchAll();

    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["MassUpload"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap-multiselect.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
    <style>
      .is-loading-div.is-loading {
  position: relative;
  pointer-events: none;
  opacity: 0.5;
}
.is-loading-div.is-loading:after {
  -webkit-animation: spinAround 0.5s infinite linear;
  animation: spinAround 0.5s infinite linear;
  border: 2px solid #dbdbdb;
  border-radius: 290486px;
  border-right-color: transparent;
  border-top-color: transparent;
  content: "";
  display: block;
  position: absolute;
  top: calc(50% - 2.5em);
  left: calc(50% - 2.5em);
  width: 5em;
  height: 5em;
  border-width: 0.25em;
}
    </style>
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["MassUpload"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">

												<div id="productProperties">
                          <form method="post" enctype="multipart/form-data">
                            <input type="file" name="fileUpload" @change="filesChange($event.target.name, $event.target.files);"  id="fileUpload" style="display: none;"/>
                            <label for="fileUpload" class="button is-link is-medium is-fullwidth" :class="{'is-loading': isLoading}"><?= $lang["Upload"] ?></label>
                            <br>
                            <a href="<?= ROOT_PATH ?>assets/misc/MassUploadPricelist.csv" download class="button is-light is-small is-fullwidth"><?= $lang["Admin"]["MassUpload"]["DownloadCSV"] ?></a>
                            <a href="productjsongenerator" class="button is-light is-small is-fullwidth"><?= $lang["Admin"]["MassUpload"]["JSONGenerator"] ?></a>
                          </form>
												</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
    <script src="<?= ROOT_PATH ?>assets/js/papaparse.min.js"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        var language = "<?= $language ?>";
        let vmData = {
          isLoading: false,
          makers: <?= json_encode($makers) ?>,
          categories: <?= json_encode($categories) ?>,
          types: <?= json_encode($types) ?>,
          series: <?= json_encode($series) ?>,
          subseries: <?= json_encode($subseries) ?>,
          branches: <?= json_encode($branches) ?>,
          optionsset: <?= json_encode($optionsset) ?>,
          productname: '',
          productdesc: {
            <?php foreach ($availableLanguages as $key => $value) {
              echo '"'.$value.'": "",';
            } ?>
          },
          productprice: '',
          productimage: '',
          defaultspecs: [],
          addedspecs: [],
          defaultopts: [],
          addedopts: [],
          addeddocs: [],
          selected: {
            makers: '',
            categories: '',
            types: '',
            series: '',
            subseries: '',
            branches: {}
          },
        }

        let arrayTypes = {
          specs: {
            title: {
              <?php foreach ($availableLanguages as $key => $value) {
                echo '"'.$value.'": "",';
              } ?>
            },
            arg: {
              can: '', 
              us: '',
            }
          },
          docs: {
            name: {
              <?php foreach ($availableLanguages as $key => $value) {
                echo '"'.$value.'": "",';
              } ?>
            },
            file: ''
          }
        }

        var vm = new Vue({
          el: '#productProperties',
          data: vmData,
          methods: {
            filesChange(fieldName, fileList) {
              vmData.isLoading = true;
              Array.from(fileList).forEach(file => {
                if(file.type == "application/vnd.ms-excel") {
                  Papa.parse(file, { //Parse the csv as an array
                    header: true,
                    step: function(row) {
                      if(Object.keys(row.data[0]).length > 1) {
                        let data = {
                          'userID': "<?= $user["id"] ?>",
                          "userPassword": "<?= $user["password"] ?>",
                          'productID': 1,
                          'data': {
                            'type': 'product_add_mass',
                            'product': row.data[0]
                          }
                        }
                        $.ajax({
                          type: "POST",
                          url: "<?= ROOT_PATH ?>admin/ajax/changeproduct",
                          data: data,
                          dataType: "json",
                          success: function (response) {
                            
                          }
                        }).fail(function( jqXHR, textStatus ) {
                          alert(`Request failed: ${textStatus}`)
                        });
                      }
                    },
                    complete: function(results) {
                      swal({
                        title: `<?= $lang["Admin"]["MassUpload"]["Completed"] ?>`,
                        type: `success`,
                      }).then(() => {
                        vmData.isLoading = false;
                      });
                    }
                  });
                } else {
                  swal({
                    title: `<?= $lang["Admin"]["MassUpload"]["NotCSV"] ?>`,
                    type: 'error',
                  });
                }
              });
            },
          }
        });
      });
    </script>
  </body>
</html>