<?php
  include('../shared/config.php');
  $activeURL = "branches";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			
			$reqbranches = $bdd->query("SELECT * FROM branches");
			$branches = $reqbranches->fetchAll();
    } else {
      header('Location: '. ROOT_PATH .'/logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'/logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["BranchesList"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["BranchesList"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["BranchesList"]["Table"]["ID"] ?></th>
                              <th data-label="Name"><?= $lang["Admin"]["BranchesList"]["Table"]["Name"] ?></th>
                              <th data-label="Location"><?= $lang["Admin"]["BranchesList"]["Table"]["Location"] ?></th>
                              <th data-label="Logo"><?= $lang["Admin"]["BranchesList"]["Table"]["Logo"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["BranchesList"]["Table"]["Actions"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-branchID="0">
															<td scope="row" data-label="ID"></td>
															<td data-label="Name"></td> 
															<td data-label="Location"></td>
															<td data-label="Logo"></td>
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth addBranch"><i class="fa fa-plus"></i></a></td>
                            </tr>
                            <?php foreach ($branches as $branch) { ?>
                              <tr data-branchID="<?= $branch["id"] ?>">
                                <td scope="row" data-label="ID"><?= $branch["id"] ?></td>
                                <td data-label="Name"><?= $branch["name"] ?></td> 
                                <td data-label="Location"><?= $branch["location"] ?></td>
                                <td data-label="Logo"><img src="<?= $branch["logo"] ?>" style="max-height: 1.75rem;" alt="Logo"></td>
                                <td data-label="Actions">
																	<a class="button is-warning is-small editBranch" data-branchID="<?= $branch["id"] ?>" data-branchName="<?= $branch["name"] ?>" data-name="<?= $branch["name"] ?>" data-location="<?= $branch["location"] ?>" data-spectype="<?= $branch["spectype"] ?>" data-logo="<?= $branch["logo"] ?>" onclick="editBranch($(this));">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-danger is-small deleteBranch" onclick="deleteBranch($(this));" data-branchID="<?= $branch["id"] ?>" data-branchName="<?= $branch["name"] ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			function editBranch(element) {
				let branchID = element.attr('data-branchID');
				let branchName = element.attr('data-branchName');
				swal({
					title: "<?= $lang["Admin"]["BranchesList"]["EditBranch"]["Header"] ?>",
					html: 
						"<div id=\"swal2-content\" style=\"display: block;\"><?= $lang["Admin"]["Branch"] ?>: " + branchName + "</div>" +
						"<input class=\"swal2-input editBranch_name\" autocomplete=\"off\" type=\"text\" name=\"name\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Name"] ?>\">"+
						"<input class=\"swal2-input editBranch_location\" autocomplete=\"off\" type=\"text\" name=\"location\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Location"] ?>\">" +
						"<div class=\"toggle\" style=\"justify-content:center;\">" +
						"<div class=\"toggle_child\">" +
						"<input class=\"editBranch_spectype\" autocomplete=\"off\" type=\"radio\" value=\"can\" id=\"spectype_can\" name=\"spectype\"\"><label for=\"spectype_can\">CAN</label>" +
						"</div>" +
						"<div class=\"toggle_child\">" +
						"<input class=\"editBranch_spectype\" autocomplete=\"off\" type=\"radio\" value=\"us\" id=\"spectype_us\" name=\"spectype\"\"><label for=\"spectype_us\">US</label>" +
						"</div>" +
						"</div>" +
						"<input class=\"swal2-input editBranch_logo\" autocomplete=\"off\" type=\"file\" onchange=\"encodeImageFileAsURL(this)\" name=\"logo\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Logo"] ?>\">" +
						"<div class=\"upload-image-preview\"><img class=\"preview-image\" src=\"\"></div>",
					showCancelButton: true,
					showLoaderOnConfirm: true,
					onOpen: () => {
						$('.editBranch_name').val(element.attr('data-name'));
						$('.editBranch_location').val(element.attr('data-location'));
						$('.preview-image').attr('src', element.attr('data-logo'));
						$('.editBranch_logo').attr('data-b64', element.attr('data-logo'));
						$('.editBranch_spectype').each(function() {
							if($(this).val() == element.attr('data-spectype')) {
								return $(this).prop('checked', true);
							}
						});
					},
					preConfirm: () => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'branchID': branchID,
							'data': {
								'type': 'branch_update',
								'name': $('.editBranch_name').val(),
								'location': $('.editBranch_location').val(),
								'spectype': '',
								'logo': $('.editBranch_logo').attr('data-b64'),
							}
						}
						$('.editBranch_spectype').each(function() {
							if($(this).is(':checked')) {
								return data.data.spectype = $(this).val();
							}
						});
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changebranch",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-branchID="' + branchID + '"]';
							$(tr).find('td[data-label="Name"]').text(result.value.data.name);
							$(tr).find('td[data-label="Location"]').text(result.value.data.location);
							$(tr).find('td[data-label="Logo"]').find('img').attr('src', result.value.data.logo);

							$(tr).find('.editBranch').attr('data-name', result.value.data.name);
							$(tr).find('.editBranch').attr('data-branchName', result.value.data.name);
							$(tr).find('.deleteBranch').attr('data-branchName', result.value.data.name);
							$(tr).find('.editBranch').attr('data-location', result.value.data.location);
							$(tr).find('.editBranch').attr('data-logo', result.value.data.logo);
							$(tr).find('.editBranch').attr('data-spectype', result.value.data.spectype);
						}
					}
				});
			}

			function deleteBranch(element) {
				let branchID = element.attr('data-branchID');
				let branchName = element.attr('data-branchName');
				swal({
					title: "<?= $lang["Admin"]["BranchesList"]["DeleteBranch"]["Header"] ?>",
					text: "<?= $lang["Admin"]["Branch"] ?>: " + branchName,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'branchID': branchID,
							'data': {
								'type': 'branch_delete',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changebranch",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						}).then(function() {
							location.reload();
						});
					}
				});
			}

			//TO laod the image in b64
			function encodeImageFileAsURL(element) {
				var file = element.files[0];
				var reader = new FileReader();
				reader.onloadend = function() {
					$(element).attr('data-b64', reader.result);
					$(element).parent().find('.preview-image').attr('src', reader.result);
				}
				reader.readAsDataURL(file);
			}

      $(document).ready(function () {
				$('.addBranch').click(function() {
          swal({
            title: "<?= $lang["Admin"]["BranchesList"]["AddBranch"]["Header"] ?>",
						html: 
							"<input class=\"swal2-input addBranch_name\" autocomplete=\"off\" type=\"text\" name=\"name\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Name"] ?>\">"+
							"<input class=\"swal2-input addBranch_location\" autocomplete=\"off\" type=\"text\" name=\"location\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Location"] ?>\">" +
							"<label>Type de spécifications: </label>" +
							"<div class=\"toggle\" style=\"justify-content:center;\">" +
							"<div class=\"toggle_child\">" +
							"<input class=\"addBranch_spectype\" type=\"radio\" autocomplete=\"off\" value=\"can\" id=\"spectype_can\" name=\"spectype\"\"><label for=\"spectype_can\">CAN</label>" +
							"</div>" +
							"<div class=\"toggle_child\">" +
							"<input class=\"addBranch_spectype\" type=\"radio\" autocomplete=\"off\" value=\"us\" id=\"spectype_us\" name=\"spectype\"\"><label for=\"spectype_us\">US</label>" +
							"</div>" +
							"</div>" +
							"<input class=\"swal2-input addBranch_logo\" autocomplete=\"off\" type=\"file\" onchange=\"encodeImageFileAsURL(this)\" name=\"logo\" placeholder=\"<?= $lang['Admin']["BranchesList"]["Table"]["Logo"] ?>\">" +
							"<div class=\"upload-image-preview\"><img class=\"preview-image\" src=\"\"></div>",
            showCancelButton: true,
						showLoaderOnConfirm: true,
						preConfirm: (password) => {
							let data = { 
								'adminID': "<?= $user["id"] ?>",
								"adminPassword": "<?= $user["password"] ?>",
                'branchID': '1',
								'data': {
									'type': 'branch_add',
									'name': $('.addBranch_name').val(),
									'location': $('.addBranch_location').val(),
									'spectype': '',
									'logo': $('.addBranch_logo').attr('data-b64'),
								}
							}
							$('.addBranch_spectype').each(function() {
								if($(this).is(':checked')) {
									return data.data.spectype = $(this).val();
								}
							});
							return $.ajax({
								type: "POST",
								url: "<?= ROOT_PATH ?>admin/ajax/changebranch",
								data: data,
								dataType: "json",
								success: function (response) {
									return response;
								}
							}).fail(function( jqXHR, textStatus ) {
								alert(`Request failed: ${textStatus}`)
							});
						},
						allowOutsideClick: () => !swal.isLoading()
					}).then((result) => {
						if (result.value) {
							swal({
								title: `${result.value.text}`,
								type: `${result.value.type}`,
							}).then(function() {
								location.reload();
							});
						}
					});
        });

      });
    
    </script>
  </body>
</html>