<?php
  include('../shared/config.php');
  $activeURL = "employee";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {

			if(isset($_GET["employee"]) && !empty($_GET["employee"])) {
				$reqEmployees = $bdd->query("SELECT * FROM users 
																		WHERE username LIKE '%".htmlspecialchars($_GET["employee"])."%' 
																		OR firstname LIKE '%".htmlspecialchars($_GET["employee"])."%' 
																		OR lastname LIKE '%".htmlspecialchars($_GET["employee"])."%' 
																		OR email LIKE '%".htmlspecialchars($_GET["employee"])."%' ");
			} else {
				$reqEmployees = $bdd->query("SELECT * FROM users");
			}
			$employees = $reqEmployees->fetchAll();
			
			$reqbranches = $bdd->query("SELECT * FROM branches");
			$branches = $reqbranches->fetchAll();

      $passwordPatterns = [
        "L1" => "^(?=.*\d)(?=.*[a-z]).{8,}$",
        "L2" => "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$",
        "L3" => "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$^+=!*()@%&]).{8,}$"
      ];
      $passwordHelp = [
        "L1" => $lang['Admin']["UserSettings"]["PasswordComplexity"]["L1"],
        "L2" => $lang['Admin']["UserSettings"]["PasswordComplexity"]["L2"],
        "L3" => $lang['Admin']["UserSettings"]["PasswordComplexity"]["L3"]
      ];
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["EmployeesList"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["EmployeesList"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["EmployeesList"]["Table"]["ID"] ?></th>
                              <th data-label="Firstname"><?= $lang["Admin"]["EmployeesList"]["Table"]["Firstname"] ?></th>
                              <th data-label="Lastname"><?= $lang["Admin"]["EmployeesList"]["Table"]["Lastname"] ?></th>
                              <th data-label="Username"><?= $lang["Admin"]["EmployeesList"]["Table"]["Username"] ?></th>
                              <th data-label="Mail"><?= $lang["Admin"]["EmployeesList"]["Table"]["Mail"] ?></th>
                              <th data-label="Password"><?= $lang["Admin"]["EmployeesList"]["Table"]["Password"] ?></th>
                              <th data-label="LastSeen"><?= $lang["Admin"]["EmployeesList"]["Table"]["LastSeen"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["EmployeesList"]["Table"]["Actions"]["Header"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-employeeID="<?= $employee["id"] ?>">
															<td scope="row" data-label="ID"></td>
															<td data-label="Firstname"></td> 
															<td data-label="Lastname"></td>
															<td data-label="Username"></td>
															<td data-label="Mail"></td>
															<td data-label="Password"></td>
															<td data-label="LastSeen"></td>
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth addEmployee"><i class="fa fa-plus"></i></a></td>
														</tr>
                            <?php while($employee = array_shift($employees)) { ?>
                              <tr data-employeeID="<?= $employee["id"] ?>">
                                <td scope="row" data-label="ID"><?= $employee["id"] ?></td>
                                <td data-label="Firstname"><?= $employee["firstname"] ?></td> 
                                <td data-label="Lastname"><?= $employee["lastname"] ?></td>
                                <td data-label="Username"><?= $employee["username"] ?></td>
                                <td data-label="Mail"><?= $employee["email"] ?></td>
                                <td data-label="Password"><a class="button is-info is-small resetPassword" data-employeeID="<?= $employee["id"] ?>" data-employeeUsername="<?= $employee["username"] ?>" onclick="resetPassword($(this));"><?= $lang["Admin"]["EmployeesList"]["Table"]["Actions"]["ResetPassword"] ?></a></td>
                                <td data-label="LastSeen"><?= $employee["last_seen"] ?></td>
                                <td data-label="Actions">
																	<a class="button is-warning is-small editEmployee" data-employeeID="<?= $employee["id"] ?>" data-employeeUsername="<?= $employee["username"] ?>" data-firstname="<?= $employee["firstname"] ?>" data-lastname="<?= $employee["lastname"] ?>" data-username="<?= $employee["username"] ?>" data-mail="<?= $employee["email"] ?>" onclick="editUser($(this));">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-info is-small autorizationsEmployee employeeAuth_<?= $employee["id"] ?>" onclick="setAutorizationUser($(this));" data-employeeID="<?= $employee["id"] ?>" data-employeeUsername="<?= $employee["username"] ?>">
																		<i class="fa fa-cog"></i>
																	</a> 
																	<a class="button is-danger is-small deleteEmployee" onclick="deleteUser($(this));" data-employeeID="<?= $employee["id"] ?>" data-employeeUsername="<?= $employee["username"] ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			function resetPassword(element) {
				let employeeID = element.attr('data-employeeID');
				let employeeUsername = element.attr('data-employeeUsername');
				swal({
					title: "<?= $lang["Admin"]["EmployeesList"]["ResetPassword"]["Header"] ?>",
					text: "<?= $lang["Admin"]["Employee"] ?>: " + employeeUsername,
					input: "password",
					inputPlaceholder: "<?= $lang["Admin"]["EmployeesList"]["ResetPassword"]["Placeholder"] ?>",
					showCancelButton: true,
					inputValidator: (value) => {
						if(!value) {
							return "<?= $lang["Errors"]["SomethingIsMissing"] ?>"
						} else {
							if(!new RegExp(<?= "/".$passwordPatterns[$userconfig["passwordComplexity"]]."/" ?>).test(value)) {
								return "<?= $passwordHelp[$userconfig["passwordComplexity"]] ?>";
							}
						}
					},
					showLoaderOnConfirm: true,
					preConfirm: (password) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'employeeID': employeeID,
							'data': {
								'type': 'password_change',
								'password': password
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeemployee",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						})
					}
				});
			}

			function editUser(element) {
				let employeeID = element.attr('data-employeeID');
				let employeeUsername = element.attr('data-employeeUsername');
				swal({
					title: "<?= $lang["Admin"]["EmployeesList"]["EditEmployee"]["Header"] ?>",
					html: 
						"<div id=\"swal2-content\" style=\"display: block;\"><?= $lang["Admin"]["Employee"] ?>: " + employeeUsername + "</div>" +
						"<input class=\"swal2-input editEmployee_firstname\" autocomplete=\"off\" type=\"text\" name=\"firstname\" placeholder=\"<?= $lang["Profile"]["Name"]["Firstname"] ?>\">"+
						"<input class=\"swal2-input editEmployee_lastname\" autocomplete=\"off\" type=\"text\" name=\"lastname\" placeholder=\"<?= $lang["Profile"]["Name"]["Lastname"] ?>\">" +
						"<input class=\"swal2-input editEmployee_username\" autocomplete=\"off\" type=\"text\" name=\"username\" placeholder=\"<?= $lang["Profile"]["Username"]["Header"] ?>\">" +
						"<input class=\"swal2-input editEmployee_mail\" autocomplete=\"off\" type=\"text\" name=\"mail\" placeholder=\"<?= $lang["Profile"]["Mail"]["Header"] ?>\">",
					showCancelButton: true,
					showLoaderOnConfirm: true,
					onOpen: () => {
						$('.editEmployee_firstname').val(element.attr('data-firstname'));
						$('.editEmployee_lastname').val(element.attr('data-lastname'));
						$('.editEmployee_username').val(element.attr('data-username'));
						$('.editEmployee_mail').val(element.attr('data-mail'));

						$('.editEmployee_firstname').keyup(function() {
							if($(this).val() != "") {
								let first = $(this).val().substr(0, 1);
								$('.editEmployee_username').val( $('.editEmployee_username').val().replace($('.editEmployee_username').val().charAt(0), sanitizeUsername(first)) );
							}
						});
						$('.editEmployee_lastname').keyup(function(){
							if($(this).val() != "") {
								$('.editEmployee_username').val( $('.editEmployee_username').val().charAt(0) + sanitizeUsername($(this).val()) );
							}
						});
					},
					preConfirm: (password) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'employeeID': employeeID,
							'data': {
								'type': 'user_update',
								'firstname': $('.editEmployee_firstname').val(),
								'lastname': $('.editEmployee_lastname').val(),
								'username': $('.editEmployee_username').val(),
								'mail': $('.editEmployee_mail').val(),
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeemployee",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-employeeID="' + employeeID + '"]';
							$(tr).find('td[data-label="Firstname"]').text(result.value.data.firstname);
							$(tr).find('td[data-label="Lastname"]').text(result.value.data.lastname);
							$(tr).find('td[data-label="Username"]').text(result.value.data.username);
							$(tr).find('td[data-label="Mail"]').text(result.value.data.mail);

							$(tr).find('.editEmployee').attr('data-firstname', result.value.data.firstname);
							$(tr).find('.editEmployee').attr('data-lastname', result.value.data.lastname);
							$(tr).find('.editEmployee').attr('data-username', result.value.data.username);
							$(tr).find('.editEmployee').attr('data-employeeUsername', result.value.data.username);
							$(tr).find('.deleteEmployee').attr('data-employeeUsername', result.value.data.username);
							$(tr).find('.autorizationsEmployee').attr('data-employeeUsername', result.value.data.username);
							$(tr).find('.resetPassword').attr('data-employeeUsername', result.value.data.username);
							$(tr).find('.editEmployee').attr('data-mail', result.value.data.mail);
						}
					}
				});
			}

			function deleteUser(element) {
				let employeeID = element.attr('data-employeeID');
				let employeeUsername = element.attr('data-employeeUsername');
				swal({
					title: "<?= $lang["Admin"]["EmployeesList"]["DeleteEmployee"]["Header"] ?>",
					text: "<?= $lang["Admin"]["Employee"] ?>: " + employeeUsername,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'employeeID': employeeID,
							'data': {
								'type': 'user_delete',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeemployee",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-employeeID="' + employeeID + '"]';
							$(tr).remove();
						}
					}
				});
			}

			function setAutorizationUser(element) {
				let employeeID = element.attr('data-employeeID');
				window.location.replace("<?= ROOT_PATH ?>admin/employeepermissions?employee="+employeeID);
			}

			function sanitizeUsername(str) {
				str = str.toLowerCase();
				return makeSortString(str);
			}

			function makeSortString(s) {
				if(!makeSortString.translate_re) makeSortString.translate_re = /[āǟḑēīļņōȯȱõȭŗšțūžêîôûŵŷäëïöüẅÿàèìòùẁỳáéíóúẃýø]/g;
				var translate = {
					"â": "a", "ê": "e", "î": "i", "ô": "o", "û": "u", "ŵ": "w", "ŷ": "y", 
					"ä": "a", "ë": "e", "ï": "i", "ö": "o", "ü": "u", "ẅ": "w", "ÿ": "y", 
					"à": "a", "è": "e", "ì": "i", "ò": "o", "ù": "u", "ẁ": "w", "ỳ": "y", 
					"á": "a", "é": "e", "í": "i", "ó": "o", "ú": "u", "ẃ": "w", "ý": "y",
					"ā": "a", "ǟ": "a", "ḑ": "d", "ē": "e", "ī": "i", "ļ": "l", "ņ": "n", 
					"ō": "o", "ȯ": "o", "ȱ": "o", "õ": "o", "ȭ": "o", "ŗ": "r", "š": "s", 
					"ț": "t", "ū": "u", "ž": "z", "ø": "o"
				}
				return ( s.replace(makeSortString.translate_re, function(match) { 
					return translate[match];
				}) );
			}

			//Add the .format() function with {argumentID}
			String.prototype.format = String.prototype.f = function() {
				var s = this,
					i = arguments.length;

				while (i--) {
					s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
				}
				return s;
			}

      $(document).ready(function () {
				$('.addEmployee').click(function() {
          swal({
            title: "<?= $lang["Admin"]["EmployeesList"]["AddEmployee"]["Header"] ?>",
						html: 
							"<input class=\"swal2-input addEmployee_firstname\" autocomplete=\"off\" type=\"text\" name=\"firstname\" placeholder=\"<?= $lang["Profile"]["Name"]["Firstname"] ?>\">"+
							"<input class=\"swal2-input addEmployee_lastname\" autocomplete=\"off\" type=\"text\" name=\"lastname\" placeholder=\"<?= $lang["Profile"]["Name"]["Lastname"] ?>\">" +
							"<input class=\"swal2-input addEmployee_username\" autocomplete=\"off\" type=\"text\" name=\"username\" placeholder=\"<?= $lang["Profile"]["Username"]["Header"] ?>\">" +
							"<input class=\"swal2-input addEmployee_mail\" autocomplete=\"off\" type=\"text\" name=\"mail\" placeholder=\"<?= $lang["Profile"]["Mail"]["Header"] ?>\">" +
							"<input class=\"swal2-input addEmployee_password\" autocomplete=\"off\" type=\"password\" name=\"password\" placeholder=\"<?= $lang["Profile"]["Password"]["Header"] ?>\">",
            showCancelButton: true,
						showLoaderOnConfirm: true,
						onOpen: () => {
							$('.addEmployee_firstname').keyup(function() {
								if($(this).val() != "") {
									let first = $(this).val().substr(0, 1);
									$('.addEmployee_username').val( $('.addEmployee_username').val().replace($('.addEmployee_username').val().charAt(0), sanitizeUsername(first)) );
								}
							});
							$('.addEmployee_lastname').keyup(function(){
								if($(this).val() != "") {
									$('.addEmployee_username').val( $('.addEmployee_username').val().charAt(0) + sanitizeUsername($(this).val()) );
								}
							});
						},
						preConfirm: (password) => {
							let data = { 
								'adminID': "<?= $user["id"] ?>",
								"adminPassword": "<?= $user["password"] ?>",
                'employeeID': '1',
								'data': {
									'type': 'user_add',
									'firstname': $('.addEmployee_firstname').val(),
									'lastname': $('.addEmployee_lastname').val(),
									'username': $('.addEmployee_username').val(),
									'mail': $('.addEmployee_mail').val(),
									'password': $('.addEmployee_password').val()
								}
							}
							return $.ajax({
								type: "POST",
								url: "<?= ROOT_PATH ?>admin/ajax/changeemployee",
								data: data,
								dataType: "json",
								success: function (response) {
									return response;
								}
							}).fail(function( jqXHR, textStatus ) {
								alert(`Request failed: ${textStatus}`)
							});
						},
						allowOutsideClick: () => !swal.isLoading()
					}).then((result) => {
						if (result.value) {
							swal({
								title: `${result.value.text}`,
								type: `${result.value.type}`,
							}).then(function() {
								location.reload();
							});
						}
					});
        });

      });
    
    </script>
  </body>
</html>