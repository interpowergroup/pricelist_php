<?php
  include('../shared/config.php');
  $activeURL = "dashboard";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			if(isset($_GET["clear"]) && is_numeric($_GET["clear"]) && $_GET["clear"] == 1) {
				$bdd->query("TRUNCATE `events`");
				header('Location: logs');
			}
			
      $reqEmployeeCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM users");
      $employeeCount = $reqEmployeeCount->fetch();
			$reqActiveCount = $bdd->query("SELECT * FROM users WHERE `last_seen` >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)");
			$empCount = $reqActiveCount->rowCount();
			$activeEmployee = $reqActiveCount->fetchAll();

			if(isset($_GET["sort"]) && !empty($_GET["sort"])) {
				$sort = htmlspecialchars($_GET["sort"]);
				if($sort == "Logins") {
					$reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%login%' ORDER BY `time` DESC");
				} elseif($sort == "Modifications") {
					$reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%updated%' ORDER BY `time` DESC");
				} elseif($sort == "Deletions") {
					$reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%deleted%' ORDER BY `time` DESC");
				} elseif($sort == "Stocks") {
					$reqEvents = $bdd->query("SELECT * FROM events WHERE `type` LIKE '%inventory%' ORDER BY `time` DESC");
				} else {
					$reqEvents = $bdd->query("SELECT * FROM events ORDER BY `time` DESC");
				}
			} else {
				$sort = 'All';
				$reqEvents = $bdd->query("SELECT * FROM events ORDER BY `time` DESC");
			}
    } else {
      header('Location: '. ROOT_PATH .'/logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'/logout?err=NoSession');
  }

  function replacePlaceholder($text, $details) {

      $details = json_decode($details, true);

      if($details == null) {
        $details = ["" => ""];
      }
    
      foreach ($details as $key => $value) {
        $text = str_replace("{".$key."}", $value, $text);
      }
    
    return $text;
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
			<div class="columns" style="margin-left: 0;">
				<div class="column is-9">
					<div class="card events-card" style="margin-bottom: 0;">
						<header class="card-header">
							<p class="card-header-title">
							<?= $lang["Admin"]["Events"]["Header"] ?>
							</p>
							<div class="dropdown is-hoverable is-right card-header-icon">
								<div class="dropdown-trigger">
									<span class="icon is-small" aria-haspopup="true" aria-controls="dropdown-menu2">
										<i class="fas fa-filter" aria-hidden="true"></i>
									</span>
								</div>
								<div class="dropdown-menu" id="dropdown-menu2" role="menu">
									<div class="dropdown-content">
										<a href="?sort=All" class="dropdown-item"><?= $lang["Admin"]["Events"]["Sort"]["All"] ?></a>
										<a href="?sort=Logins" class="dropdown-item"><?= $lang["Admin"]["Events"]["Sort"]["Logins"] ?></a>
										<a href="?sort=Deletions" class="dropdown-item"><?= $lang["Admin"]["Events"]["Sort"]["Deletions"] ?></a>
										<a href="?sort=Modifications" class="dropdown-item"><?= $lang["Admin"]["Events"]["Sort"]["Modifications"] ?></a>
										<a href="?sort=Stocks" class="dropdown-item"><?= $lang["Admin"]["Events"]["Sort"]["Stocks"] ?></a>
									</div>
								</div>
							</div>
						</header>
						<div class="card-table" style="min-height: 70vh;">
							<div class="content">
								<table class="table is-fullwidth is-striped">
									<tbody class="allEvents">
										<?php while($event = $reqEvents->fetch()) { ?>
											<tr data-eventid="<?= $event["id"] ?>">
												<td width="5%">
													<i class="<?= $lang["Admin"]["Events"][$event["type"]]["Icon"] ?>"></i>
												</td>
												<td><?= replacePlaceholder($lang["Admin"]["Events"][$event["type"]]["DisplayText"], $event["details"]) ?></td>
												<td><?= $event["time"] ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<footer class="card-footer">
							<a class="card-footer-item clearAll"><?= $lang["Admin"]["Events"]["ClearAll"] ?></a>
						</footer>
					</div>
				</div>
				<div class="column is-3">
          <section>
            <div class="tile is-ancestor has-text-centered">
              <div class="tile is-parent">
                <article class="tile is-child box">
                  <p class="title"><span class="activeEmployees"><?= $empCount ?></span>/<?= $employeeCount["COUNT"] ?></p>
                  <p class="subtitle"><?= $lang["Admin"]["Employees"] ?></p>
                </article>
              </div>
            </div>
						<div class="has-text-centered">
							<div class="box employeeList">
							  <?php foreach ($activeEmployee as $employee) { ?>
									<p data-employeeUsername="<?= $employee["username"] ?>"><?= $employee["username"] ?></p>
								<?php } ?>
							</div>
						</div>
          </section>
        </div>
			</div>
			<a href="<?= ROOT_PATH ?>admin" class="button" style="margin-left: 10px;position:absolute;">Retour</a>
		</div>
    <?php include_once('../shared/scripts.php'); ?>
    <script type="text/javascript">
      $(document).ready(function () {
				$('.clearAll').click(function() {
					swal({
						title: "<?= $lang["Admin"]["Events"]["Delete"]["Header"] ?>",
						type: "warning",
						confirmButtonColor: '#d33',
						confirmButtonText: 'Yes, delete it!',
						showCancelButton: true,
						showLoaderOnConfirm: true,
						allowOutsideClick: () => !swal.isLoading()
					}).then((result) => {
						if (result.value) {
							swal({
								title: "<?= $lang["Admin"]["Events"]["Delete"]["Done"] ?>",
								type: 'success',
							}).then(function() {
								location.replace('?clear=1');
							});
						}
					});
				});

        var eventType = <?php echo json_encode($lang["Admin"]["Events"]) ?>;
        setInterval(function() { 
          $.ajax({
            type: "get",
						url: "ajax/getEvents",
						data: {'sort': '<?= $sort ?>'},
            success: function (response) {
              response = JSON.parse(response);
              response[0].forEach(element => {
                if($('.allEvents tr[data-eventid='+(element["id"])+']').length == 0) {
                  $('.allEvents').prepend(
                      '<tr data-eventid="' + element["id"] + '">' +
                      '  <td width="5%">' +
                      '    <i class="' + eventType[element["type"]]["Icon"] + '"></i>' +
                      '  </td>' +
                      '  <td>' + replacePlaceholder(eventType[element["type"]]["DisplayText"], element["details"]) + '</td>' +
                      '  <td>' + element["time"] + '</td>' +
                      '</tr>'
                   );
                }
              });
              $('.activeEmployees').text(response[1].length);
							$('.employeeList').empty();
							response[1].forEach(element => {
								$('.employeeList').prepend(`
									<p data-employeeUsername="${element["username"]}">${element["username"]}</p>
								`)
							});
            }
          });
        }, 5000); //Update each 5secs

        function replacePlaceholder(text, details) {
          try {
            details = JSON.parse(details);
          } catch(e) { }
          let i = 0;
          jQuery.each(details, function(i, val) {
            text = text.replace("{" + i + "}", val);
          });
          return text;
        }
      });
    </script>
  </body>
</html>