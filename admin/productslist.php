<?php
  include('../shared/config.php');
  $activeURL = "productslist";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			if(isset($_GET["product"]) && !empty($_GET["product"])) {
				$reqproducts = $bdd->query("SELECT prods.id, prods.product_name, prods.sequence, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, series.name AS `serie`, subseries.name AS `subserie`, prods.desc, prods.base_price, prods.branches, details.details, opts.options, docs.docs FROM products AS prods 
																		RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																		RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																		RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																		RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																		RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																		RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																		RIGHT JOIN products_options AS opts ON prods.id = opts.product_id 
																		RIGHT JOIN products_docs AS docs ON prods.id = docs.product_id 
																		WHERE prods.product_name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR makers.name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR cats.name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR types.name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR series.name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR subseries.name LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		OR prods.base_price LIKE '%".htmlspecialchars($_GET["product"])."%' 
																		ORDER BY prods.sequence");
			} else {
				$reqproducts = $bdd->query("SELECT prods.id, prods.product_name, prods.sequence, makers.name AS `maker`, cats.name AS `cat`, types.name AS `type`, series.name AS `serie`, subseries.name AS `subserie`, prods.desc, prods.base_price, prods.branches, details.details, opts.options, docs.docs FROM products AS prods 
																		RIGHT JOIN products_makers AS makers ON prods.maker_id = makers.id 
																		RIGHT JOIN products_categories AS cats ON prods.category_id = cats.id 
																		RIGHT JOIN products_types AS types ON prods.type_id = types.id 
																		RIGHT JOIN products_series AS series ON prods.serie_id = series.id 
																		RIGHT JOIN products_subseries AS subseries ON prods.subserie_id = subseries.id 
																		RIGHT JOIN products_details AS details ON prods.id = details.product_id 
																		RIGHT JOIN products_options AS opts ON prods.id = opts.product_id 
																		RIGHT JOIN products_docs AS docs ON prods.id = docs.product_id 
																		ORDER BY prods.sequence");
			}
			$products = $reqproducts->fetchAll();
			
			$reqbranches = $bdd->query("SELECT * FROM branches");
			$branches = $reqbranches->fetchAll();
      
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["ProductsList"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["ProductsList"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["ProductsList"]["Table"]["ID"] ?></th>
                              <th data-label="Name"><?= $lang["Admin"]["ProductsList"]["Table"]["Name"] ?></th>
                              <th data-label="Maker"><?= $lang["Admin"]["ProductsList"]["Table"]["Maker"] ?></th>
                              <th data-label="Category"><?= $lang["Admin"]["ProductsList"]["Table"]["Category"] ?></th>
                              <th data-label="Type"><?= $lang["Admin"]["ProductsList"]["Table"]["Type"] ?></th>
                              <th data-label="Price"><?= $lang["Admin"]["ProductsList"]["Table"]["Price"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["ProductsList"]["Table"]["Actions"]["Header"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-productID="0">
															<td scope="row" data-label="ID"></td>
															<td data-label="Name"></td>
															<td data-label="Maker"></td>
															<td data-label="Category"></td>
															<td data-label="Type"></td>
															<td data-label="Price"></td>
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth" href="<?= ROOT_PATH ?>admin/addproduct"><i class="fa fa-plus"></i></a></td>
														</tr>
                            <?php foreach ($products as $product) { ?>
                              <tr data-productID="<?= $product["id"] ?>">
																<td scope="row" data-label="ID"><?= $product["sequence"] ?></td>
																<td data-label="Name"><?= $product["product_name"] ?></td>
																<td data-label="Maker"><?= json_decode($product["maker"], true)[$language] ?></td>
																<td data-label="Category"><?= json_decode($product["cat"], true)[$language] ?></td>
																<td data-label="Type"><?= json_decode($product["type"], true)[$language] ?></td>
																<td data-label="Price"><?= $product["base_price"] ?> $</td> <!-- TODO: Base this on user selected currency (CAD/USD) -->
                                <td data-label="Actions">
																	<a class="button is-warning is-small editProduct" href="<?= ROOT_PATH ?>admin/editproduct?id=<?= $product["id"] ?>">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-danger is-small deleteProduct" onclick="deleteProduct($(this));" data-productID="<?= $product["id"] ?>" data-productName="<?= $product["product_name"] ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			function deleteProduct(element) {
				let productID = element.attr('data-productID');
				let productName = element.attr('data-productName');
				swal({
					title: "<?= $lang["Admin"]["ProductsList"]["DeleteProduct"]["Header"] ?>",
					text: "<?= $lang["Admin"]["Product"] ?>: " + productName,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'userID': "<?= $user["id"] ?>",
							"userPassword": "<?= $user["password"] ?>",
							'productID': productID,
							'data': {
								'type': 'product_delete',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeproduct",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-productID="' + productID + '"]';
							$(tr).remove();
						}
					}
				});
			}
    </script>
  </body>
</html>