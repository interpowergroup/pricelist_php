<?php
  include('../shared/config.php');
 

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $activeURL = 'roles';
      
      $reqPerms = $bdd->query("SELECT * FROM phprbac_permissions");
      $permissions = $reqPerms->fetchAll();

    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang['Admin']["RolesList"]["AddRole"]["Header"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/switch.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <span><?php if(isset($error)) { echo $error["msg"]; } ?></span>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang['Admin']["RolesList"]["AddRole"]["Header"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
												
												<div id="productProperties">
													<div class="field">
														<div class="field-label is-normal">
															<label class="label"><?= $lang["Admin"]["RolesList"]["AddRole"]["Title"] ?></label>
														</div>
														<div class="field-body is-horizontal">
															<div class="field">
																<p class="control is-expanded has-icons-left">
																	<input class="input roleTitle" type="text" autocomplete="off" name="productname" placeholder="<?= $lang["Admin"]["RolesList"]["AddRole"]["Title"] ?>">
																	<span class="icon is-small is-left">
																		<i class="fas fa-box"></i>
																	</span>
																</p>
															</div>
														</div>
													</div>
													<div class="field">
														<div class="field-label is-normal">
															<label class="label"><?= $lang["Admin"]["RolesList"]["AddRole"]["Description"] ?></label>
														</div>
														<div class="field-body is-horizontal">
															<div class="field">
																<p class="control is-expanded">
																	<textarea name="productdesc" id="productdesc" class="textarea roleDesc" placeholder="<?= $lang["Admin"]["RolesList"]["AddRole"]["Description"] ?>"></textarea>
																</p>
															</div>
														</div>
													</div>
													<div class="checkboxes-and-radios">
													<?php 
														foreach ($permissions as $perm) { 
															//Remove the root perm and the branches perms from the list
															if($perm["Title"] != 'root' && strpos($perm["Title"], 'branch_') !== 0) {
													?>
														<div class="field">
															<input type="checkbox" name="perm_auth" autocomplete="off" id="perm_auth_<?= $perm["ID"] ?>" value="<?= $perm["ID"] ?>">
															<label for="perm_auth_<?= $perm["ID"] ?>"><?= $perm["Title"] ?></label>
															<p class="help" style="margin-top: 0;"><?= $perm["Description"] ?></p>
														</div>
													<?php
															}
														}
													?>
													</div>
													<div class="level is-mobile">
														<div class="level-item"><a class="button is-light is-medium is-fullwidth" href="<?= ROOT_PATH ?>admin/roles"><?= $lang["Return"] ?></a></div>
														<div class="level-item"><button class="button is-link is-medium is-fullwidth addRole"><?= $lang["Add"] ?></button></div>
													</div>
												</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
      var language = "<?= $language ?>";

			$('.addRole').click(function() {
				let data = { 
					'adminID': '<?= $user["id"] ?>',
					'adminPassword': '<?= $user["password"] ?>',
					'roleID': 1,
					'data': {
						'type': 'role_add',
						'Title': $('.roleTitle').val(),
						'Desc': $('.roleDesc').val(),
						'perms': []
					}
				}
				$('input[name="perm_auth"]').each(function() {
					if($(this).is(':checked')) {
						data.data.perms.push($(this).val());
					}
				});
				$.ajax({
					type: "POST",
					url: "<?= ROOT_PATH ?>admin/ajax/changerole",
					data: data,
					dataType: "json",
					success: function (response) {
						swal({
							title: `${response.text}`,
							type: `${response.type}`,
						}).then(function(){
							if(response.type == 'success') {
								location.reload();
							}
						});
					}
				}).fail(function( jqXHR, textStatus ) {
					alert(`Request failed: ${textStatus}`)
				});
			});
    </script>
  </body>
</html>