<?php
  include('../shared/config.php');
  $activeURL = "employee";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {

      $reqEmployeeCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM users");
      $employeeCount = $reqEmployeeCount->fetch();
      $reqBranchesCount = $bdd->query("SELECT COUNT(*) AS 'COUNT' FROM branches");
      $branchesCount = $reqBranchesCount->fetch();
    } else {
      header('Location: '. ROOT_PATH .'/logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'/logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Profile"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <form method="POST" name="setting_form">
                        <div class="field">
                          <div class="field-label is-normal">
                            <label class="label"><?= $lang["Profile"]["Name"]["Header"] ?></label>
                          </div>
                          <div class="field-body is-horizontal">
                            <div class="field">
                              <p class="control is-expanded has-icons-left has-icons-right">
                                <input class="input" type="text" autocomplete="off" name="firstname" minlenght="2" maxlenght="100" pattern=".{2,100}" header="<?= $lang["Profile"]["Name"]["Firstname"] ?>" placeholder="<?= $lang["Profile"]["Name"]["Firstname"] ?>" oldValue="<?= $user['firstname'] ?>" value="<?= $user['firstname'] ?>">
                                <span class="icon is-small is-left">
                                  <i class="fas fa-user"></i>
                                </span>
                                <span class="icon is-small is-right">
                                  <i class="fas fa-check"></i>
                                </span>
                              </p>
                              <p class="help is-danger"></p>
                            </div>
                            <div class="field">
                              <p class="control is-expanded has-icons-left has-icons-right">
                                <input class="input" type="text" autocomplete="off" name="lastname" minlenght="2" maxlenght="100" pattern=".{2,100}" header="<?= $lang["Profile"]["Name"]["Lastname"] ?>" placeholder="<?= $lang["Profile"]["Name"]["Lastname"] ?>" oldValue="<?= $user['lastname'] ?>" value="<?= $user['lastname'] ?>">
                                <span class="icon is-small is-left">
                                  <i class="fas fa-user"></i>
                                </span>
                                <span class="icon is-small is-right">
                                  <i class="fas fa-check"></i>
                                </span>
                              </p>
                              <p class="help is-danger"></p>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <div class="field-label is-normal">
                            <label class="label"><?= $lang["Profile"]["Username"]["Header"] ?></label>
                          </div>
                          <div class="field-body is-horizontal">
                            <div class="field">
                              <p class="control is-expanded has-icons-left has-icons-right">
                                <input class="input" type="text" autocomplete="off" name="username" minlenght="2" maxlenght="50" pattern=".{2,50}" header="<?= $lang["Profile"]["Username"]["Header"] ?>" placeholder="<?= $lang["Profile"]["Username"]["Placeholder"] ?>" oldValue="<?= $user['username'] ?>" value="<?= $user['username'] ?>" readonly>
                                <span class="icon is-small is-left">
                                  <i class="fas fa-user"></i>
                                </span>
                                <span class="icon is-small is-right">
                                  <i class="fas fa-check"></i>
                                </span>
                              </p>
                              <p class="help"><?= $lang["Profile"]["Username"]["Help"] ?></p>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <div class="field-label is-normal">
                            <label class="label"><?= $lang["Profile"]["Mail"]["Header"] ?></label>
                          </div>
                          <div class="field-body is-horizontal">
                            <div class="field">
                              <p class="control is-expanded has-icons-left has-icons-right">
                                <input class="input" type="text" autocomplete="off" name="mail" minlenght="2" maxlenght="100" pattern="^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$" header="<?= $lang["Profile"]["Mail"]["Header"] ?>" placeholder="<?= $lang["Profile"]["Mail"]["Placeholder"] ?>" oldValue="<?= $user['email'] ?>" value="<?= $user['email'] ?>">
                                <span class="icon is-small is-left">
                                  <i class="fas fa-envelope"></i>
                                </span>
                                <span class="icon is-small is-right">
                                  <i class="fas fa-check"></i>
                                </span>
                              </p>
                              <p class="help is-danger"></p>
                            </div>
                          </div>
                        </div>
                        <div class="field">
                          <div class="field-label is-normal">
                            <label class="label"><?= $lang["Profile"]["Password"]["Header"] ?></label>
                          </div>
                          <div class="field-body is-horizontal">
                            <div class="field">
                              <p class="control is-expanded has-icons-left has-icons-right">
                                <input class="input" type="text" autocomplete="off" name="password" minlenght="8" maxlenght="40" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$" header="<?= $lang["Profile"]["Mail"]["Header"] ?>" placeholder="<?= $lang["Profile"]["Password"]["Placeholder"] ?>">
                                <span class="icon is-small is-left">
                                  <i class="fas fa-lock"></i>
                                </span>
                                <span class="icon is-small is-right">
                                  <i class="fas fa-check"></i>
                                </span>
                              </p>
                              <p class="help is-danger"></p>
                            </div>
                          </div>
                        </div>
                        <div class="field is-grouped is-grouped-centered">
                          <p class="control update_profile">
                            <input type="submit" autocomplete="off" class="button is-info update_profile" name="update_profile" value="<?= $lang['Update'] ?>" disabled>
                          </p>
                          <p class="control cancel_profile">
                            <a href="<?= ROOT_PATH ?>admin/employees" class="button is-light cancel_profile">
                              <?= $lang["Return"] ?>
                            </a>
                          </p>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
  </body>
</html>