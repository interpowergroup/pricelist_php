<?php
  $reqbranches = $bdd->query("SELECT * FROM branches");
  $branches = $reqbranches->fetchAll();
?>
<div class="column is-3">
  <aside class="menu">
    <p class="menu-label">
    <?= $lang["Admin"]["SideMenu"]["General"]["Header"] ?>
    </p>
    <ul class="menu-list">
      <li>
        <a href="<?= ROOT_PATH ?>admin" <?php if($activeURL == "dashboard") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["General"]["Dashboard"] ?></a>
      </li>
    </ul>
    <p class="menu-label">
    <?= $lang["Admin"]["SideMenu"]["Administration"]["Header"] ?>
    </p>
    <ul class="menu-list">
      <li>
        <a href="<?= ROOT_PATH ?>admin/usersettings" <?php if($activeURL == "usersettings") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Administration"]["Settings"] ?></a>
      </li>
      <li>
        <a><?= $lang["Admin"]["SideMenu"]["Administration"]["Management"]["Header"] ?></a>
        <ul>
          <li>
            <a href="<?= ROOT_PATH ?>admin/roles" <?php if($activeURL == "roles") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Administration"]["Management"]["Roles"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/employees" <?php if($activeURL == "employee") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Administration"]["Management"]["Employees"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/branches" <?php if($activeURL == "branches") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Administration"]["Management"]["Branches"] ?></a>
          </li>
        </ul>
      </li>
    </ul>
    <p class="menu-label">
    <?= $lang["Admin"]["SideMenu"]["Products"]["Header"] ?>
    </p>
    <ul class="menu-list">
      <li>
        <a><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Header"] ?></a>
        <ul>
          <li>
            <a href="<?= ROOT_PATH ?>admin/parameters?type=makers" <?php if($activeURL == "makers") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Makers"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/specsets" <?php if($activeURL == "specs") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Specs"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/parameters?type=categories" <?php if($activeURL == "categories") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Categories"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/parameters?type=types" <?php if($activeURL == "types") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Types"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/optionssets" <?php if($activeURL == "options") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Options"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/parameters?type=series" <?php if($activeURL == "series") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Series"] ?></a>
          </li>
          <li>
            <a href="<?= ROOT_PATH ?>admin/parameters?type=subseries" <?php if($activeURL == "subseries") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Settings"]["Subseries"] ?></a>
          </li>
        </ul>
      </li>
      <li>
        <a><?= $lang["Admin"]["SideMenu"]["Administration"]["Management"]["Header"] ?></a>
        <ul>
          <li>
            <a href="<?= ROOT_PATH ?>admin/productslist" <?php if($activeURL == "productslist") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["List"] ?></a>
          </li>
          <!-- <hr class="navbar-divider" style="background-color:#dbdbdb;height:1px;"> -->
          <li>
            <a href="<?= ROOT_PATH ?>admin/addproduct" <?php if($activeURL == "addproduct") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["Add"] ?></a>
          </li>
          <!-- <li>
            <a href="<?= ROOT_PATH ?>admin/massproductupload" <?php if($activeURL == "massupload") { echo 'class="is-active"'; } ?>><?= $lang["Admin"]["SideMenu"]["Products"]["MassUpload"] ?></a>
          </li> -->
        </ul>
      </li>
    </ul>
  </aside>
</div>