<?php
  include('../shared/config.php');
  $activeURL = "roles";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {

      $reqRoles = $bdd->query("SELECT * FROM `phprbac_roles`");
			$roles = $reqRoles->fetchAll();
			
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["RolesList"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["RolesList"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["RolesList"]["Table"]["ID"] ?></th>
                              <th data-label="Title"><?= $lang["Admin"]["RolesList"]["Table"]["Title"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["RolesList"]["Table"]["Actions"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-roleID="<?= $role["id"] ?>">
															<td scope="row" data-label="ID"></td>
															<td data-label="Title"></td> 
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth addRole" href="<?= ROOT_PATH ?>admin/addrole"><i class="fa fa-plus"></i></a></td>
														</tr>
														<?php 
															foreach ($roles as $role) { 
																if($role["Title"] != 'root' && strpos($role["Title"], 'branch_') !== 0) {
														?>
                              <tr data-roleID="<?= $role["ID"] ?>">
                                <td scope="row" data-label="ID"><?= $role["ID"] ?></td>
                                <td data-label="Title"><?= $role["Title"] ?></td> 
                                <td data-label="Actions">
																	<a class="button is-warning is-small editRole" href="<?= ROOT_PATH ?>admin/editrole?id=<?= $role["ID"] ?>">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-danger is-small deleteRole" onclick="deleteRole($(this));" data-roleID="<?= $role["ID"] ?>" data-roleTitle="<?= $role["Title"] ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
														<?php 
																} 
															}
														?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			function deleteRole(element) {
				let roleID = element.attr('data-roleID');
				let roleTitle = element.attr('data-roleTitle');
				swal({
					title: "<?= $lang["Admin"]["RolesList"]["DeleteRole"]["Header"] ?>",
					text: "<?= $lang["Admin"]["Role"] ?>: " + roleTitle,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'roleID': roleID,
							'data': {
								'type': 'role_delete',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changerole",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						});
						if(result.value.type == 'success') {
							let tr = 'tr[data-roleID="' + roleID + '"]';
							$(tr).remove();
						}
					}
				});
			}   
    </script>
  </body>
</html>