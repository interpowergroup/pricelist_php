<?php
  include('../shared/config.php');


  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $activeURL = 'massupload';

      $reqmakers = $bdd->query("SELECT * FROM products_makers");
      $makers = $reqmakers->fetchAll();
      $reqcategories = $bdd->query("SELECT * FROM products_categories");
      $categories = $reqcategories->fetchAll();
      $reqtypes = $bdd->query("SELECT * FROM products_types");
      $types = $reqtypes->fetchAll();
      $reqseries = $bdd->query("SELECT * FROM products_series");
      $series = $reqseries->fetchAll();
      $reqsubseries = $bdd->query("SELECT * FROM products_subseries");
      $subseries = $reqsubseries->fetchAll();
      $reqbranches = $bdd->query("SELECT * FROM branches");
      $branches = $reqbranches->fetchAll();
      $reqoptionsset = $bdd->query("SELECT * FROM products_optionset");
      $optionsset = $reqoptionsset->fetchAll();

    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["MassUpload"]["JSONGenerator"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap-multiselect.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["MassUpload"]["JSONGenerator"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">

												<div id="productProperties">
                          <form method="post" v-on:submit.prevent="onSubmit" enctype="multipart/form-data">
                          <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Makers"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="maker in makers" :key="'makers_' + maker.id" v-model="selected['makers']" :parameter="maker" param-type="makers" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Categories"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="category in filtered('categories', 'makers')" :key="'categories_' + category.id" v-model="selected['categories']" :parameter="category" param-type="categories" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Types"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="type in filtered('types', 'categories')" :key="'types_' + type.id" v-model="selected['types']" :parameter="type" param-type="types" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Series"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="serie in filtered('series', 'types', 'makers')" :key="'series_' + serie.id" v-model="selected['series']" :parameter="serie" param-type="series" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang['Admin']["SideMenu"]["Products"]["Settings"]["Subseries"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="subserie in filtered('subseries', 'series')" :key="'subseries_' + subserie.id" v-model="selected['subseries']" :parameter="subserie" param-type="subseries" input-type="radio"></parameters-list>
                            </div>
                            <div class="field-label is-normal">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["SideMenu"]["Products"]["Branches"]["Header"] ?></label>
                              </div>
                            </div>
                            <div class="toggle">
                              <parameters-list v-for="branch in branches" :key="'branches_' + branch.id" v-model="selected['branches'][branch.id]" :parameter="branch" param-type="branches" input-type="checkbox"></parameters-list>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Name"] ?></label>
                              </div>
                              <div class="field-body is-horizontal">
                                <div class="field">
                                  <p class="control is-expanded has-icons-left">
                                    <input class="input" type="text" autocomplete="off" name="productname" v-model="productname" placeholder="<?= $lang["Admin"]["AddProduct"]["Name"] ?>">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-box"></i>
                                    </span>
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Description"] ?></label>
                              </div>
                              <div class="field-body is-horizontal">
                                <div class="field">
                                  <?php foreach ($availableLanguages as $key => $value) { ?>
                                    <div class="field-label is-small">
                                      <label class="label"><?= $lang[$value] ?></label>
                                    </div>
                                    <p class="control is-expanded">
                                      <textarea name="productdesc" id="productdesc" v-model="productdesc['<?= $value ?>']" class="textarea" placeholder="<?= $lang["Admin"]["AddProduct"]["Description"] ?>"></textarea>
                                    </p>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Price"] ?></label>
                              </div>
															<div class="field-body is-horizontal">
																<div class="field">
																	<p class="control is-expanded has-icons-left">
																		<input class="input currentStock" autocomplete="off" type="number" step="any" v-model="productprice" placeholder="<?= $lang["Admin"]["AddProduct"]["Price"] ?>">
																		<span class="icon is-small is-left">
																			<i class="fas fa-dollar-sign"></i>
																		</span>
                                    <p class="help"><?= $lang["Admin"]["AddProduct"]["PriceDisclamer"] ?> (1 USD = <?= $userconfig["exchangerate"] ?> CAD)</p>
																	</p>
																</div>
															</div>
														</div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Specs"]["Header"] ?> <a style="margin-left: 15px;" @click="additem('specs')"><i class="fa fa-plus"></i></a></label>
                              </div>
															<?php foreach ($availableLanguages as $key => $value) { ?>
                                <div class="field-label is-small">
                                  <label class="label"><?= $lang[$value] ?></label>
                                </div>
                                <div class="specs spec_<?= $value ?>" data-lang="<?= $value ?>">
                                  <product-spec v-for="(spec, index) in filtereditem('specs', 'categories', '<?= $value ?>')" :key="index" :index="index" :item="spec" lang="<?= $value ?>" v-on:remove="!spec.readonly ? addedspecs.splice(index - defaultspecs.length, 1) : log(index);" item-title="<?= $lang["Admin"]["AddProduct"]["Specs"]["Title"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Specs"]["Argument"] ?>"></product-spec>
                                </div>
                              <?php } ?>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Opts"]["Header"] ?></label>
                                <p class="help"><?= $lang["Admin"]["AddProduct"]["PriceDisclamer"] ?> (1 USD = <?= $userconfig["exchangerate"] ?> CAD)</p>
                              </div>
															<div class="opts">
																<product-options v-for="(opt, index) in filteredOptions" :key="index" :index="index" :item="opt" lang="<?= $language ?>" v-on:remove="!opt.readonly ? addedopts.splice(index - defaultopts.length, 1) : log(defaultopts.length);" item-title="<?= $lang["Admin"]["AddProduct"]["Opts"]["Title"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Opts"]["Argument"] ?>"></product-opt>
															</div>
                            </div>
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Documents"]["Header"] ?> <a style="margin-left: 15px;" @click="additem('docs')"><i class="fa fa-plus"></i></a></label>
                              </div>
															<div class="docs">
															<?php foreach ($availableLanguages as $key => $value) { ?>
                                <div class="field-label is-small">
                                  <label class="label"><?= $lang[$value] ?></label>
                                </div>
                                <div class="docs doc_<?= $value ?>" data-lang="<?= $value ?>">
                                  <product-doc v-for="(doc, index) in addeddocs" :key="index" :index="index" :item="doc" lang="<?= $value ?>" v-on:remove="addeddocs.splice(index, 1)" item-title="<?= $lang["Admin"]["AddProduct"]["Documents"]["Name"] ?>" item-argument="<?= $lang["Admin"]["AddProduct"]["Documents"]["Upload"] ?>"></product-doc>
                                </div>
                              <?php } ?>
															</div>
                            </div>
                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label" style="font-size: 1rem !important;"><?= $lang["Admin"]["AddProduct"]["Image"]["Header"] ?></label>
                              </div>
                              <div class="image">
                                <div class="field">
                                  <p class="control is-expanded has-icons-left">
                                    <input class="input itemFile" autocomplete="off" type="file" @change="convertToB64($event)">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-file-image"></i>
                                    </span>
                                  </p>
                                </div>
                                <img :src="productimage" style="width:30%;margin: auto;" />
                              </div>
                            </div>
                            <button class="button is-link is-medium is-fullwidth"><?= $lang["Admin"]["MassUpload"]["GetTheJSON"] ?></button>
                          </form>
                          <div class="jsonTable">
                            <table class="table" v-show="showJSON">
                              <thead>
                                <th>product_name</th>
                                <th>maker_id</th>
                                <th>category_id</th>
                                <th>type_id</th>
                                <th>serie_id</th>
                                <th>subserie_id</th>
                                <th>listprice</th>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>{{ jsonTable.productname }}</td>
                                  <td>{{ jsonTable.maker_id }}</td>
                                  <td>{{ jsonTable.category_id }}</td>
                                  <td>{{ jsonTable.type_id }}</td>
                                  <td>{{ jsonTable.serie_id }}</td>
                                  <td>{{ jsonTable.subserie_id }}</td>
                                  <td>{{ jsonTable.productprice }}</td>
                                </tr>
                              </tbody>
                            </table>
                            <table class="table" v-show="showJSON">
                              <thead>
                                <th>desc</th>
                                <th>branches</th>
                                <th>specs</th>
                                <th>options</th>
                                <th>docs</th>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>{{ jsonTable.productdesc }}</td>
                                  <td>{{ jsonTable.branches }}</td>
                                  <td>{{ jsonTable.specs }}</td>
                                  <td>{{ jsonTable.opts }}</td>
                                  <td>{{ jsonTable.docs }}</td>
                                </tr>
                              </tbody>
                            </table>
                            
                          </div>
												</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
      var language = "<?= $language ?>";
      let vmData = {
        jsonTable: {},
        makers: <?= json_encode($makers) ?>,
        categories: <?= json_encode($categories) ?>,
        types: <?= json_encode($types) ?>,
        series: <?= json_encode($series) ?>,
        subseries: <?= json_encode($subseries) ?>,
        branches: <?= json_encode($branches) ?>,
        optionsset: <?= json_encode($optionsset) ?>,
        showJSON: false,
        productname: '',
        productdesc: {
          <?php foreach ($availableLanguages as $key => $value) {
            echo '"'.$value.'": "",';
          } ?>
        },
        productprice: '',
        productimage: '',
        defaultspecs: [],
        addedspecs: [],
				defaultopts: [],
        addedopts: [],
        addeddocs: [],
        selected: {
          makers: '',
          categories: '',
          types: '',
          series: '',
          subseries: '',
          branches: {}
        },
      }

			Vue.component('parameters-list', {
        props: ['parameter', 'paramType', 'inputType', 'value'],
        data: function () {
          return {
            selectedParam: false,
          }
        },
        methods: {
          select(){
						let _this = this;
						let i = 0;
						let index;
						$.each(vmData.selected, function(k, v) {
							if(k == _this.paramType) {
								return index = i;
							}
							i++;
						});
						i = 0;

						$.each(vmData.selected, function(k, v) {
							if(i > index) {
								if(k != 'branches') {
									vmData.selected[k] = '';
									$('input[name="'+ k +'"]').prop('checked', false);
								}
							}
							i++;
						});

						if(_this.paramType == "categories" || _this.paramType == "series") {
							let myFilter;
							vmData[_this.paramType].forEach(element => {
								if(element.id == vmData.selected[_this.paramType]) {
									return myFilter = element;
								}
							});
							if(_this.paramType == "categories") {
								vmData['defaultspecs'] = JSON.parse(myFilter.specs);
							} else if(_this.paramType == "series") {
								vmData['defaultopts'] = vmData.optionsset.filter((option) => {
                  return myFilter.specs.includes(option.id);
                });
								vmData.defaultopts.forEach(element => {
									element["name"] = JSON.parse(element["name"]);
									element["options"] = JSON.parse(element["options"]);
									element["options"].forEach(element2 => {
										element2.arg = '';
									});
								});
							}
						}

						if(this.paramType == 'series') {
							vmData.productname = $('.' + this.paramType + '_' + $('input[name="'+ this.paramType +'"]:checked').val()).text();
						} else if(this.paramType == 'subseries') {
							if(vmData.productname.indexOf('-') != -1) {
								vmData.productname = $('input[name="productname"]').val().substring(0, vmData.productname.indexOf('-'));
							}
							if($('input[name="'+ this.paramType +'"]:checked').val() != 1) {
								vmData.productname += '-' + $('.' + this.paramType + '_' + $('input[name="'+ this.paramType +'"]:checked').val()).text();
							}
						} else if(this.paramType != 'branches') {
							vmData.productname = '';
						}
          }
        },
        template: `
          <div class="toggle_child">
            <input v-if="inputType == 'radio'" :type="inputType" autocomplete="off" :name="paramType" @change="$emit('input', $event.target.value); select();" :value="parameter.id" :id="paramType + '_' + parameter.id" />
            <input v-if="inputType == 'checkbox'" :type="inputType" autocomplete="off" :name="paramType" @change="$emit('input', $event.target.checked); select();" :value="parameter.id" :id="paramType + '_' + parameter.id" />
            <label :for="paramType + '_' + parameter.id" :class="paramType + '_' + parameter.id">{{ paramType != 'branches' ? JSON.parse(parameter.name)['${language}'] : parameter.name }}</label>
          </div>
        `
      });

      Vue.component('product-spec', {
        template: `
          <div class="field-body is-horizontal">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemTitle" type="text" autocomplete="off" :readonly="item.readonly" v-model="item['title'][lang]" :placeholder="itemTitle">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemArg" type="text" autocomplete="off" v-model="item['arg']['can']" :placeholder="itemArgument + ' (CANADA)'">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
						<div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemArg" type="text" autocomplete="off" v-model="item['arg']['us']" :placeholder="itemArgument + ' (US)'">
                <span class="icon is-small is-left">
                  <i class="fas fa-cog"></i>
                </span>
              </p>
            </div>
            <a class="fa fa-trash-alt deleteSpec" :style="{ visibility: item.visibility }" @click="$emit('remove')" style="font-size:2rem;"></a>
          </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			Vue.component('product-options', {
        template: `
          <div>
            <label>{{ item['name'][lang] }}</label>
            <div class="field-body is-horizontal" v-for="opt in item['options']">
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input class="input itemTitle" type="text" autocomplete="off" readonly="true" v-model="opt['title'][lang]" :placeholder="itemTitle">
                  <span class="icon is-small is-left">
                    <i class="fas fa-tag"></i>
                  </span>
                </p>
              </div>
              <div class="field">
                <p class="control is-expanded has-icons-left">
                  <input class="input itemArg" type="text" autocomplete="off" v-model="opt['arg']" :placeholder="itemArgument">
                  <span class="icon is-small is-left">
                    <i class="fas fa-dollar-sign"></i>
                  </span>
                </p>
              </div>
            </div>
          </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			Vue.component('product-doc', {
        template: `
          <div class="field-body is-horizontal">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemTitle" type="text" autocomplete="off" v-model="item['name'][lang]" :placeholder="itemTitle">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <div class="field">
							<p class="control is-expanded has-icons-left">
                <input class="input itemFile" type="text" autocomplete="off" v-model="item['file']" :placeholder="itemArgument">
                <span class="icon is-small is-left">
                  <i class="fas fa-file-alt"></i>
                </span>
              </p>
            </div>
            <a class="fa fa-trash-alt deleteSpec" @click="$emit('remove')" style="font-size:2rem;"></a>
          </div>
        `,
        props: ['item', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			let arrayTypes = {
				specs: {
					title: {
						<?php foreach ($availableLanguages as $key => $value) {
              echo '"'.$value.'": "",';
            } ?>
					},
					arg: {
						can: '', 
						us: '',
					}
				},
				docs: {
					name: {
            <?php foreach ($availableLanguages as $key => $value) {
              echo '"'.$value.'": "",';
            } ?>
					},
					file: ''
				}
			}

			var vm = new Vue({
				el: '#productProperties',
				data: vmData,
        computed: {
          filteredOptions() {
            return this.defaultopts;
          }
        },
				methods: {
          log: function(id) {
            console.log(id);
          },
          additem: function (type) {
            this['added'+type].push(JSON.parse(JSON.stringify(arrayTypes[type])));
          },
          filtereditem: function(type, filter) {
            var _this = this;
						let items = [];
            let specField = JSON.parse(JSON.stringify(arrayTypes[type]));
            if(_this.selected[filter] == '') {
              return _this['added'+type];
            }

            let myFilter;
            _this[filter].forEach(element => {
              if(element.id == _this.selected[filter]) {
                return myFilter = element;
              }
            });

            _this['default'+type].forEach(element => {
							element.readonly = true;
							element.visibility = 'hidden';
							items.push(element);
            });
            _this['added'+type].forEach(element => {
              element.readonly = false;
              element.visibility = '';
              items.push(element);
            });
            return items;
          },
					filtered: function(paramType, filter, subfilter = '') {
            var _this = this;
            if(_this.selected[filter] == '') return null;

            let myFilter = null;
            let mySubFilter = null;
            /**
             * @myFilter rep= thresent the filter object on it own
             * @filter helps get the myFilter by specifing what object we are looping throught
             * The loop check if somewhere in the array of objects pointed by the @filter is the one selected by the parameter (@select)
             */
            this[filter].forEach(element => {
              if(element.id == _this.selected[filter]) {
                return myFilter = element;
              }
            });
            if(subfilter != '') {
              this[subfilter].forEach(element => {
              if(element.id == _this.selected[subfilter]) {
                return mySubFilter = element;
              }
            });
            }
            //This returns an array of objects of base on the filter parameter
            if(mySubFilter == null) {
              return this[paramType].filter(function (item) {
                if(item.availCategories.includes(myFilter.id)) return item
              });
            } else {
              let firstPass = this[paramType].filter(function (item) {
                if(item.availCategories.includes(myFilter.id)) return item
              });
              return firstPass.filter(function (item) {
                if(item.makers.includes(mySubFilter.id)) return item
              });
            }
          },
          onSubmit: function() {
            let product = {
              productname: this.productname,
              productprice: this.productprice,
              productdesc: this.productdesc,
              maker_id: this.selected.makers,
              category_id: this.selected.categories,
              type_id: this.selected.types,
              serie_id: this.selected.series,
              subserie_id: this.selected.subseries,
              branches: this.selected.branches,
              specs: [],
              opts: [],
              docs: [],
            }

            product.specs = this.defaultspecs.map(function(item) {
              let newItem = {
                title: item.title, 
                arg: item.arg, 
              }
              return newItem;
            });

            this.addeddocs.forEach(element => {
              product.docs.push(element);
            });
            this.defaultopts.forEach(element => {
              let args = [];
              element["options"].forEach(option => {
                args.push(option["arg"]);
              });

              product.opts.push({"id": element["id"], "args": args});
            });
            
            this.showJSON = true;
            this.jsonTable = product;
          },
          convertToB64(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;

            var reader = new FileReader();
            reader.onload = (e) => {
              vmData.productimage = e.target.result;
            };
            reader.readAsDataURL(files[0]);
          }
				}
			});
    </script>
  </body>
</html>