<?php
  include('../shared/config.php');
  $activeURL = "specs";

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
			
			$reqsets = $bdd->query("SELECT * FROM products_specset");
			$sets = $reqsets->fetchAll();

    } else {
      header('Location: '. ROOT_PATH .'/logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'/logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"]["OptionsSet"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"]["SpecsSet"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
                      <div class="table_wrapper">
                        
                        <table class="table is-hoverable is-striped is-fullwidth">
                          <thead>
                            <tr>
                              <th data-label="ID"><?= $lang["Admin"]["SpecsSet"]["Table"]["ID"] ?></th>
                              <th data-label="Name"><?= $lang["Admin"]["SpecsSet"]["Table"]["Name"] ?></th>
                              <th data-label="SpecsCount"><?= $lang["Admin"]["SpecsSet"]["Table"]["SpecsCount"] ?></th>
                              <th data-label="Actions"><?= $lang["Admin"]["SpecsSet"]["Table"]["Actions"] ?></th>
                            </tr>
                          </thead>
                          <tbody class="employeeList">
														<tr data-setID="0">
															<td scope="row" data-label="ID"></td>
															<td data-label="Name"></td> 
															<td data-label="OptionsCount"></td>
															<td data-label="Actions"><a class="button is-success is-small is-fullwidth addSet" href="editspec?add"><i class="fa fa-plus"></i></a></td>
                            </tr>
                            <?php foreach ($sets as $a_set) { ?>
                              <tr data-setID="<?= $a_set["id"] ?>">
                                <td scope="row" data-label="ID"><?= $a_set["id"] ?></td>
                                <td data-label="Name"><?= json_decode($a_set["name"], true)[$language] ?></td> 
                                <td data-label="OptionsCount"><?= count(json_decode($a_set["specs"]), true) ?></td>
                                <td data-label="Actions">
																	<a class="button is-warning is-small editSet" href="editspec?edit=<?= $a_set["id"] ?>">
																		<i class="fa fa-pen"></i>
																	</a> 
																	<a class="button is-danger is-small deleteSet" onclick="deleteSet($(this));" data-setID="<?= $a_set["id"] ?>" data-setName="<?= json_decode($a_set["name"], true)[$language] ?>">
																		<i class="fa fa-trash-alt"></i>
																	</a>
																</td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
			function deleteSet(element) {
				let setID = element.attr('data-setID');
				let setName = element.attr('data-setName');
				swal({
					title: "<?= $lang["Admin"]["SpecsSet"]["DeleteSet"]["Header"] ?>",
					text: "<?= $lang["Admin"]["SpcsSet"] ?>: " + setName,
					type: "warning",
					confirmButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
					showCancelButton: true,
					showLoaderOnConfirm: true,
					preConfirm: (value) => {
						let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'setID': setID,
							'data': {
								'type': 'set_delete',
								'delete': value
							}
						}
						return $.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changeset",
							data: data,
							dataType: "json",
							success: function (response) {
								return response;
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then((result) => {
					if (result.value) {
						swal({
							title: `${result.value.text}`,
							type: `${result.value.type}`,
						}).then(function() {
							location.reload();
						});
					}
				});
			}
    </script>
  </body>
</html>