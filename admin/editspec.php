<?php
  include('../shared/config.php');
 

  if(isset($_SESSION["USER"])) {
    $user = $_SESSION["USER"];
    if($rbac->check('view_admin_panel', $user['id'])) {
      $activeURL = 'specs';
      
      if((isset($_GET["add"])) || (isset($_GET["edit"]) && !empty($_GET["edit"]) && is_numeric($_GET["edit"]))) {

				$objectAvailLangEmpty = [];
				foreach ($availableLanguages as $key => $value) {
					$objectAvailLangEmpty[$value] = "";
				}
				
				$editType = 'Unknown';
        if(isset($_GET["add"])) {
					$editType = 'Add';
					$optsets = [ "name" => json_encode($objectAvailLangEmpty),"specs" => json_encode([ ["title" => $objectAvailLangEmpty] ])];
					$setID = 1;
				} else {
					$editType = 'Edit';
					$setID = htmlspecialchars($_GET["edit"]);
					$reqset = $bdd->prepare("SELECT * FROM `products_specset` WHERE id = ?");
					$reqset->execute(array($setID));
					$optsets = $reqset->fetch();
				}

      } else {
        header('Location: '. ROOT_PATH .'admin/specsets');
      }
    } else {
      header('Location: '. ROOT_PATH .'logout?err=NotAutorized');
    }
  } else {
    header('Location: '. ROOT_PATH .'logout?err=NoSession');
  }
?>
<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <title>IPG - <?= $lang["Admin"]["PageTitle"] ?> - <?= $lang["Admin"][$editType."Set"]["PageTitle"] ?></title>
    <?php include_once('../shared/head.php') ?>
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin.css">
		<link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/toggle.css">
		<link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?= ROOT_PATH ?>assets/css/bootstrap-multiselect.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?= ROOT_PATH ?>assets/css/admin-ie8.css">
    <![endif]-->
  </head>
  <body>
    <?php include_once('../shared/navbar.php'); ?>
    <div class="container">
      <div class="columns">
        <?php include_once('sidebar.php'); ?>
        <div class="column is-9">
          <div class="notification <?php if(isset($error)) { echo 'is-'.$error["type"]; } ?> has-text-centered" style="<?php if(!isset($error)) { echo 'display:none;'; } ?>">
            <button class="delete"></button>
            <?php if(isset($error)) { echo $error["msg"]; } ?>
          </div>
          <div class="intro column is-12 has-text-centered" style="margin:auto;">
            <h2 class="title"><?= $lang["Admin"][$editType."Set"]["PageTitle"] ?></h2>
          </div>
          <div class="columns is-centered" style="width:initial;height:fit-content;">
              <div class="column is-12">
                <div class="card">
                  <div class="card-content">
                    <div class="content">
												
												<div id="app">
                          <form method="post" v-on:submit.prevent="onSubmit">
                          
														<div class="field">
                              <div class="field-label is-normal">
                                <label class="label"><?= $lang["Admin"]["EditSet"]["Name"] ?></label>
                              </div>
                              <?php foreach ($availableLanguages as $key => $value) { ?>
                                <div class="field-label is-small">
                                  <label class="label"><?= $lang[$value] ?></label>
                                </div>
                                <div class="setname setname_<?= $value ?>" data-lang="<?= $value ?>">
																	<p class="control is-expanded has-icons-left">
                                    <input class="input" type="text" autocomplete="off" name="setname" v-model="name['<?= $value ?>']" placeholder="<?= $lang["Admin"]["EditSet"]["Name"] ?>">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-tag"></i>
                                    </span>
                                  </p>
                                </div>
                              <?php } ?>
                            </div>

                            <div class="field">
                              <div class="field-label is-normal">
                                <label class="label"><?= $lang["Admin"]["EditSet"]["Specs"]["Header"] ?> <a style="margin-left: 15px;" @click="addOpt"><i class="fa fa-plus"></i></a></label>
                              </div>
                              <?php foreach ($availableLanguages as $key => $value) { ?>
                                <div class="field-label is-small">
                                  <label class="label"><?= $lang[$value] ?></label>
                                </div>
                                <div class="opts opt_<?= $value ?>" data-lang="<?= $value ?>">
                                  <option-set v-for="(opt, index) in specs" :key="index" :index="index" :item="opt" lang="<?= $value ?>" v-on:remove="specs.splice(index, 1)" item-title="<?= $lang["Admin"]["EditSet"]["Specs"]["Title"] ?>" item-argument="<?= $lang["Admin"]["EditSet"]["Specs"]["Argument"] ?>"></product-opt>
                                </div>
                              <?php } ?>
                            </div>

                            <div class="level is-mobile">
                              <div class="level-item"><a class="button is-light is-fullwidth" href="<?= ROOT_PATH ?>admin/specsets"><?= $lang["Return"] ?></a></div>
                              <div class="level-item"><button class="button is-link is-fullwidth"><?= $lang[$editType] ?></button></div>
                            </div>
                            
                          </form>
												</div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include_once('../shared/scripts.php'); ?>
		<script type="text/javascript">
      var language = "<?= $language ?>";
      let vmData = {
				name: <?= $optsets["name"] ?>,
        specs: <?= $optsets["specs"] ?>,
      }
      
			let arrayTypes = {
				opts: {
					title: {
						<?php foreach ($availableLanguages as $key => $value) {
              echo '"'.$value.'": "",';
            } ?>
					}
				},
      }
      
      Vue.component('option-set', {
        template: `
          <div class="field-body is-horizontal">
            <div class="field">
              <p class="control is-expanded has-icons-left">
                <input class="input itemTitle" type="text" autocomplete="off" v-model="item['title'][lang]" :placeholder="itemTitle">
                <span class="icon is-small is-left">
                  <i class="fas fa-tag"></i>
                </span>
              </p>
            </div>
            <a class="fa fa-trash-alt deleteSpec" @click="$emit('remove')" style="font-size:2rem;"></a>
          </div>
        `,
        props: ['item', 'relation', 'index', 'lang', 'itemTitle', 'itemArgument']
      });

			var vm = new Vue({
				el: '#app',
				data: vmData,
				methods: {
          log: function(id) {
            console.log(id);
          },
          addOpt: function (type) {
            this.specs.push(JSON.parse(JSON.stringify(arrayTypes.opts)));
          },
          onSubmit: function() {
            let optionset = JSON.parse(JSON.stringify(vmData));
            let data = { 
							'adminID': "<?= $user["id"] ?>",
							"adminPassword": "<?= $user["password"] ?>",
							'setID': <?= $setID ?>,
							'data': {
								'type': 'set_<?= strtolower($editType) ?>',
                'specset': optionset
							}
						}
						$.ajax({
							type: "POST",
							url: "<?= ROOT_PATH ?>admin/ajax/changespec",
							data: data,
							dataType: 'json',
							success: function (response) {
								swal({
									title: `${response.text}`,
									type: `${response.type}`,
								}).then(function() {
									if(response.type == 'success') {
										location.replace('<?= ROOT_PATH ?>admin/specsets');
									}
								});
							}
						}).fail(function( jqXHR, textStatus ) {
							alert(`Request failed: ${textStatus}`)
						});
          },
				}
			});
    </script>
  </body>
</html>