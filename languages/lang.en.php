<?php
/*
------------------
Language: Francais
------------------
*/

$lang = array();

//SHARED
$language = 'en';
$lang['fr'] = 'French';
$lang['en'] = 'English';
$lang['Short'] = 'en';
$lang['Long'] = 'English';

$lang['KEY'] = 'VALUE';
$lang['Add'] = 'Add';
$lang['Added'] = 'Added';
$lang['Update'] = 'Update';
$lang['Updated'] = 'Updated';
$lang['Upload'] = 'Upload';
$lang['Uploaded'] = 'Uploaded';
$lang['Cancel'] = 'Cancel';
$lang['Canceled'] = 'Canceled';
$lang['Edit'] = 'Edit';
$lang['Edited'] = 'Edited';
$lang['Delete'] = 'Delete';
$lang['Deleted'] = 'DEleted';
$lang['Return'] = 'Return';
$lang['UndefinedAmountOfTime'] = 'an undefined amount of time';
$lang['Hours'] = 'Hours';
$lang['Minutes'] = 'Minutes';
$lang['Seconds'] = 'Secondes';
$lang['None'] = 'None';
$lang['Invalid'] = 'Invalid';



//NAVBAR
$lang['Navbar']["Branch"] = "Branches";
$lang['Navbar']["Search"] = "Search a product...";
$lang['Navbar']["Categories"]["Header"] = "Categories";
$lang['Navbar']["Categories"]["All"] = "All categories";
$lang['Navbar']["Categories"]["AllProducts"] = "All stock";
$lang['Navbar']["Categories"]["AllPrices"] = "All prices";
$lang['Navbar']["Categories"]["NothingToShow"] = "No product matches your search";
$lang['Navbar']["SearchInventory"] = "Inventory search";
$lang['Navbar']["Account"]["MyAccount"] = "My Account";
$lang['Navbar']["Account"]["Profile"] = "User profile";
$lang['Navbar']["Account"]["Admin"] = "Admin";
$lang['Navbar']["Account"]["Logout"] = "Logout";
$lang['Navbar']["Language"] = "Language";
$lang['Navbar']["SelectLanguage"] = "Select your language";


//LOGIN
$lang['Login']["PageTitle"] = "Login";
$lang['Login']["Username"] = "Username";
$lang['Login']["Password"] = "Password";


//BRANCH SELECTION
$lang['BranchSelection']["PageTitle"] = "Branch selection";
$lang['BranchSelection']["PleaseSelect"] = "Please select one branch";
$lang['BranchSelection']["SeeStock"] = "See inventory";
$lang['BranchSelection']["SeeProduct"] = "See product";
$lang['BranchSelection']["Select"] = "Select";


//POFILE (DO not remove before chedcking if no other pages use theses varibales)
$lang['Profile']["PageTitle"] = "User profile";
$lang['Profile']["Name"]["Header"] = "Name";
$lang['Profile']["Name"]["Firstname"] = "Firstname";
$lang['Profile']["Name"]["Lastname"] = "Lastname";
$lang['Profile']["Username"]["Header"] = "Username";
$lang['Profile']["Username"]["Placeholder"] = "My username";
$lang['Profile']["Username"]["Help"] = "Thsi field is generated automaticaly based on you firstname and lastname";
$lang['Profile']["Mail"]["Header"] = "Email";
$lang['Profile']["Mail"]["Placeholder"] = "My email";
$lang['Profile']["Password"]["Header"] = "Password";
$lang['Profile']["Password"]["Placeholder"] = "My new password";


//PRODUCT VIEWER
$lang['ProductViewer']["BasePrice"] = "Base price";
$lang['ProductViewer']["OptionsPrice"]["Opts"] = "Options";
$lang['ProductViewer']["OptionsPrice"]["Final"] = "Total";
$lang['ProductViewer']["Documents"] = "Documents";
$lang['ProductViewer']["NoDocuments"] = "No document available";
$lang['ProductViewer']["Specs"] = "Spécifications";
$lang['ProductViewer']["NoSpecs"] = "No spécification available";
$lang['ProductViewer']["Options"] = "Options";
$lang['ProductViewer']["InStock"] = "In stock";
$lang['ProductViewer']["OnOrder"] = "On order";
$lang['ProductViewer']["wasRemoved"] = "was removed to";
$lang['ProductViewer']["whereRemoved"] = "where removed to";
$lang['ProductViewer']["wasAdded"] = "was added to";
$lang['ProductViewer']["whereAdded"] = "where added to";
$lang['ProductViewer']["Add"] = "Add";
$lang['ProductViewer']["Remove"] = "Remove";
$lang['ProductViewer']["CannotEnterNegativeValue"] = "You cannot enter negative values";


//PRODUCTS
$lang['Products']["Name"] = "Name";
$lang['Products']["Price"] = "Price";
$lang['Products']["Stock"] = "Stock";
$lang['Products']["Order"] = "Order";
$lang['Products']["Description"] = "Description";
$lang['Products']["SCFM"] = "SCFM";
$lang['Products']["PSI"] = "PSI";
$lang['Products']["HP"] = "HP";
$lang['Products']["Voltage"] = "Voltage";

$lang['Products']["ListPrice"] = "List Price";
$lang['Products']["Discount1"] = "Discount 1";
$lang['Products']["Discount2"] = "Discount 2";
$lang['Products']["BeforeExchange"] = "Cost Before Exchange";
$lang['Products']["Transport"] = "Transport";
$lang['Products']["ExchangeRate"] = "Exchange Rate";
$lang['Products']["AfterExchange"] = "Cost After Exchange";
$lang['Products']["ProfitMultiplier"] = "Profit Multiplier";
$lang['Products']["SellingPrice"] = "Selling Price";


//ADMIN
$lang['Admin']["PageTitle"] = "Administration";
$lang['Admin']["Employees"] = "Employees";
$lang['Admin']["Employee"] = "Employee";
$lang['Admin']["Branches"] = "Branches";
$lang['Admin']["Branch"] = "Branch";
$lang['Admin']["Categories"] = "Categories";
$lang['Admin']["Category"] = "Category";
$lang['Admin']["Products"] = "Products";
$lang['Admin']["Product"] = "Product";
$lang['Admin']["Roles"] = "Roles";
$lang['Admin']["Role"] = "Role";
$lang['Admin']["OptsSets"] = "Options groups";
$lang['Admin']["OptsSet"] = "Options group";
$lang['Admin']["ProductSearch"] = "Product search";
$lang['Admin']["EmployeeSearch"] = "Employee search";

$lang['Admin']["Events"]["Header"] = "Events";
$lang['Admin']["Events"]["ViewAll"] = "View all";
$lang['Admin']["Events"]["ClearAll"] = "Delete all";
$lang['Admin']["Events"]["Delete"]["Header"] = "Do you really want to delete all the logs ?";
$lang['Admin']["Events"]["Delete"]["Done"] = "All the logs where deleted";
$lang['Admin']["Events"]["Sort"]["All"] = "All";
$lang['Admin']["Events"]["Sort"]["Logins"] = "Logins";
$lang['Admin']["Events"]["Sort"]["Deletions"] = "Deletions";
$lang['Admin']["Events"]["Sort"]["Modifications"] = "Modifications";
$lang['Admin']["Events"]["Sort"]["Stocks"] = "Stocks";

$lang['Admin']["Events"]["loginFailure"]["Icon"] = "fa fa-exclamation-triangle has-text-danger";
$lang['Admin']["Events"]["loginFailure"]["DisplayText"] = "Failed password attemp for the user <b>{username}</b> with the ip: <b>{ip}</b>";
$lang['Admin']["Events"]["loginSuccess"]["Icon"] = "fa fa-sign-in-alt has-text-success";
$lang['Admin']["Events"]["loginSuccess"]["DisplayText"] = "The user <b>{username}</b> has connected with the ip: <b>{ip}</b>";
$lang['Admin']["Events"]["roleAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["roleAdded"]["DisplayText"] = "The user <b>{username}</b> has added the access group <b>{group}</b>";
$lang['Admin']["Events"]["roleUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["roleUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the access group <b>{group}</b>";
$lang['Admin']["Events"]["roleDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["roleDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the access group <b>{group}</b>";
$lang['Admin']["Events"]["productAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["productAdded"]["DisplayText"] = "The user <b>{username}</b> has added the product <b>{product}</b>";
$lang['Admin']["Events"]["productUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["productUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the product <b>{product}</b>";
$lang['Admin']["Events"]["productDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["productDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the product <b>{product}</b>";
$lang['Admin']["Events"]["inventoryUpdated"]["Icon"] = "fa fa-boxes has-text-info";
$lang['Admin']["Events"]["inventoryUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the inventory of the product <b>{product}</b>";
$lang['Admin']["Events"]["parameterAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["parameterAdded"]["DisplayText"] = "The user <b>{username}</b> has added the parameter <b>{parameter}</b>";
$lang['Admin']["Events"]["parameterUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["parameterUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the parameter <b>{parameter}</b>";
$lang['Admin']["Events"]["parameterDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["parameterDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the parameter <b>{parameter}</b>";
$lang['Admin']["Events"]["userAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["userAdded"]["DisplayText"] = "The user <b>{username}</b> has added the employee <b>{user}</b>";
$lang['Admin']["Events"]["userUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["userUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the employee <b>{user}</b>";
$lang['Admin']["Events"]["userDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["userDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the employee <b>{user}</b>";
$lang['Admin']["Events"]["userAuthChanged"]["Icon"] = "fa fa-key has-text-link";
$lang['Admin']["Events"]["userAuthChanged"]["DisplayText"] = "The user <b>{username}</b> à changé the authorization of the employee <b>{user}</b>";
$lang['Admin']["Events"]["userPassword"]["Icon"] = "fa fa-edit has-text-link";
$lang['Admin']["Events"]["userPassword"]["DisplayText"] = "The user <b>{username}</b> has modified the password of the employee <b>{user}</b>";
$lang['Admin']["Events"]["branchAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["branchAdded"]["DisplayText"] = "The user <b>{username}</b> has added the branch <b>{branch}</b>";
$lang['Admin']["Events"]["branchUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["branchUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the branch <b>{branch}</b>";
$lang['Admin']["Events"]["branchDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["branchDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the branch <b>{branch}</b>";
$lang['Admin']["Events"]["setAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["setAdded"]["DisplayText"] = "The user <b>{username}</b> has added the options group <b>{set}</b>";
$lang['Admin']["Events"]["setUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["setUpdated"]["DisplayText"] = "The user <b>{username}</b> has modified the options group <b>{set}</b>";
$lang['Admin']["Events"]["setDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["setDeleted"]["DisplayText"] = "The user <b>{username}</b> has deleted the options group <b>{set}</b>";

$lang['Admin']["SideMenu"]["General"]["Header"] = "General";
$lang['Admin']["SideMenu"]["General"]["Dashboard"] = "Dashboard";
$lang['Admin']["SideMenu"]["Administration"]["Header"] = "Adminsitration";
$lang['Admin']["SideMenu"]["Administration"]["Settings"] = "General settings";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Header"] = "Management";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Employees"] = "Employees";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Branches"] = "Branches";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Roles"] = "Access groups";
$lang['Admin']["SideMenu"]["Products"]["Header"] = "Products";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Header"] = "Parameters";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Makers"] = "Makers";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Specs"] = "Specification";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Categories"] = "Category";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Types"] = "Type";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Options"] = "Option";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Series"] = "Serie";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Subseries"] = "Sub-serie";
$lang['Admin']["SideMenu"]["Products"]["Branches"]["Header"] = "Branches";
$lang['Admin']["SideMenu"]["Products"]["Add"] = "Add a product";
$lang['Admin']["SideMenu"]["Products"]["List"] = "Products list";
$lang['Admin']["SideMenu"]["Products"]["MassUpload"] = "Mass upload";

$lang['Admin']["UserSettings"]["PageTitle"] = "General settings";
$lang['Admin']["UserSettings"]["MaxPasswordAttemps"]["Number"] = "Number of login attemps before activating reCAPTCHA";
$lang['Admin']["UserSettings"]["ExchangeRate"]["Header"] = "Exchange rate (1 USD &rArr; X CAD)";
$lang['Admin']["UserSettings"]["ExchangeRate"]["Helper"] = "Use a notation with '.' and no spaces (ex: 1.31)";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["Header"] = "Password complexity";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L1"] = "At least: 4 caracters, letters and numbers";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L2"] = "At least: 8 caracters, 1 upercase letter, 1 lowercase letter and 1 number";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L3"] = "At least: 8 caracters, 1 upercase letter, 1 lowercase letter, 1 number and 1 special caracter";
$lang['Admin']["UserSettings"]["StockLevels"]["Header"] = "Alert (visual) of stock level";
$lang['Admin']["UserSettings"]["StockLevels"]["Danger"] = "Stock level critical";
$lang['Admin']["UserSettings"]["StockLevels"]["Warning"] = "Stock level low";
$lang['Admin']["UserSettings"]["StockLevels"]["Helper"]["Danger"] = "Stock level critial shown in red";
$lang['Admin']["UserSettings"]["StockLevels"]["Helper"]["Warning"] = "Stock level low shown in orange";

$lang['Admin']["EmployeesList"]["PageTitle"] = "Employees list";
$lang['Admin']["EmployeesList"]["Table"]["ID"] = "ID";
$lang['Admin']["EmployeesList"]["Table"]["Firstname"] = "Firstname";
$lang['Admin']["EmployeesList"]["Table"]["Lastname"] = "Lastname";
$lang['Admin']["EmployeesList"]["Table"]["Username"] = "Username";
$lang['Admin']["EmployeesList"]["Table"]["Mail"] = "Email";
$lang['Admin']["EmployeesList"]["Table"]["Password"] = "Password";
$lang['Admin']["EmployeesList"]["Table"]["LastSeen"] = "Last intercation";
$lang['Admin']["EmployeesList"]["Table"]["Actions"]["Header"] = "Actions";
$lang['Admin']["EmployeesList"]["Table"]["Actions"]["ResetPassword"] = "Reset the password";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Header"] = "New password";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Placeholder"] = "Enter the new password";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Done"] = "The password was modified";
$lang['Admin']["EmployeesList"]["EditEmployee"]["Header"] = "Modify an employee";
$lang['Admin']["EmployeesList"]["EditEmployee"]["Done"] = "The employee was modified";
$lang['Admin']["EmployeesList"]["DeleteEmployee"]["Header"] = "Delete the employee ?";
$lang['Admin']["EmployeesList"]["DeleteEmployee"]["Done"] = "The employee was deleted";
$lang['Admin']["EmployeesList"]["AutorizationsEmployee"]["Header"] = "Employee's authorizations";
$lang['Admin']["EmployeesList"]["AutorizationsEmployee"]["Done"] = "The authorizations where modified";
$lang['Admin']["EmployeesList"]["AddEmployee"]["Header"] = "Add an employee";
$lang['Admin']["EmployeesList"]["AddEmployee"]["Done"] = "The employee was added";

$lang['Admin']["ProductsList"]["PageTitle"] = "Products list";
$lang['Admin']["ProductsList"]["Table"]["ID"] = "ID";
$lang['Admin']["ProductsList"]["Table"]["Name"] = "Product name";
$lang['Admin']["ProductsList"]["Table"]["Maker"] = "Maker";
$lang['Admin']["ProductsList"]["Table"]["Category"] = "Category";
$lang['Admin']["ProductsList"]["Table"]["Type"] = "Type";
$lang['Admin']["ProductsList"]["Table"]["Price"] = "List price";
$lang['Admin']["ProductsList"]["Table"]["Actions"]["Header"] = "Actions";
$lang['Admin']["ProductsList"]["EditProduct"]["Header"] = "Modify a product";
$lang['Admin']["ProductsList"]["EditProduct"]["Done"] = "The product was modified";
$lang['Admin']["ProductsList"]["DeleteProduct"]["Header"] = "Delete a product ?";
$lang['Admin']["ProductsList"]["DeleteProduct"]["Done"] = "The product was deleted";
$lang['Admin']["ProductsList"]["AddProduct"]["Header"] = "Add a product";
$lang['Admin']["ProductsList"]["AddProduct"]["Done"] = "The product was added";

$lang['Admin']["BranchesList"]["PageTitle"] = "Branches list";
$lang['Admin']["BranchesList"]["Table"]["ID"] = "ID";
$lang['Admin']["BranchesList"]["Table"]["Name"] = "Name";
$lang['Admin']["BranchesList"]["Table"]["Location"] = "Location";
$lang['Admin']["BranchesList"]["Table"]["Logo"] = "Logo";
$lang['Admin']["BranchesList"]["Table"]["Actions"] = "Actions";
$lang['Admin']["BranchesList"]["EditBranch"]["Header"] = "Modify a branch";
$lang['Admin']["BranchesList"]["EditBranch"]["Done"] = "The branch was modified";
$lang['Admin']["BranchesList"]["DeleteBranch"]["Header"] = "Delete a branch ?";
$lang['Admin']["BranchesList"]["DeleteBranch"]["Done"] = "The branch was deleted";
$lang['Admin']["BranchesList"]["AddBranch"]["Header"] = "Add a branch";
$lang['Admin']["BranchesList"]["AddBranch"]["Done"] = "The branch was added";

$lang['Admin']["OptionsSet"]["PageTitle"] = "Options groups list";
$lang['Admin']["OptionsSet"]["Table"]["ID"] = "ID";
$lang['Admin']["OptionsSet"]["Table"]["Name"] = "Name";
$lang['Admin']["OptionsSet"]["Table"]["OptionsCount"] = "Number of options";
$lang['Admin']["OptionsSet"]["Table"]["Relation"] = "Relation";
$lang['Admin']["OptionsSet"]["Table"]["Actions"] = "Actions";
$lang['Admin']["OptionsSet"]["EditSet"]["Header"] = "Modify an options group";
$lang['Admin']["OptionsSet"]["EditSet"]["Done"] = "The options group was modified";
$lang['Admin']["OptionsSet"]["DeleteSet"]["Header"] = "Delete an options group ?";
$lang['Admin']["OptionsSet"]["DeleteSet"]["Done"] = "The options group was deleted";
$lang['Admin']["OptionsSet"]["AddSet"]["Header"] = "Add an options group";
$lang['Admin']["OptionsSet"]["AddSet"]["Done"] = "The options group was added";

$lang['Admin']["SpecsSet"]["PageTitle"] = "Specifications groups list";
$lang['Admin']["SpecSet"]["Table"]["ID"] = "ID";
$lang['Admin']["SpecSet"]["Table"]["Name"] = "Name";
$lang['Admin']["SpecSet"]["Table"]["SpecsCount"] = "Number of specifications";
$lang['Admin']["SpecSet"]["Table"]["Relation"] = "Relation";
$lang['Admin']["SpecSet"]["Table"]["Actions"] = "Actions";
$lang['Admin']["SpecSet"]["EditSet"]["Header"] = "Modify an specifications group";
$lang['Admin']["SpecSet"]["EditSet"]["Done"] = "The specifications group was modified";
$lang['Admin']["SpecSet"]["DeleteSet"]["Header"] = "Delete an specifications group ?";
$lang['Admin']["SpecSet"]["DeleteSet"]["Done"] = "The specifications group was deleted";
$lang['Admin']["SpecSet"]["AddSet"]["Header"] = "Add an specifications group";
$lang['Admin']["SpecSet"]["AddSet"]["Done"] = "The specifications group was added";

$lang['Admin']["RolesList"]["PageTitle"] = "Access group list";
$lang['Admin']["RolesList"]["Table"]["ID"] = "ID";
$lang['Admin']["RolesList"]["Table"]["Title"] = "Title";
$lang['Admin']["RolesList"]["Table"]["PermCount"] = "Number of permissions";
$lang['Admin']["RolesList"]["Table"]["Actions"] = "Actions";
$lang['Admin']["RolesList"]["EditRole"]["Header"] = "Modify an access group";
$lang['Admin']["RolesList"]["EditRole"]["Title"] = "Titre";
$lang['Admin']["RolesList"]["EditRole"]["Description"] = "Description";
$lang['Admin']["RolesList"]["EditRole"]["Done"] = "The access group was modified";
$lang['Admin']["RolesList"]["DeleteRole"]["Header"] = "Delete an access group ?";
$lang['Admin']["RolesList"]["DeleteRole"]["Done"] = "The access group was deleted";
$lang['Admin']["RolesList"]["AddRole"]["Header"] = "Add an access group";
$lang['Admin']["RolesList"]["AddRole"]["Title"] = "Titre";
$lang['Admin']["RolesList"]["AddRole"]["Description"] = "Description";
$lang['Admin']["RolesList"]["AddRole"]["Done"] = "The access group was added";

$lang['Admin']["AddProduct"]["PageTitle"] = "Add a product";
$lang['Admin']["AddProduct"]["Name"] = "Name of the product";
$lang['Admin']["AddProduct"]["Description"] = "Description of the product";
$lang['Admin']["AddProduct"]["SequenceNumber"] = "Sequence Number";
$lang["Admin"]["AddProduct"]["Specs"]["Header"] = "Specifications";
$lang["Admin"]["AddProduct"]["Specs"]["Title"] = "Title of the specification";
$lang["Admin"]["AddProduct"]["Specs"]["Argument"] = "Argument";
$lang["Admin"]["AddProduct"]["Opts"]["Header"] = "Options";
$lang["Admin"]["AddProduct"]["Opts"]["Title"] = "Option name";
$lang["Admin"]["AddProduct"]["Opts"]["Argument"] = "Provider cost";
$lang["Admin"]["AddProduct"]["Inventory"]["Header"] = "Inventory";
$lang["Admin"]["AddProduct"]["Inventory"]["InStock"] = "In stock at the moment";
$lang["Admin"]["AddProduct"]["Inventory"]["OnOrder"] = "On order at the moment";
$lang["Admin"]["AddProduct"]["Image"]["Header"] = "Picture";
$lang["Admin"]["AddProduct"]["Price"] = "List price";
$lang["Admin"]["AddProduct"]["Documents"]["Header"] = "Documents";
$lang["Admin"]["AddProduct"]["Documents"]["Name"] = "Document name";
$lang["Admin"]["AddProduct"]["Documents"]["Upload"] = "Document access link";
$lang["Admin"]["AddProduct"]["Done"] = "The product was added";
$lang["Admin"]["AddProduct"]["Branches"]["Header"] = "Discount and Multipliers per branches";
$lang["Admin"]["AddProduct"]["Branches"]["Discount"] = "Discounts";
$lang["Admin"]["AddProduct"]["Branches"]["Multiplier"] = "Multipliers";
$lang["Admin"]["AddProduct"]["Branches"]["Help"] = "Percentage value without symbol, to enter several values, please separate them with a comma or enter 0 if one of the fields does not apply (ex: 40, 10)";
$lang["Admin"]["AddProduct"]["PriceDisclamer"] = "All prices must be in USD, use the notation with the dot and no symbol";
$lang["Admin"]["AddProduct"]["TransportPrice"] = "Transport percentage";

$lang['Admin']["EditProduct"]["PageTitle"] = "Modify a product";

$lang['Admin']["ParametersList"]["PageTitle"] = "Product editions";
$lang['Admin']["ParametersList"]["Table"]["ID"] = "ID";
$lang['Admin']["ParametersList"]["Table"]["Name"] = "Name";
$lang['Admin']["ParametersList"]["Table"]["Image"] = "Picture";
$lang['Admin']["ParametersList"]["Table"]["Actions"] = "Actions";

$lang['Admin']["ParametersList"]["Categories"] = "Categories list";
$lang['Admin']["ParametersList"]["EditCategories"]["Header"] = "Modify a category";
$lang['Admin']["ParametersList"]["EditCategories"]["Done"] = "The category was modified";
$lang['Admin']["ParametersList"]["DeleteCategories"]["Header"] = "Delete a category ?";
$lang['Admin']["ParametersList"]["DeleteCategories"]["Done"] = "The category was deleted";
$lang['Admin']["ParametersList"]["AddCategories"]["Header"] = "Add a category";
$lang['Admin']["ParametersList"]["AddCategories"]["Done"] = "The category was added";
$lang['Admin']["ParametersList"]["Makers"] = "Makers list";
$lang['Admin']["ParametersList"]["OptionsSet"] = "Options groups list";
$lang['Admin']["ParametersList"]["EditMakers"]["Header"] = "Modify a maker";
$lang['Admin']["ParametersList"]["EditMakers"]["Done"] = "The maker was modified";
$lang['Admin']["ParametersList"]["DeleteMakers"]["Header"] = "Delete a maker ?";
$lang['Admin']["ParametersList"]["DeleteMakers"]["Done"] = "The maker was deleted";
$lang['Admin']["ParametersList"]["AddMakers"]["Header"] = "Add a maker";
$lang['Admin']["ParametersList"]["AddMakers"]["Done"] = "The maker was added";
$lang['Admin']["ParametersList"]["Types"] = "Types list";
$lang['Admin']["ParametersList"]["EditTypes"]["Header"] = "Modify a type";
$lang['Admin']["ParametersList"]["EditTypes"]["Done"] = "The type was modified";
$lang['Admin']["ParametersList"]["DeleteTypes"]["Header"] = "Delete a type ?";
$lang['Admin']["ParametersList"]["DeleteTypes"]["Done"] = "The type was deleted";
$lang['Admin']["ParametersList"]["AddTypes"]["Header"] = "Add a type";
$lang['Admin']["ParametersList"]["AddTypes"]["Done"] = "The type was added";
$lang['Admin']["ParametersList"]["Series"] = "Series list";
$lang['Admin']["ParametersList"]["EditSeries"]["Header"] = "Modify a serie";
$lang['Admin']["ParametersList"]["EditSeries"]["Done"] = "The serie was modified";
$lang['Admin']["ParametersList"]["DeleteSeries"]["Header"] = "Delete a serie ?";
$lang['Admin']["ParametersList"]["DeleteSeries"]["Done"] = "The serie was deleted";
$lang['Admin']["ParametersList"]["AddSeries"]["Header"] = "Add a serie";
$lang['Admin']["ParametersList"]["AddSeries"]["Done"] = "The serie was added";
$lang['Admin']["ParametersList"]["Subseries"] = "Sub-series list";
$lang['Admin']["ParametersList"]["EditSubseries"]["Header"] = "Modify a sub-serie";
$lang['Admin']["ParametersList"]["EditSubseries"]["Done"] = "The sub-serie was modified";
$lang['Admin']["ParametersList"]["DeleteSubseries"]["Header"] = "Delete a sub-serie ?";
$lang['Admin']["ParametersList"]["DeleteSubseries"]["Done"] = "The sub-serie was deleted";
$lang['Admin']["ParametersList"]["AddSubseries"]["Header"] = "Add a sub-serie";
$lang['Admin']["ParametersList"]["AddSubseries"]["Done"] = "The sub-serie was added";

$lang['Admin']["Permissions"]["PageTitle"] = "Permissions";
$lang['Admin']["Permissions"]["AccessToBranch"] = "Access right to the products of the branch";
$lang['Admin']["Permissions"]["Group"] = "Access group";
$lang['Admin']["Permissions"]["Employee"] = "Employee's permissions";
$lang['Admin']["Permissions"]["IsAdminWarning"] = "This employee is an administrator, any change will be ignored";

$lang["Admin"]["EditSet"]["PageTitle"] = "Modification of an options group";
$lang["Admin"]["AddSet"]["PageTitle"] = "Addition of an options group";
$lang["Admin"]["EditSet"]["Name"] = "Group name";
$lang["Admin"]["EditSet"]["Relation"] = "Relation between the options";
$lang["Admin"]["EditSet"]["Opts"]["Header"] = "Options";
$lang["Admin"]["EditSet"]["Opts"]["Title"] = "Option name";
$lang["Admin"]["EditSet"]["Opts"]["Argument"] = "Provider cost";
$lang["Admin"]["EditSet"]["Specs"]["Header"] = "Specifications";
$lang["Admin"]["EditSet"]["Specs"]["Title"] = "Specification name";
$lang["Admin"]["EditSet"]["Specs"]["Argument"] = "Argument";
$lang["Admin"]["EditSet"]["PriceDisclamer"] = "All prices must be in USD, use the notation with the dot and no symbol";

$lang["Admin"]["MassUpload"]["PageTitle"] = "Mass upload";
$lang["Admin"]["MassUpload"]["NotCSV"] = "Teh file is not in the 'CSV' format";
$lang["Admin"]["MassUpload"]["GetTheJSON"] = "Get the JSON";
$lang["Admin"]["MassUpload"]["JSONGenerator"] = "JSON Generator";
$lang["Admin"]["MassUpload"]["DownloadCSV"] = "Downlaod the CSV template";
$lang["Admin"]["MassUpload"]["Completed"] = "All the products where uploaded";


//ERRORS
$lang['Errors']["SomethingIsMissing"] = "Please fill all the fields";
$lang['Errors']["WrongUsername"] = "The username is wrong";
$lang['Errors']["WrongPassword"] = "The password is wrong";
$lang['Errors']["CompleteEverything"] = "Please complete all the fields";
$lang['Errors']["FieldCannotBeEmpty"] = "The field cannot be empty";
$lang['Errors']["FieldIsOK"] = "The field will be modifed";
$lang['Errors']["FieldOutOfBounds"] = "At least {1} and maximum {2} caracters";
$lang['Errors']["MailDoesNotMatch"] = "It is not a valid email";
$lang['Errors']["UpdateSucessful"] = "The informations where saved";
$lang['Errors']["EmailIsNotAsEmailFormat"] = "The email is not valid";
$lang['Errors']["TextTooLong"] = "One of the field has too much caracters";
$lang['Errors']["NoSession"] = "You need to login before accessing any pages";
$lang['Errors']["NoBranches"] = "You don't have the right to access the products of the branches";
$lang['Errors']["NotAutorized"] = "You are not allowed to access this page";
$lang['Errors']["FieldAreNotNumerical"] = "One of the numerical field received a non-numerical value";
$lang['Errors']["UnexpectedValue"] = "One of the filed receive an unexpected value";
$lang['Errors']["TooManyAttempt"] = "Your account was disabled during {0} because you failed to connect {1} times in a row";
$lang['Errors']["NotAnAdmin"] = "You are not an administrator";
$lang['Errors']["UnknownType"] = "Request type uncknown";
$lang['Errors']["NeedAPicture"] = "No picture was given";
$lang['Errors']["SQL"] = "An unknown error occured, please contact a member of the It service as soon as possible";

?>
