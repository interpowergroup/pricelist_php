<?php
/*
------------------
Language: Francais
------------------
*/

$lang = array();

//SHARED
$language = 'fr';
$lang['fr'] = 'Français';
$lang['en'] = 'Anglais';
$lang['Short'] = 'fr';
$lang['Long'] = 'Français';

$lang['KEY'] = 'VALUE';
$lang['Add'] = 'Ajouter';
$lang['Added'] = 'Ajouté';
$lang['Update'] = 'Mettre à jour';
$lang['Updated'] = 'Mis à jour';
$lang['Upload'] = 'Mettre en ligne';
$lang['Uploaded'] = 'Mis en ligne';
$lang['Cancel'] = 'Annuler';
$lang['Canceled'] = 'Annulé';
$lang['Edit'] = 'Modifier';
$lang['Edited'] = 'Modifié';
$lang['Delete'] = 'Supprimer';
$lang['Deleted'] = 'Supprimé';
$lang['Return'] = 'Retour';
$lang['UndefinedAmountOfTime'] = 'un temps indéfini';
$lang['Hours'] = 'Heures';
$lang['Minutes'] = 'Minutes';
$lang['Seconds'] = 'Secondes';
$lang['None'] = 'Aucune';
$lang['Invalid'] = 'Invalide';



//NAVBAR
$lang['Navbar']["Branch"] = "Succursales";
$lang['Navbar']["Search"] = "Rechercher un produit...";
$lang['Navbar']["Categories"]["Header"] = "Catégories";
$lang['Navbar']["Categories"]["All"] = "Toutes les catégories";
$lang['Navbar']["Categories"]["AllProducts"] = "Tout l'inventaire";
$lang['Navbar']["Categories"]["AllPrices"] = "Tout les prix";
$lang['Navbar']["Categories"]["NothingToShow"] = "Aucun produit ne correspond à votre recherche";
$lang['Navbar']["SearchInventory"] = "Rechercher l'inventaire";
$lang['Navbar']["Account"]["MyAccount"] = "Mon compte";
$lang['Navbar']["Account"]["Profile"] = "Profil utilisateur";
$lang['Navbar']["Account"]["Admin"] = "Admin";
$lang['Navbar']["Account"]["Logout"] = "Logout";
$lang['Navbar']["Language"] = "Language";
$lang['Navbar']["SelectLanguage"] = "Sélectionner votre language";


//LOGIN
$lang['Login']["PageTitle"] = "Connexion";
$lang['Login']["Username"] = "Nom d'utilisateur";
$lang['Login']["Password"] = "Mot de passe";
$lang['Login']["RememberMe"] = "Se souvenir de moi";


//BRANCH SELECTION
$lang['BranchSelection']["PageTitle"] = "Sélection de la succursale";
$lang['BranchSelection']["PleaseSelect"] = "Veuillez sélectionner une succursale";
$lang['BranchSelection']["SeeStock"] = "Voir l'inventaire";
$lang['BranchSelection']["SeeProduct"] = "Voir le produit";
$lang['BranchSelection']["Select"] = "Sélectionner";


//POFILE (DO not remove before chedcking if no other pages use theses varibales)
$lang['Profile']["PageTitle"] = "Profil utilisateur";
$lang['Profile']["Name"]["Header"] = "Nom";
$lang['Profile']["Name"]["Firstname"] = "Prénom";
$lang['Profile']["Name"]["Lastname"] = "Nom de famille";
$lang['Profile']["Username"]["Header"] = "Nom d'utilisateur";
$lang['Profile']["Username"]["Placeholder"] = "Mon nom d'utilisateur";
$lang['Profile']["Username"]["Help"] = "Ce champ est généré automatiquement en relation avec votre prénom et nom de famille";
$lang['Profile']["Mail"]["Header"] = "Adresse couriel";
$lang['Profile']["Mail"]["Placeholder"] = "Mon adresse couriel";
$lang['Profile']["Password"]["Header"] = "Mot de passe";
$lang['Profile']["Password"]["Placeholder"] = "Mon nouveau mot de passe";


//PRODUCT VIEWER
$lang['ProductViewer']["BasePrice"] = "Prix de base";
$lang['ProductViewer']["OptionsPrice"]["Opts"] = "Options";
$lang['ProductViewer']["OptionsPrice"]["Final"] = "Total";
$lang['ProductViewer']["Documents"] = "Documents";
$lang['ProductViewer']["NoDocuments"] = "Aucun document disponible";
$lang['ProductViewer']["Specs"] = "Spécifications";
$lang['ProductViewer']["NoSpecs"] = "Aucune spécification disponible";
$lang['ProductViewer']["Options"] = "Options";
$lang['ProductViewer']["InStock"] = "En stock";
$lang['ProductViewer']["OnOrder"] = "En commande";
$lang['ProductViewer']["wasRemoved"] = "a été retiré de";
$lang['ProductViewer']["whereRemoved"] = "ont été retiré de";
$lang['ProductViewer']["wasAdded"] = "a été ajouté de";
$lang['ProductViewer']["whereAdded"] = "ont été ajouté de";
$lang['ProductViewer']["Add"] = "Ajouter";
$lang['ProductViewer']["Remove"] = "Retirer";
$lang['ProductViewer']["CannotEnterNegativeValue"] = "Vous ne pouvez pas entrer de valeur négative";


//PRODUCTS
$lang['Products']["Name"] = "Nom";
$lang['Products']["Price"] = "Prix";
$lang['Products']["Stock"] = "Stock";
$lang['Products']["Order"] = "Order";
$lang['Products']["Description"] = "Description";
$lang['Products']["SCFM"] = "SCFM";
$lang['Products']["PSI"] = "PSI";
$lang['Products']["HP"] = "HP";
$lang['Products']["Voltage"] = "Voltage";

$lang['Products']["ListPrice"] = "Prix de liste";
$lang['Products']["Discount1"] = "Discount 1";
$lang['Products']["Discount2"] = "Discount 2";
$lang['Products']["BeforeExchange"] = "Coût avant échange";
$lang['Products']["Transport"] = "Transport";
$lang['Products']["ExchangeRate"] = "Taux de change";
$lang['Products']["AfterExchange"] = "Coût après échange";
$lang['Products']["ProfitMultiplier"] = "Multiplicateur de profit";
$lang['Products']["SellingPrice"] = "Prix de vente";


//ADMIN
$lang['Admin']["PageTitle"] = "Administration";
$lang['Admin']["Employees"] = "Employés";
$lang['Admin']["Employee"] = "Employé";
$lang['Admin']["Branches"] = "Succursales";
$lang['Admin']["Branch"] = "Succursale";
$lang['Admin']["Categories"] = "Catégories";
$lang['Admin']["Category"] = "Catégorie";
$lang['Admin']["Products"] = "Produits";
$lang['Admin']["Product"] = "Produit";
$lang['Admin']["Roles"] = "Roles";
$lang['Admin']["Role"] = "Role";
$lang['Admin']["OptsSets"] = "Groupes d'options";
$lang['Admin']["OptsSet"] = "Groupe d'options";
$lang['Admin']["SpcsSets"] = "Groupes de spécifications";
$lang['Admin']["SpcsSet"] = "Groupes de spécifications";
$lang['Admin']["ProductSearch"] = "Recherche de produits";
$lang['Admin']["EmployeeSearch"] = "Recherche d'employés";

$lang['Admin']["Events"]["Header"] = "Évenements";
$lang['Admin']["Events"]["ViewAll"] = "Voir tout";
$lang['Admin']["Events"]["ClearAll"] = "Supprimer tout";
$lang['Admin']["Events"]["Delete"]["Header"] = "Voulez-vous vraiment supprimé tous les logs ?";
$lang['Admin']["Events"]["Delete"]["Done"] = "Tous les logs ont été supprimés";
$lang['Admin']["Events"]["Sort"]["All"] = "Tout";
$lang['Admin']["Events"]["Sort"]["Logins"] = "Connections";
$lang['Admin']["Events"]["Sort"]["Deletions"] = "Suppressions";
$lang['Admin']["Events"]["Sort"]["Modifications"] = "Modifications";
$lang['Admin']["Events"]["Sort"]["Stocks"] = "Inventaires";

$lang['Admin']["Events"]["loginFailure"]["Icon"] = "fa fa-exclamation-triangle has-text-danger";
$lang['Admin']["Events"]["loginFailure"]["DisplayText"] = "Tentative de mot de passe échoué pour l'utilisateur <b>{username}</b> avec l'ip: <b>{ip}</b>";
$lang['Admin']["Events"]["loginSuccess"]["Icon"] = "fa fa-sign-in-alt has-text-success";
$lang['Admin']["Events"]["loginSuccess"]["DisplayText"] = "L'utilisateur <b>{username}</b> s'est connecté avec l'ip: <b>{ip}</b>";
$lang['Admin']["Events"]["roleAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["roleAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté le groupe d'accès <b>{group}</b>";
$lang['Admin']["Events"]["roleUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["roleUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié le groupe d'accès <b>{group}</b>";
$lang['Admin']["Events"]["roleDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["roleDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé le groupe d'accès <b>{group}</b>";
$lang['Admin']["Events"]["productAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["productAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté le produit <b>{product}</b>";
$lang['Admin']["Events"]["productUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["productUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié le produit <b>{product}</b>";
$lang['Admin']["Events"]["productDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["productDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé le produit <b>{product}</b>";
$lang['Admin']["Events"]["inventoryUpdated"]["Icon"] = "fa fa-boxes has-text-info";
$lang['Admin']["Events"]["inventoryUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié l'inventaire du produit <b>{product}</b>";
$lang['Admin']["Events"]["parameterAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["parameterAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté le paramètre <b>{parameter}</b>";
$lang['Admin']["Events"]["parameterUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["parameterUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié le paramètre <b>{parameter}</b>";
$lang['Admin']["Events"]["parameterDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["parameterDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé le paramètre <b>{parameter}</b>";
$lang['Admin']["Events"]["userAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["userAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté l'employé <b>{user}</b>";
$lang['Admin']["Events"]["userUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["userUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié l'employé <b>{user}</b>";
$lang['Admin']["Events"]["userDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["userDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé l'employé <b>{user}</b>";
$lang['Admin']["Events"]["userAuthChanged"]["Icon"] = "fa fa-key has-text-link";
$lang['Admin']["Events"]["userAuthChanged"]["DisplayText"] = "L'utilisateur <b>{username}</b> à changé les autorisations de l'employé <b>{user}</b>";
$lang['Admin']["Events"]["userPassword"]["Icon"] = "fa fa-edit has-text-link";
$lang['Admin']["Events"]["userPassword"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié le mot de passe de l'employé <b>{user}</b>";
$lang['Admin']["Events"]["branchAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["branchAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté la succursale <b>{branch}</b>";
$lang['Admin']["Events"]["branchUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["branchUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié la succursale <b>{branch}</b>";
$lang['Admin']["Events"]["branchDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["branchDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé la succursale <b>{branch}</b>";
$lang['Admin']["Events"]["setAdded"]["Icon"] = "fa fa-plus has-text-link";
$lang['Admin']["Events"]["setAdded"]["DisplayText"] = "L'utilisateur <b>{username}</b> à ajouté le groupe d'options <b>{set}</b>";
$lang['Admin']["Events"]["setUpdated"]["Icon"] = "fa fa-edit has-text-info";
$lang['Admin']["Events"]["setUpdated"]["DisplayText"] = "L'utilisateur <b>{username}</b> à modifié le groupe d'options <b>{set}</b>";
$lang['Admin']["Events"]["setDeleted"]["Icon"] = "fa fa-trash has-text-primary";
$lang['Admin']["Events"]["setDeleted"]["DisplayText"] = "L'utilisateur <b>{username}</b> à supprimé le groupe d'options <b>{set}</b>";

$lang['Admin']["SideMenu"]["General"]["Header"] = "Général";
$lang['Admin']["SideMenu"]["General"]["Dashboard"] = "Tableau de bord";
$lang['Admin']["SideMenu"]["Administration"]["Header"] = "Adminsitration";
$lang['Admin']["SideMenu"]["Administration"]["Settings"] = "Paramètres généraux";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Header"] = "Gestion";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Employees"] = "Employés";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Branches"] = "Succursales";
$lang['Admin']["SideMenu"]["Administration"]["Management"]["Roles"] = "Groupes d'accès";
$lang['Admin']["SideMenu"]["Products"]["Header"] = "Produits";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Header"] = "Paramètres";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Makers"] = "Manufacturier";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Specs"] = "Spécification";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Categories"] = "Catégorie";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Types"] = "Type";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Options"] = "Option";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Series"] = "Série";
$lang['Admin']["SideMenu"]["Products"]["Settings"]["Subseries"] = "Sous-série";
$lang['Admin']["SideMenu"]["Products"]["Branches"]["Header"] = "Succursales";
$lang['Admin']["SideMenu"]["Products"]["Add"] = "Ajouter un produit";
$lang['Admin']["SideMenu"]["Products"]["List"] = "Liste des produits";
$lang['Admin']["SideMenu"]["Products"]["MassUpload"] = "Upload de masse";

$lang['Admin']["UserSettings"]["PageTitle"] = "Paramètres généraux";
$lang['Admin']["UserSettings"]["MaxPasswordAttemps"]["Number"] = "Nombre de tentative de connexion avant l'action de reCAPTCHA";
$lang['Admin']["UserSettings"]["ExchangeRate"]["Header"] = "Taux de change (1 USD &rArr; X CAD)";
$lang['Admin']["UserSettings"]["ExchangeRate"]["Helper"] = "Utilisez la notation avec des '.' et aucun espace (ex: 1.31)";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["Header"] = "Complexité du mot de passe";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L1"] = "Au moins: 4 caractères, des lettres et des chiffres";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L2"] = "Au moins: 8 caractères, 1 lettre majuscule, 1 lettre minuscule et 1 chiffre";
$lang['Admin']["UserSettings"]["PasswordComplexity"]["L3"] = "Au moins: 8 caractères, 1 lettre majuscule, 1 lettre minuscule, 1 chiffres et 1 caractère spécial";
$lang['Admin']["UserSettings"]["StockLevels"]["Header"] = "Alert (visuelle) du niveau des stocks";
$lang['Admin']["UserSettings"]["StockLevels"]["Danger"] = "Niveau du stock critique";
$lang['Admin']["UserSettings"]["StockLevels"]["Warning"] = "Niveau du stock bas";
$lang['Admin']["UserSettings"]["StockLevels"]["Helper"]["Danger"] = "Niveau du stock critique affiché en rouge";
$lang['Admin']["UserSettings"]["StockLevels"]["Helper"]["Warning"] = "Niveau du stock bas affiché en orange";

$lang['Admin']["EmployeesList"]["PageTitle"] = "Liste des employés";
$lang['Admin']["EmployeesList"]["Table"]["ID"] = "ID";
$lang['Admin']["EmployeesList"]["Table"]["Firstname"] = "Prénom";
$lang['Admin']["EmployeesList"]["Table"]["Lastname"] = "Nom de famille";
$lang['Admin']["EmployeesList"]["Table"]["Username"] = "Nom d'utilisateur";
$lang['Admin']["EmployeesList"]["Table"]["Mail"] = "Adresse couriel";
$lang['Admin']["EmployeesList"]["Table"]["Password"] = "Mot de passe";
$lang['Admin']["EmployeesList"]["Table"]["LastSeen"] = "Dernière Interaction";
$lang['Admin']["EmployeesList"]["Table"]["Actions"]["Header"] = "Actions";
$lang['Admin']["EmployeesList"]["Table"]["Actions"]["ResetPassword"] = "Renouveler le mot de passe";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Header"] = "Nouveau mot de passe";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Placeholder"] = "Entrez le nouveau mot de passe";
$lang['Admin']["EmployeesList"]["ResetPassword"]["Done"] = "Le mot de passe à été modifié";
$lang['Admin']["EmployeesList"]["EditEmployee"]["Header"] = "Modifier un employé";
$lang['Admin']["EmployeesList"]["EditEmployee"]["Done"] = "L'employé a été modifié";
$lang['Admin']["EmployeesList"]["DeleteEmployee"]["Header"] = "Supprimer un employé ?";
$lang['Admin']["EmployeesList"]["DeleteEmployee"]["Done"] = "L'employé a été supprimé";
$lang['Admin']["EmployeesList"]["AutorizationsEmployee"]["Header"] = "Autorisations d'employé";
$lang['Admin']["EmployeesList"]["AutorizationsEmployee"]["Done"] = "Les autorisations ont été modifié";
$lang['Admin']["EmployeesList"]["AddEmployee"]["Header"] = "Ajouter un employé";
$lang['Admin']["EmployeesList"]["AddEmployee"]["Done"] = "L'employé a été ajouté";

$lang['Admin']["ProductsList"]["PageTitle"] = "Liste des produits";
$lang['Admin']["ProductsList"]["Table"]["ID"] = "Sequence";
$lang['Admin']["ProductsList"]["Table"]["Name"] = "Nom du produit";
$lang['Admin']["ProductsList"]["Table"]["Maker"] = "Manufacturier";
$lang['Admin']["ProductsList"]["Table"]["Category"] = "Catégorie";
$lang['Admin']["ProductsList"]["Table"]["Type"] = "Type";
$lang['Admin']["ProductsList"]["Table"]["Price"] = "Prix de liste";
$lang['Admin']["ProductsList"]["Table"]["Actions"]["Header"] = "Actions";
$lang['Admin']["ProductsList"]["EditProduct"]["Header"] = "Modifier un produit";
$lang['Admin']["ProductsList"]["EditProduct"]["Done"] = "Le produit a été modifié";
$lang['Admin']["ProductsList"]["DeleteProduct"]["Header"] = "Supprimer un produit ?";
$lang['Admin']["ProductsList"]["DeleteProduct"]["Done"] = "Le produit a été supprimé";
$lang['Admin']["ProductsList"]["AddProduct"]["Header"] = "Ajouter un produit";
$lang['Admin']["ProductsList"]["AddProduct"]["Done"] = "Le produit a été ajouté'";

$lang['Admin']["BranchesList"]["PageTitle"] = "Liste des succursales";
$lang['Admin']["BranchesList"]["Table"]["ID"] = "ID";
$lang['Admin']["BranchesList"]["Table"]["Name"] = "Nom";
$lang['Admin']["BranchesList"]["Table"]["Location"] = "Lieu";
$lang['Admin']["BranchesList"]["Table"]["Logo"] = "Logo";
$lang['Admin']["BranchesList"]["Table"]["Actions"] = "Actions";
$lang['Admin']["BranchesList"]["EditBranch"]["Header"] = "Modifier une succursale";
$lang['Admin']["BranchesList"]["EditBranch"]["Done"] = "La succursale a été modifié";
$lang['Admin']["BranchesList"]["DeleteBranch"]["Header"] = "Supprimer une succursale ?";
$lang['Admin']["BranchesList"]["DeleteBranch"]["Done"] = "La succursale a été supprimé";
$lang['Admin']["BranchesList"]["AddBranch"]["Header"] = "Ajouter une succursale";
$lang['Admin']["BranchesList"]["AddBranch"]["Done"] = "La succursale a été ajouté";

$lang['Admin']["OptionsSet"]["PageTitle"] = "Liste des groupes d'options";
$lang['Admin']["OptionsSet"]["Table"]["ID"] = "ID";
$lang['Admin']["OptionsSet"]["Table"]["Name"] = "Nom";
$lang['Admin']["OptionsSet"]["Table"]["OptionsCount"] = "Nombre d'options";
$lang['Admin']["OptionsSet"]["Table"]["Relation"] = "Relation";
$lang['Admin']["OptionsSet"]["Table"]["Actions"] = "Actions";
$lang['Admin']["OptionsSet"]["EditSet"]["Header"] = "Modifier un groupe d'options";
$lang['Admin']["OptionsSet"]["EditSet"]["Done"] = "Le groupe d'options a été modifié";
$lang['Admin']["OptionsSet"]["DeleteSet"]["Header"] = "Supprimer un groupe d'options ?";
$lang['Admin']["OptionsSet"]["DeleteSet"]["Done"] = "Le groupe d'options a été supprimé";
$lang['Admin']["OptionsSet"]["AddSet"]["Header"] = "Ajouter un groupe d'options";
$lang['Admin']["OptionsSet"]["AddSet"]["Done"] = "Le groupe d'options a été ajouté";

$lang['Admin']["SpecsSet"]["PageTitle"] = "Liste des groupes de spécifications";
$lang['Admin']["SpecsSet"]["Table"]["ID"] = "ID";
$lang['Admin']["SpecsSet"]["Table"]["Name"] = "Nom";
$lang['Admin']["SpecsSet"]["Table"]["SpecsCount"] = "Nombre de spécifications";
$lang['Admin']["SpecsSet"]["Table"]["Relation"] = "Relation";
$lang['Admin']["SpecsSet"]["Table"]["Actions"] = "Actions";
$lang['Admin']["SpecsSet"]["EditSet"]["Header"] = "Modifier un groupe de spécifications";
$lang['Admin']["SpecsSet"]["EditSet"]["Done"] = "Le groupe de spécifications a été modifié";
$lang['Admin']["SpecsSet"]["DeleteSet"]["Header"] = "Supprimer un groupe de spécifications";
$lang['Admin']["SpecsSet"]["DeleteSet"]["Done"] = "Le groupe de spécifications a été supprimé";
$lang['Admin']["SpecsSet"]["AddSet"]["Header"] = "Ajouter un groupe de spécifications";
$lang['Admin']["SpecsSet"]["AddSet"]["Done"] = "Le groupe de spécifications a été ajouté";

$lang['Admin']["RolesList"]["PageTitle"] = "Liste des groupes d'accès";
$lang['Admin']["RolesList"]["Table"]["ID"] = "ID";
$lang['Admin']["RolesList"]["Table"]["Title"] = "Titre";
$lang['Admin']["RolesList"]["Table"]["PermCount"] = "Nombre de permissions";
$lang['Admin']["RolesList"]["Table"]["Actions"] = "Actions";
$lang['Admin']["RolesList"]["EditRole"]["Header"] = "Modifier un groupe d'accès";
$lang['Admin']["RolesList"]["EditRole"]["Title"] = "Titre";
$lang['Admin']["RolesList"]["EditRole"]["Description"] = "Description";
$lang['Admin']["RolesList"]["EditRole"]["Done"] = "Le groupe d'accès a été modifié";
$lang['Admin']["RolesList"]["DeleteRole"]["Header"] = "Supprimer un groupe d'accès ?";
$lang['Admin']["RolesList"]["DeleteRole"]["Done"] = "Le groupe d'accès a été supprimé";
$lang['Admin']["RolesList"]["AddRole"]["Header"] = "Ajouter un groupe d'accès";
$lang['Admin']["RolesList"]["AddRole"]["Title"] = "Titre";
$lang['Admin']["RolesList"]["AddRole"]["Description"] = "Description";
$lang['Admin']["RolesList"]["AddRole"]["Done"] = "Le groupe d'accès a été ajouté";

$lang['Admin']["AddProduct"]["PageTitle"] = "Ajouter un produit";
$lang['Admin']["AddProduct"]["Name"] = "Nom du produit";
$lang['Admin']["AddProduct"]["Description"] = "Description du produit";
$lang['Admin']["AddProduct"]["SequenceNumber"] = "Numéro de séquence";
$lang["Admin"]["AddProduct"]["Specs"]["Header"] = "Spécifications";
$lang["Admin"]["AddProduct"]["Specs"]["Title"] = "Titre de la spécification";
$lang["Admin"]["AddProduct"]["Specs"]["Argument"] = "Argument";
$lang["Admin"]["AddProduct"]["Opts"]["Header"] = "Options";
$lang["Admin"]["AddProduct"]["Opts"]["Title"] = "Nom de l'option";
$lang["Admin"]["AddProduct"]["Opts"]["Argument"] = "Coût du fournisseur";
$lang["Admin"]["AddProduct"]["Inventory"]["Header"] = "Inventaire";
$lang["Admin"]["AddProduct"]["Inventory"]["InStock"] = "En stock présentement";
$lang["Admin"]["AddProduct"]["Inventory"]["OnOrder"] = "En commande présentement";
$lang["Admin"]["AddProduct"]["Image"]["Header"] = "Image";
$lang["Admin"]["AddProduct"]["Price"] = "Prix de liste";
$lang["Admin"]["AddProduct"]["Documents"]["Header"] = "Documents";
$lang["Admin"]["AddProduct"]["Documents"]["Name"] = "Nom du document";
$lang["Admin"]["AddProduct"]["Documents"]["Upload"] = "Lien d'accès du document";
$lang["Admin"]["AddProduct"]["Done"] = "Le produit à été ajouté";
$lang["Admin"]["AddProduct"]["Branches"]["Header"] = "Rabais et Multiplicateurs par succursale";
$lang["Admin"]["AddProduct"]["Branches"]["Discount"] = "Rabais";
$lang["Admin"]["AddProduct"]["Branches"]["Multiplier"] = "Multiplicateurs";
$lang["Admin"]["AddProduct"]["Branches"]["Help"] = "Valeur en pourcentage sans le symbole, pour entrer plusieurs valeurs, veuillez les séparer par une virgule ou entrez 0 si un des champs ne s'applique pas (ex: 40, 10)";
$lang["Admin"]["AddProduct"]["PriceDisclamer"] = "Tous les prix doivent être en USD, utiliser la notation avec le point et aucun symbole";
$lang["Admin"]["AddProduct"]["TransportPrice"] = "Pourcentage du transport";

$lang['Admin']["EditProduct"]["PageTitle"] = "Modifier un produit";

$lang['Admin']["ParametersList"]["PageTitle"] = "Éditions des produits";
$lang['Admin']["ParametersList"]["Table"]["ID"] = "ID";
$lang['Admin']["ParametersList"]["Table"]["Name"] = "Nom";
$lang['Admin']["ParametersList"]["Table"]["Image"] = "Image";
$lang['Admin']["ParametersList"]["Table"]["Actions"] = "Actions";

$lang['Admin']["ParametersList"]["Categories"] = "Liste des catégories";
$lang['Admin']["ParametersList"]["EditCategories"]["Header"] = "Modifier une catégorie";
$lang['Admin']["ParametersList"]["EditCategories"]["Done"] = "La catégorie a été modifié";
$lang['Admin']["ParametersList"]["DeleteCategories"]["Header"] = "Supprimer une catégorie ?";
$lang['Admin']["ParametersList"]["DeleteCategories"]["Done"] = "La catégorie a été supprimé";
$lang['Admin']["ParametersList"]["AddCategories"]["Header"] = "Ajouter une catégorie";
$lang['Admin']["ParametersList"]["AddCategories"]["Done"] = "La catégorie a été ajouté";
$lang['Admin']["ParametersList"]["Makers"] = "Liste des manufacturiers";
$lang['Admin']["ParametersList"]["OptionsSet"] = "Liste des groupes d'options";
$lang['Admin']["ParametersList"]["EditMakers"]["Header"] = "Modifier un manufacturier";
$lang['Admin']["ParametersList"]["EditMakers"]["Done"] = "Le manufacturier a été modifié";
$lang['Admin']["ParametersList"]["DeleteMakers"]["Header"] = "Supprimer un manufacturier ?";
$lang['Admin']["ParametersList"]["DeleteMakers"]["Done"] = "Le manufacturier a été supprimé";
$lang['Admin']["ParametersList"]["AddMakers"]["Header"] = "Ajouter un manufacturier";
$lang['Admin']["ParametersList"]["AddMakers"]["Done"] = "Le manufacturier a été ajouté";
$lang['Admin']["ParametersList"]["Types"] = "Liste des types";
$lang['Admin']["ParametersList"]["EditTypes"]["Header"] = "Modifier un type";
$lang['Admin']["ParametersList"]["EditTypes"]["Done"] = "Le type a été modifié";
$lang['Admin']["ParametersList"]["DeleteTypes"]["Header"] = "Supprimer un type ?";
$lang['Admin']["ParametersList"]["DeleteTypes"]["Done"] = "Le type a été supprimé";
$lang['Admin']["ParametersList"]["AddTypes"]["Header"] = "Ajouter un type";
$lang['Admin']["ParametersList"]["AddTypes"]["Done"] = "Le type a été ajouté";
$lang['Admin']["ParametersList"]["Series"] = "Liste des series";
$lang['Admin']["ParametersList"]["EditSeries"]["Header"] = "Modifier une serie";
$lang['Admin']["ParametersList"]["EditSeries"]["Done"] = "La serie a été modifié";
$lang['Admin']["ParametersList"]["DeleteSeries"]["Header"] = "Supprimer une serie ?";
$lang['Admin']["ParametersList"]["DeleteSeries"]["Done"] = "La serie a été supprimé";
$lang['Admin']["ParametersList"]["AddSeries"]["Header"] = "Ajouter une serie";
$lang['Admin']["ParametersList"]["AddSeries"]["Done"] = "La serie a été ajouté";
$lang['Admin']["ParametersList"]["Subseries"] = "Liste des sous-series";
$lang['Admin']["ParametersList"]["EditSubseries"]["Header"] = "Modifier une sous-serie";
$lang['Admin']["ParametersList"]["EditSubseries"]["Done"] = "La sous-serie a été modifié";
$lang['Admin']["ParametersList"]["DeleteSubseries"]["Header"] = "Supprimer une sous-serie ?";
$lang['Admin']["ParametersList"]["DeleteSubseries"]["Done"] = "La sous-serie a été supprimé";
$lang['Admin']["ParametersList"]["AddSubseries"]["Header"] = "Ajouter une sous-serie";
$lang['Admin']["ParametersList"]["AddSubseries"]["Done"] = "La sous-serie a été ajouté";

$lang['Admin']["Permissions"]["PageTitle"] = "Permissions";
$lang['Admin']["Permissions"]["AccessToBranch"] = "Droit d'accès aux produits de la succursale";
$lang['Admin']["Permissions"]["Group"] = "Groupe d'accès";
$lang['Admin']["Permissions"]["Employee"] = "Permissions de l'employé";
$lang['Admin']["Permissions"]["IsAdminWarning"] = "Cet employé est adminstrateur, tout changement de permissions seront ignorés";

$lang["Admin"]["EditSet"]["PageTitle"] = "Modification d'un groupe";
$lang["Admin"]["AddSet"]["PageTitle"] = "Ajout d'un groupe";
$lang["Admin"]["EditSet"]["Name"] = "Nom du groupe";
$lang["Admin"]["EditSet"]["Relation"] = "Relation entre les options";
$lang["Admin"]["EditSet"]["Opts"]["Header"] = "Options";
$lang["Admin"]["EditSet"]["Opts"]["Title"] = "Nom de l'option";
$lang["Admin"]["EditSet"]["Opts"]["Argument"] = "Coût du fournisseur";
$lang["Admin"]["EditSet"]["Specs"]["Header"] = "Spécifications";
$lang["Admin"]["EditSet"]["Specs"]["Title"] = "Nom de la spécification";
$lang["Admin"]["EditSet"]["Specs"]["Argument"] = "Argument";
$lang["Admin"]["EditSet"]["PriceDisclamer"] = "Tous les prix doivent être en USD, utilisez la notation avec le point et aucun symbole";

$lang["Admin"]["MassUpload"]["PageTitle"] = "Upload de masse";
$lang["Admin"]["MassUpload"]["NotCSV"] = "Le fichier n'est pas du format 'CSV'";
$lang["Admin"]["MassUpload"]["GetTheJSON"] = "Obtenir le JSON";
$lang["Admin"]["MassUpload"]["JSONGenerator"] = "Générateur de JSON";
$lang["Admin"]["MassUpload"]["DownloadCSV"] = "Télécharger le template CSV";
$lang["Admin"]["MassUpload"]["Completed"] = "Tous les produits on été uploadé";


//ERRORS
$lang['Errors']["SomethingIsMissing"] = "Veuillez remplir tous les champs";
$lang['Errors']["WrongUsername"] = "Le nom d'utilisateur est erroné";
$lang['Errors']["WrongPassword"] = "Le mot de passe est erroné";
$lang['Errors']["CompleteEverything"] = "Veuillez completé tous les champs";
$lang['Errors']["FieldCannotBeEmpty"] = "Le champ ne peut pas être vide";
$lang['Errors']["FieldIsOK"] = "Le champ sera modifié";
$lang['Errors']["FieldOutOfBounds"] = "Au moins {1} et au maximum {2} caractères";
$lang['Errors']["MailDoesNotMatch"] = "Ce n'est pas une adresse email valide";
$lang['Errors']["UpdateSucessful"] = "Les informations ont bien été enregistrées";
$lang['Errors']["EmailIsNotAsEmailFormat"] = "L'adresse couriel n'est pas valide";
$lang['Errors']["TextTooLong"] = "Un des champs dépasse la taille maximale de caratères";
$lang['Errors']["NoSession"] = "Vous devez vous connecter avant d'accédé aux pages";
$lang['Errors']["NoBranches"] = "Vous n'avez pas été autorisé à accéder aux produits des succursales";
$lang['Errors']["NotAutorized"] = "Vous n'êtes pas autorisé à accéder à cette page";
$lang['Errors']["FieldAreNotNumerical"] = "Un des champs numériques a reçus une valeur non-numérique";
$lang['Errors']["UnexpectedValue"] = "Un des champs à reçus une valeur inattendu";
$lang['Errors']["TooManyAttempt"] = "Votre compte à été désactivé durant {0} puisque vous n'avez pas réussi à vous connecté {1} fois de suite";
$lang['Errors']["NotAnAdmin"] = "Vous n'êtes pas un administrateur";
$lang['Errors']["UnknownType"] = "Type de requête inconnue";
$lang['Errors']["NeedAPicture"] = "Aucune image n'a été fourni";
$lang['Errors']["SQL"] = "Une erreur inconnue est survenue, contactez un membre du service IT le plus rapidement possible";

?>
