<?php 
  if($currSerie != null || $currCategory == 'all' || $currCategory == 'prices') {
    $each = $series;
    $item = 'products';
  } else {
    $each = $types;
    $item = 'series';
  }
  foreach ($each as $serie) { 
    if(count($serie[$item]) > 0) {
?>
<div class="column is-narrow">
  <article class="message is-link">
    <div class="message-header">
      <p><?= json_decode($serie["name"], true)[$language] ?></p>
    </div>
    <div class="message-body" style="font-size: .9rem; padding: .50em .50em;padding-bottom: 1.7em;">
      
      <?php if($currSerie != null || $currCategory == 'all') { ?>
        <div class="table_wrapper">
          <table class="table is-hoverable is-striped is-fullwidth">
            <thead>
              <tr>
                <th></th>
                <th></th>
                <?php foreach ($branches as $branch) {
                  if($branch["spectype"] == $selectedBranch["spectype"]) {
                ?>
                  <th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="2"><?= $branch["name"] ?></th>
                <?php } } ?>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
              <tr>
                <th><?= $lang["Products"]["Name"] ?></th>
                <th><?= $lang["Products"]["Price"] ?></th>
                <?php foreach ($branches as $branch) {
                  if($branch["spectype"] == $selectedBranch["spectype"]) {
                ?>
                  <th style="text-align: center;"><?= $lang["Products"]["Stock"] ?></th>
                  <th style="text-align: center;"><?= $lang["Products"]["Order"] ?></th>
                <?php } } ?>
                <th><?= $lang["Products"]["Description"] ?></th>
                <th><?= $lang["Products"]["SCFM"] ?></th>
                <th><?= $lang["Products"]["PSI"] ?></th>
                <th><?= $lang["Products"]["HP"] ?></th>
                <th><?= $lang["Products"]["Voltage"] ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($serie[$item] as $product) { ?>
              <tr>
                <td style="text-align: left;" scope="row"><a href="viewproduct?product=<?= $product["id"] ?>&branch=<?= htmlspecialchars($_GET["branch"]) ?>" class="has-text-link"><?= trim(json_decode($product["maker"], true)[$language]) ?> - <?= trim($product["product_name"]) ?></a></td></td>
                <td style="text-align: left;"><?= calculatedPrice($product["base_price"], json_decode($product["discounts"], true)[$selectedBranch["id"]], json_decode($product["multipliers"], true)[$selectedBranch["id"]], $product["transport"], $exchangeRate) ?></td> 
                <?php foreach ($branches as $branch) {
                  if($branch["spectype"] == $selectedBranch["spectype"]) {
                ?>
                  <td class="<?php if(json_decode($product["in_stock"], true)[$branch["id"]] >= 1) { echo 'is-success'; } ?>"><?= json_decode($product["in_stock"], true)[$branch["id"]] ?></td>
                  <td class="<?php if(json_decode($product["on_order"], true)[$branch["id"]] >= 1) { echo 'is-warning'; } ?>"><?= json_decode($product["on_order"], true)[$branch["id"]] ?></td>
                <?php } } ?>
                <td style="text-align: left;"><?= trim(json_decode($product["desc"], true)[$language]) ?></td>
                <td style="text-align: left;"><?= getSpec($product, 'Capacity') ?></td>
                <td style="text-align: left;"><?= getSpec($product, 'Pressure') ?></td>
                <td style="text-align: left;"><?= getSpec($product, 'Horsepower') ?></td>
                <td style="text-align: left;"><?= getSpec($product, 'Voltage') ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <?php } elseif($currCategory == 'prices') { ?>
        <div class="table_wrapper">
          <table class="table is-hoverable is-striped is-fullwidth">
            <thead>
              <tr>
                <th></th>
                <th></th>
                <th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="4">List Price and Discount</th>
                <th></th>
                <th class="is-light" style="border-color: #dbdbdb;border-right: 2px solid #dbdbdb;border-left: 2px solid #dbdbdb;" colspan="4">Exchange Rate and Profit</th>
              </tr>
              <tr>
                <th><?= $lang["Products"]["Name"] ?></th>
                <th><?= $lang["Products"]["Description"] ?></th>
                <th><?= $lang["Products"]["ListPrice"] ?></th>
                <th><?= $lang["Products"]["Discount1"] ?></th>
                <th><?= $lang["Products"]["Discount2"] ?></th>
                <th><?= $lang["Products"]["BeforeExchange"] ?></th>
                <th><?= $lang["Products"]["Transport"] ?></th>
                <th><?= $lang["Products"]["ExchangeRate"] ?></th>
                <th><?= $lang["Products"]["AfterExchange"] ?></th>
                <th><?= $lang["Products"]["ProfitMultiplier"] ?></th>
                <th><?= $lang["Products"]["SellingPrice"] ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($serie["products"] as $product) { ?>
                <tr>
                  <td style="text-align: left;" scope="row"><a href="viewproduct?product=<?= $product["id"] ?>&branch=<?= htmlspecialchars($_GET["branch"]) ?>" class="has-text-link"><?= trim(json_decode($product["maker"], true)[$language]) ?> - <?= trim($product["product_name"]) ?></a></td></td>
                  <td style="text-align: left;"><?= trim(json_decode($product["desc"], true)[$language]) ?></td> 
                  <td style="text-align: left;"><?= formatPrice($product["base_price"]) ?></td>
                  <td style="text-align: left;"><?= json_decode($product["discounts"], true)[$selectedBranch["id"]][0] ?>%</td>
                  <td style="text-align: left;"><?= json_decode($product["discounts"], true)[$selectedBranch["id"]][1] ?>%</td>
                  <td style="text-align: left;"><?= formatPrice(calculatedDiscount($product["base_price"], json_decode($product["discounts"], true)[$selectedBranch["id"]], $exchangeRate)) ?></td>
                  <td style="text-align: left;"><?= $product["transport"] ?>%</td>
                  <td style="text-align: left;"><?= $exchangeRate ?></td>
                  <td style="text-align: left;font-weight: bold;"><?= formatPrice((floatval(calculatedTransport(calculatedDiscount($product["base_price"], json_decode($product["discounts"], true)[$selectedBranch["id"]]), $product["transport"])) * floatval($exchangeRate))) ?></td>
                  <td style="text-align: left;"><?= json_decode($product["multipliers"], true)[$selectedBranch["id"]] ?>%</td>
                  <td style="text-align: left;font-weight: bold;"><?= calculatedPrice($product["base_price"], json_decode($product["discounts"], true)[$selectedBranch["id"]], json_decode($product["multipliers"], true)[$selectedBranch["id"]], $product["transport"], $exchangeRate) ?></td> 
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <?php } else { ?>
        <div class="board-item">
          <!-- Loop through the `items` array from the current season to show each item -->
          <div class="board-item-content">
            <div class="row columns is-multiline">
              <?php foreach ($serie["series"] as $product) { ?>
              <div class="column is-2">
                <div class="card is-shady has-text-centered card-equal-height">
                  <a style="text-decoration: none;" href="products?branch=<?= htmlspecialchars($_GET["branch"]) ?>&cat=<?= $currCategory ?>&serie=<?= $product["id"] ?>">
                    <div class="card-image card-equal-height">
                      <figure class="image">
                        <img src="<?= $product["img"] ?>" style="width: 50%;margin: auto;margin-bottom: 0;padding-bottom: 0;" alt="Image">
                      </figure>
                    </div>
                    <div class="card-content" style="padding-top: 0;">
                      <div class="content" style="margin: 0;">
                        <h4><?= json_decode($product["name"], true)[$language] ?></h4>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </article>
</div>
<?php } } ?>